<?php # -*- coding: utf-8 -*-
/*
 * This file is part of the MultilingualPress package.
 *
 * (c) Inpsyde GmbH
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Inpsyde\MultilingualPress\Module\LanguageSwitcher;

class View
{
    /**
     * Displays widget view in frontend
     *
     * @param array $model
     * @return void
     * phpcs:disable Generic.Metrics.NestingLevel.TooHigh
     */
    public function render(array $model)
    {
        // phpcs:enable

        if (empty($model)) {
            return;
        }

        if ($model['title']) { ?>
            <h2 class="widget-title"><?= esc_html($model['title']) ?></h2>
        <?php }

        if ($model['items']) { ?>
            <nav class="mlp-language-switcher">
                <ul>
                    <?php foreach ($model['items'] as $item) : ?>
                        <li>
                            <a href="<?= esc_url($item->url()) ?>">
                                <?php if ($item->flag()) { ?>
                                    <img alt="<?= esc_attr($item->isoName()) ?>" src="<?= esc_attr($item->flag()) ?>">
                                <?php } ?>
                                <?= esc_attr($item->isoName()) ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </nav>
        <?php }
    }
}
