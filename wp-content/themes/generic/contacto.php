<?php /* Template Name: Contacto */ ?>
<?php get_header(); ?>
<div class="titulo-pagina">
    <h1 style="color: #ff6400;
    font-size: 28px;">Contáctanos</h1>    
</div>
<main id="content">
    <div class="parrafo estrecho">
    <?php
    echo do_shortcode('[contact-form-7 id="58" title="Formulario de contacto"]')
    ?>
    </div>
    
</main>
<?php get_footer(); ?>