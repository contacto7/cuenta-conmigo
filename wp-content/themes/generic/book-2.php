<?php /* Template Name: Book 2*/ ?>
<?php
//IMPORTANTE: id="pag-6" y id="pag-7" tienen su primera imagen con trasparencia cambiadas
if(isset($_GET["sexo"])){
    $sexo=$_GET["sexo"];
}else{
    //echo "<script> window.location.href='index.php';</script>";
}

if(isset($_GET["nombre"])){
    //$nombre=urldecode($_GET["nombre"]);
    $nombre =urldecode($_GET["nombre"]);
}else{
    //echo "<script> window.location.href='index.php';</script>";
}
if(isset($_GET["fec_nin"])){
    //$nombre=urldecode($_GET["nombre"]);
    $fecha_nino =urldecode($_GET["fec_nin"]);
    $partes_fecha = explode("-", $fecha_nino);
    $dia_nac= $partes_fecha[0];
    $mes_nac= $partes_fecha[1];
    $ano_nac= $partes_fecha[2];

    $now = time(); // or your date as well

    //Offset, el número de meses significa cuánto tiempo tienes de pasado el cumpleaños para regalar por el año que pasó
    $now = strtotime('-1 month', $now);

    $your_date = strtotime("$ano_nac-$mes_nac-$dia_nac");
    $datediff = $now - $your_date;
    $anos_nino= ceil($datediff / (60 * 60 * 24 * 365));

    /*
    echo $datediff."<br>";
    echo ceil($datediff / (60 * 60 * 24 * 365));
    */
    $mes_txt_nac="";
    switch ((int)$mes_nac) {
        case 1:
            $mes_txt_nac = "enero";
            break;
        case 2:
            $mes_txt_nac = "febrero";
            break;
        case 3:
            $mes_txt_nac = "marzo";
            break;
        case 4:
            $mes_txt_nac = "abril";
            break;
        case 5:
            $mes_txt_nac = "mayo";
            break;
        case 6:
            $mes_txt_nac = "junio";
            break;
        case 7:
            $mes_txt_nac = "julio";
            break;
        case 8:
            $mes_txt_nac = "agosto";
            break;
        case 9:
            $mes_txt_nac = "septiembre";
            break;
        case 10:
            $mes_txt_nac = "octubre";
            break;
        case 11:
            $mes_txt_nac = "noviembre";
            break;
        case 12:
            $mes_txt_nac = "diciembre";
            break;
    }

}
if(isset($_GET["nbr_pap"])){
    //$nombre_padre=urldecode($_GET["nbr_pap"]);
    $nombre_padre = urldecode($_GET["nbr_pap"]);
}
if(isset($_GET["nbr_mad"])){
    //$nombre_madre=urldecode($_GET["nbr_mad"]);
    $nombre_madre=urldecode($_GET["nbr_mad"]);
}
if(isset($_GET["nbr_gat"])){
    //$nombre_madre=urldecode($_GET["nbr_mad"]);
    $nombre_gato=urldecode($_GET["nbr_gat"]);
}
if(isset($_GET["nbr_fam"])){
    //$nombre_madre=urldecode($_GET["nbr_mad"]);
    $nombre_familiar=urldecode($_GET["nbr_fam"]);
}
if(isset($_GET["ded"])){
    //$dedicatoria=urldecode($_GET["ded"]);
    $dedicatoria=stripslashes(urldecode($_GET["ded"]));
}
if(isset($_GET["ciu_nac"])){
    $ciudad_nacimiento=stripslashes(urldecode($_GET["ciu_nac"]));

}
if(isset($_GET["can_fav"])){
    $cancion_favorita=stripslashes(urldecode($_GET["can_fav"]));

}
if(isset($_GET["com_fav"])){
    $comida_favorita=stripslashes(urldecode($_GET["com_fav"]));

}
if(isset($_GET["jug_fav"])){
    $jugete_favorito=stripslashes(urldecode($_GET["jug_fav"]));

}
if(isset($_GET["act_fav"])){
    $actividad_favorita=stripslashes(urldecode($_GET["act_fav"]));

}
if(isset($_GET["bio"])){
    $biografia=stripslashes(urldecode($_GET["bio"]));

}

//Arreglo de break line en html para hacer echo en los textarea o inputs
$breaks = array("<br />","<br>","<br/>");




//CONVERTIR NUMERO A LETRA
//REFERENCIA: http://php.net/manual/es/function.strval.php
//OTRO EJEMPLO: https://glib.org.mx/article.php?story=20030711113025879
$nwords = array( "cero", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete",
    "ocho", "nueve", "diez", "once", "doce", "trece",
    "catorce", "quince", "dieciséis", "diecisiete", "dieciocho",
    "diecinueve", "veint", 30 => "treinta", 40 => "cuarenta",
    50 => "cincuenta", 60 => "sesenta", 70 => "setenta", 80 => "ochenta",
    90 => "noventa" );
function int_to_words($x) {
    global $nwords;

    if(!is_numeric($x))
        $w = '#';
    else if(fmod($x, 1) != 0)
        $w = '#';
    else {
        if($x < 0) {
            $w = 'minus ';
            $x = -$x;
        } else
            $w = '';
        // ... now $x is a non-negative integer.

        if($x < 20)   // 0 to 20
            $w .= $nwords[$x];
        else if($x < 30) {   // 20 to 29
            $w .= $nwords[10 * floor($x/10)];
            $r = fmod($x, 10);
            if($r > 0){
                $w .= 'i'. $nwords[$r];
            }else{
                $w .= 'e';
            }

        }  else if($x < 100) {   // 21 to 99
            $w .= $nwords[10 * floor($x/10)];
            $r = fmod($x, 10);
            if($r > 0)
                $w .= ' y '. $nwords[$r];
        } else if($x < 1000) {   // 100 to 999
            $w .= $nwords[floor($x/100)] .' ciento';
            $r = fmod($x, 100);
            if($r > 0)
                $w .= ' y '. int_to_words($r);
        } else if($x < 1000000) {   // 1000 to 999999
            $w .= int_to_words(floor($x/1000)) .' mil';
            $r = fmod($x, 1000);
            if($r > 0) {
                $w .= ' ';
                if($r < 100)
                    $w .= 'y ';
                $w .= int_to_words($r);
            }
        } else {    //  millions
            $w .= int_to_words(floor($x/1000000)) .' millon';
            $r = fmod($x, 1000000);
            if($r > 0) {
                $w .= ' ';
                if($r < 100)
                    $word .= 'y ';
                $w .= int_to_words($r);
            }
        }
    }
    return $w;
}
//echo int_to_words($anos_nino);
//END CONVERTIR NUMERO A LETRA

//LINK PARA AÑADIR AL CARRITO EL LIBRO

$id_producto = 64;
$variacion_15_15_dura = 66;
$variacion_20_20_dura = 67;
$variacion_20_20_blanda = 69;

$link = $_SERVER['SERVER_NAME']."/cart/?add-to-cart=".$id_producto;

/*linK=
    http://18.224.253.62/libro/?sexo=h&nombre=Jes%C3%BAs&nbr_pap=Jos%C3%A9&nbr_mad=Maria&ded=Para%20mi%20hermoso%20pr%C3%ADncipe%20Jes%C3%BAs%20en%20su%20cumplea%C3%B1os.%3Cbr%20%2F%3ETe%20amamos%20mucho%20papi%2C%20y%20mami.&bio=Naci%C3%B3%20el%2024%20de%20julio%20de%202017.%3Cbr%20%2F%3EEs%20una%20ni%C3%B1a%20vivaz%20y%20risue%C3%B1a.%3Cbr%20%2F%3ELe%20encanta%20ver%20videos%20de%20Little%20Kitten%20en%20Youtube%2C%20porque%20le%20gustan%20los%20gatos%20como%20su%20gata%20%22Lola%22.%3Cbr%20%2F%3EAprende%20todo%20de%20su%20querida%20hermanita%20mayor%20Kianna%20y%20le%20gusta%20jugar%20con%20sus%20juguetes.%3Cbr%20%2F%3EYa%20aprendi%C3%B3%20a%20caminar%20y%20se%20desplaza%20por%20toda%20la%20casa.&img_0=1540332869&img_1=1540318626&img_2=1540333162&img_3=1540333182&img_4=1540333205
*/
?>
<?php get_header(); ?>

<link rel="stylesheet" href="../wp-content/themes/generic/css/bootstrap-mix.css">
<link href="../wp-content/themes/generic/css/croppie.css" rel="stylesheet">
<link rel="stylesheet" href="/wp-content/themes/generic/css/slick.css" type="text/css" media="all">
<link rel="stylesheet" href="/wp-content/themes/generic/css/slick-theme.css" type="text/css" media="all">
<link href="../wp-content/themes/generic/css/princ-lib2-<?php echo ($sexo == "h" ? "h" : "m"); ?>.css?ver=<?php echo INOLOOP_VERSION; ?>" rel="stylesheet">
<link href="../wp-content/themes/generic/css/bootstrap-datetimepicker.css" rel="stylesheet">
<main id="content">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="entry-content">
                <div class="vis-libro seccion marco">
                    <div class="castillo-l adorno">
                        <img src="/wp-content/imagenes/Castillo.png" alt="castillo">
                    </div>
                    <div class="titulo-pagina">
                        <h1 class="tit-inicio-mobile">Preview the book you have created for <span class="nombre-libro"><?php echo $nombre; ?></span></h1>

                    </div>
                    <div class="subir marco subir-foto-libro" style="display:none">
                        <div class="imagen-ref adorno"  href="#subir-foto-mod" data-target="#subir-foto-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" >
                            <div class="adorno imagen-1">

                            </div>
                            <div class="adorno camara">
                                <img src="/wp-content/imagenes/Camara.png" alt="camara">

                            </div>
                            <div class="adorno linea">
                                <img src="/wp-content/imagenes/Linea.png" alt="linea">
                            </div>
                            <div class="adorno descrip">
                                Upload photos
                            </div>

                        </div>

                    </div>
                    <div class="libro-estado-wrapp">
                        <div class="libro-estado-inn">
                            <div class="libro-estado-gif">
                                <img src="../wp-content/themes/generic/img/loader.gif">
                            </div>
                            <div class="libro-estado-txt">
                                Your story is loading...
                            </div>
                        </div>
                    </div>
                    <div class="libro marco">
                        <div class="unicornio-l adorno">
                            <img src="/wp-content/imagenes/Unicornio-l.png" alt="unicornio">
                        </div>

                        <div class="regalo-l adorno">
                            <img src="/wp-content/imagenes/Regalo.png" alt="regalo">
                        </div>
                        <div class="click-here">
                            <img src="/wp-content/themes/generic/img/click-here-2.png" alt="regalo">
                        </div>
                        <div class="visualizador-libro banner estrecho">

                            <div class="visualizador-perso-wrapper">
                                
                                <div class="libro-wrapper" style="display:none">
                                    
                                    <div class="libro-inner">
                                        
                                        
                                        
                                        

<div class="libro-pagina pagina-activa-derecha" id="pag-1" data-id="1">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp" style="overflow:hidden">
            <?php  if(isset($_GET['img_0'])){ ?>
                <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/portada-der-ing.png" style="position: relative; background-color: inherit;">
                <img class="libro-img-ade" id="imga-0-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <?php  }else{  ?>
                <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/portada-der-ing.png" style="position: relative">
            <?php }  ?>
            <div class="texto-agregar" id="p1-1-ing">
                <div id="p1-1-1-ing">
                    <?php echo $nombre; ?>'s
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/2.jpg">
        </div>
    </div>
</div>
<!--
<div class="libro-pagina pagina-siguiente" id="pag-2" data-id="2">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/p2-ing.png">
            <div class="texto-agregar" id="p1-2-ing">
                <div id="p1-1-2">
                    <?php echo $nombre; ?>'s<br>
                    Adventure in<br>
                    Machu Picchu
                </div>
            </div>     
            <div class="texto-agregar" id="p1-3-ing">
                <div id="p1-1-3">
                    Ilustrations by<br>
                    Hermanos Magia
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/4.png">
        </div>
    </div>
</div>
-->
<div class="libro-pagina" id="pag-2" data-id="2">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/5.png">
            <?php  if(isset($_GET['img_2'])){ ?>
                <img class="libro-img-ade" id="imga-retra-nin" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_2'];  ?>.png">
            <?php  }else{  ?>
                <div class="texto-agregar" id="p2-01">
                    <div id="p2-0-1">
                        FAMILY PORTRAIT
                    </div>
                </div>
            <?php }  ?>
            <div class="texto-agregar" id="p3-1">
                <div id="p3-1-1"><?php
                    if(isset($_GET["ded"])){
                        echo $dedicatoria;
                    }else{
                        echo "Dear $nombre,<br>The real magic to make all your dreams come true is within you!";
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <?php  if(isset($_GET['img_3']) && isset($_GET['img_4'])){ ?>
                <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/1.png">
                <img class="libro-img-atras" id="imga-3-m" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_3'];  ?>.png">
                <img class="libro-img-atras" id="imga-3-p" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_4'];  ?>.png">
            <?php  }elseif(isset($_GET['img_3'])){  ?>
                <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/1-sinpapa.png">
                <img class="libro-img-atras" id="imga-3-m" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_3'];  ?>.png">
            <?php }elseif(isset($_GET['img_4'])){  ?>
                <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/1-sinmama.png">
                <img class="libro-img-atras" id="imga-3-p" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_4'];  ?>.png">
            <?php }else{  ?>
                <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/1-sinmama-sinpapa.png">
            <?php }  ?>
            <img class="libro-img-ade" id="imga-3-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
        </div>
    </div>
</div>
<div class="libro-pagina" id="pag-3" data-id="3">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/2.png">
            <div class="texto-agregar" id="p4-1">
                <div id="p4-1-1">
                    <?php echo $nombre; ?> and <?php echo ($sexo == "h" ? "his" : "her"); ?> family are on vacation in Cusco. They climb the mountain to Machu Picchu, a city built by the Incas. It has stone walls like giant mazes, and terraces that go all the way up to the top. 
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/3.png">
            <img class="libro-img-atras" id="imga-4-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p4-2">
                <div id="p4-2-1">
                    <?php echo $nombre; ?> goes up and down the steps. <br><?php echo ($sexo == "h" ? "He" : "She"); ?> goes in and out of the buildings. <br><?php echo ($sexo == "h" ? "He" : "She"); ?> hides behind the rocks, surprising the tourists. <?php echo ($sexo == "h" ? "He" : "She"); ?> slides <?php echo ($sexo == "h" ? "his" : "her"); ?> hands over the stones in the walls, which have been perfectly put together as if they were a jigsaw puzzle. 
                </div>
            </div>
        </div>
    </div>
</div>
<div class="libro-pagina" id="pag-4" data-id="4">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <?php  if(isset($_GET['img_3']) && isset($_GET['img_4'])){ ?>
                <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/4.png">
                <img class="libro-img-atras" id="imga-5-m" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_3'];  ?>.png">
                <img class="libro-img-atras" id="imga-5-p" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_4'];  ?>.png">
            <?php  }elseif(isset($_GET['img_3'])){  ?>
                <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/4-sinpapa.png">
                <img class="libro-img-atras" id="imga-5-m" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_3'];  ?>.png">
            <?php }elseif(isset($_GET['img_4'])){  ?>
                <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/4-sinmama.png">
                <img class="libro-img-atras" id="imga-5-p" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_4'];  ?>.png">
            <?php }else{  ?>
                <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/4-sinmama-sinpapa.png">
            <?php }  ?>
            <div class="texto-agregar" id="p5-1">
                <div id="p5-1-1">
                    "Come on, <?php echo $nombre; ?> get in the picture!” says mom, camera at the ready.
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/5.png">
            <div class="texto-agregar" id="p5-2">
                <div id="p5-2-1">
                    They head towards the Temple of the Condor. On the ground there's a stone carved in the shape of a condor. 
                </div>
            </div>
            <div class="texto-agregar" id="p5-3">
                <div id="p5-3-1">
                    <?php echo $nombre; ?> gets closer to touch it, and as <?php echo ($sexo == "h" ? "he" : "she"); ?> does <?php echo ($sexo == "h" ? "he" : "she"); ?> sees a black feather on the carving. 
                </div>
            </div>
        </div>
    </div>
</div>
<div class="libro-pagina" id="pag-5" data-id="5">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/6.png">
            <img class="libro-img-atras" id="imga-6-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p6-1">
                <div id="p6-1-1">
                    <?php echo ($sexo == "h" ? "He" : "She"); ?> reaches out <?php echo ($sexo == "h" ? "his" : "her"); ?> hand to grab it, and the feather dances away in the breeze.
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/7.png">
            <img class="libro-img-atras" id="imga-6-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p6-2">
                <div id="p6-2-1">
                    The feather flies until it loses itself in a tunnel carved into the rock. <?php echo $nombre; ?> goes in, lowering <?php echo ($sexo == "h" ? "his" : "her"); ?> head to avoid bumping it. Everything is very dark. 
                </div>
            </div>
        </div>
    </div>
</div>
<div class="libro-pagina" id="pag-6" data-id="6">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/8.png">
            <img class="libro-img-atras" id="imga-7-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p7-1">
                <div id="p7-1-1">
                    <?php echo ($sexo == "h" ? "He" : "She"); ?> moves forward carefully, until <?php echo ($sexo == "h" ? "he" : "she"); ?> notices a light coming from the other end. 
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/9.png">
            <img class="libro-img-atras" id="imga-7-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p7-2">
                <div id="p7-2-1">
                    <?php echo $nombre; ?> comes out of the tunnel. <?php echo ($sexo == "h" ? "He" : "She"); ?> looks around, and rubs <?php echo ($sexo == "h" ? "his" : "her"); ?> eyes with <?php echo ($sexo == "h" ? "his" : "her"); ?> hands. <?php echo ($sexo == "h" ? "He" : "She"); ?> can't believe it!
                </div>
            </div>
        </div>
    </div>
</div>
<div class="libro-pagina" id="pag-7" data-id="7">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/10.png">
            <div class="texto-agregar" id="p8-1">
                <div id="p8-1-1">
                    The buildings now have thatched roofs. There are a lot of people, very different from the tourists of a moment ago. The women wear dresses with geometric patterns on them whilst the men wear different coloured tunics.
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/11.png">
            <img class="libro-img-atras" id="imga-8-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p8-2">
                <div id="p8-2-1">
                    In the distance, a colourful parade and the sound of drums announce the arrival of the leader, the Inca. He carries a golden scepter, and a gold pendant in the shape of the sun covers his chest. He walks up to <?php echo $nombre; ?> and stops in front of <?php echo ($sexo == "h" ? "him" : "her"); ?>.    
                </div>
            </div>
            <div class="texto-agregar" id="p8-3">
                <div id="p8-3-1">
                    “Welcome, we've been waiting for you”, says the Inca, holding out his hand to <?php echo ($sexo == "h" ? "him" : "her"); ?>.
                </div>
            </div>
        </div>
    </div>
</div>
<div class="libro-pagina" id="pag-8" data-id="8">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/12.png">
            <div class="texto-agregar" id="p9-1">
                <div id="p9-1-1">
                    “Our elders have predicted that a terrible earthquake will happen tomorrow."
                </div>
            </div>
            <div class="texto-agregar" id="p9-2">
                <div id="p9-2-1">
                    "In order to save the city, you must find the condor, the puma and the snake, our sacred animals!”
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/13.png">
            <div class="texto-agregar" id="p9-3">
                <div id="p9-3-1">
                    <?php echo $nombre; ?> remembers the stone condor and accepts the mission. <?php echo ($sexo == "h" ? "He" : "She"); ?> returns to the temple to search for it. <?php echo ($sexo == "h" ? "He" : "She"); ?> walks to the stone figure and touches it again. 
                </div>
            </div>
            <div class="texto-agregar" id="p9-4">
                <div id="p9-4-1">
                    The condor flaps its broad wings, and they come right out of the rock. <?php echo $nombre; ?> falls to the floor from shock.  
                </div>
            </div>
        </div>
    </div>
</div>
<div class="libro-pagina" id="pag-9" data-id="9">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/14.png">
            <img class="libro-img-atras" id="imga-10-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p10-1">
                <div id="p10-1-1">
                    “Don't be afraid, I know you need to find the puma and the snake. Come with me”, says the condor stretching out a wing to help  <?php echo ($sexo == "h" ? "him" : "her"); ?> climb onto its back.
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/15.png">
            <img class="libro-img-atras" id="imga-10-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p10-2">
                <div id="p10-2-1">
                    <?php echo $nombre; ?> sits on him, and hugs him tightly. Together they take off, flying into the sky. The condor glides, turns, rises and falls at will. 
                </div>
            </div>
        </div>
    </div>
</div>
<div class="libro-pagina" id="pag-10" data-id="10">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/16.png">
            <div class="texto-agregar" id="p11-1">
                <div id="p11-1-1">
                    “This is amazing! Everything looks different”, says <?php echo $nombre; ?>, lifting <?php echo ($sexo == "h" ? "his" : "her"); ?> arms into the air as if <?php echo ($sexo == "h" ? "he" : "she"); ?> were on a roller coaster.
                </div>
            </div>
            <div class="texto-agregar" id="p11-2">
                <div id="p11-2-1">
                    “Yes, and you can also hear different things, like the whisper of the winds, warning us what’s about to happen.”
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/17.png">
            <img class="libro-img-atras" id="imga-11-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p11-3">
                <div id="p11-3-1">
                    “Condor, I can't believe it! The city is shaped like a puma!”
                </div>
            </div>
        </div>
    </div>
</div>
<div class="libro-pagina" id="pag-11" data-id="11">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/18.png">
            <div class="texto-agregar" id="p12-1">
                <div id="p12-1-1">
                    <?php echo $nombre; ?> and the condor swoop down, and they can see the puma on top of a large hill looking sadly at the city of Cusco, the city built in the animal’s own shape. They go to meet it. 
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/19.png">
            <img class="libro-img-atras" id="imga-12-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p12-2">
                <div id="p12-2-1">
                    “Puma! Now we only have to find the snake to face the earthquake!”
                </div>
            </div>
        </div>
    </div>
</div>
<div class="libro-pagina" id="pag-12" data-id="12">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/20.png">
            <div class="texto-agregar" id="p13-1">
                <div id="p13-1-1">
                    “I've heard the wailing of the mountains and the rivers. Danger is coming, I will help you”, says the puma.
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/21.png">
            <img class="libro-img-atras" id="imga-13-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p13-2">
                <div id="p13-2-1">
                    The puma guides them into the city, to a street where the stone walls are carved with snakes. <?php echo $nombre; ?> slides <?php echo ($sexo == "h" ? "his" : "her"); ?> fingers over one of them. Immediately the snake turns green and begins to slither down, smoothly moving back and forth until it reaches the ground. 
                </div>
            </div>
        </div>
    </div>
</div>
<div class="libro-pagina" id="pag-13" data-id="13">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/22.png">
            <img class="libro-img-atras" id="imga-14-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p14-1">
                <div id="p14-1-1">
                   “The vibrations deep within the Earth tell me that the earthquake will begin at dawn. We don't have much time”, says the snake.
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/23.png">
            <img class="libro-img-atras" id="imga-14-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <img class="libro-img-atras" id="imga-14-3" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p14-2">
                <div id="p14-2-1">
                    “Since we're all together now, let's go back to Machu Picchu to stop the earthquake!” says <?php echo $nombre; ?>, punching the air with <?php echo ($sexo == "h" ? "his" : "her"); ?> fist. 
                </div>
            </div>
            <div class="texto-agregar" id="p14-3">
                <div id="p14-3-1">
                    Together they fly on the condor’s back. <?php echo $nombre; ?> strokes the puma’s fur. The snake tickles them during the journey. 
                </div>
            </div>
            <div class="texto-agregar" id="p14-4">
                <div id="p14-4-1">
                    Night falls, and they can see the Milky Way, dotted with stars. <?php echo $nombre; ?> and the animals look up, admiring them. 
                </div>
            </div>
        </div>
    </div>
</div>
<div class="libro-pagina" id="pag-14" data-id="14">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/24.png">
            <div class="texto-agregar" id="p15-1">
                <div id="p15-1-1">
                    “Look! The dark spaces between the stars are shaped like animals! They look like they’re connected to each other!” says <?php echo $nombre; ?>, linking the outlines of the animals that <?php echo ($sexo == "h" ? "he" : "she"); ?> can see in the sky with <?php echo ($sexo == "h" ? "his" : "her"); ?> finger.
                </div>
            </div>
            <div class="texto-agregar" id="p15-2">
                <div id="p15-2-1">
                    “It's true” says the Condor. “The Incas admire the stars and copy what they see, so their cities are shaped like animals.”
                </div>
            </div>
            <div class="texto-agregar" id="p15-3">
                <div id="p15-3-1">
                    It's morning when they land, but the clouds are blocking out the sun. The earth begins to tremble, releasing an avalanche of stones that falls down the mountains.  
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/25.png">
            <img class="libro-img-atras" id="imga-15-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p15-4">
                <div id="p15-4-1">
                    “Condor, fly up high and flap your wings hard to clear the clouds,” says <?php echo $nombre; ?> pointing to the sky. 
                </div>
            </div>
            <div class="texto-agregar" id="p15-5">
                <div id="p15-5-1">
                     “Puma, dig a path with your claws so that the avalanche will be diverted and not reach the city. 
                </div>
            </div>
        </div>
    </div>
</div>
<div class="libro-pagina" id="pag-15" data-id="15">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/26.png">
            <div class="texto-agregar" id="p16-1">
                <div id="p16-1-1">
                    And you, Snake, go deep, deep down and hold the land so that it stops shaking.”  
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/27.png">
            <img class="libro-img-atras" id="imga-16-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p16-2">
                <div id="p16-2-1">
                    The animals are doing everything they can to stop the earthquake, but it's not enough. A huge crack opens at <?php echo $nombre; ?>'s feet.<br>
                    “Let's do what we saw in the stars. Let's join all of our strength together!” shouts <?php echo $nombre; ?> very loudly.<br>
                    <?php echo $nombre; ?> gets on the back of the puma. The snake wraps itself around the puma’s claws and the condor lands on <?php echo $nombre; ?>'s shoulders.
                </div>
            </div>
        </div>
    </div>
</div>
<div class="libro-pagina" id="pag-16" data-id="16">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/28.png">
            <div class="texto-agregar" id="p17-1">
                <div id="p17-1-1">
                    A beam of light falls from the sky, enveloping them and finally bursts outwards over the Earth to calm its fury. <br>
                    The fierce earthquake is stopped completely and the city is framed by a brilliant rainbow. 
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/29.png">
            <img class="libro-img-atras" id="imga-17-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p17-2">
                <div id="p17-2-1">
                    The villagers come out of their houses, cheering for <?php echo $nombre; ?> and their saviors.
                </div>
            </div>
            <div class="texto-agregar" id="p17-3">
                <div id="p17-3-1">
                     The Inca approaches and <?php echo ($sexo == "h" ? "he" : "she"); ?> tells him what happened.<br>
                    “Thank you for saving us. Without your help, the earthquake would have repeated itself every 600 years”, says the Inca. 

                </div>
            </div>
        </div>
    </div>
</div>
<div class="libro-pagina" id="pag-17" data-id="17">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/30.png">
            <img class="libro-img-atras" id="imga-18-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p18-1">
                <div id="p18-1-1">
                    “Thanks to the sacred animals as well. I'll never forget this adventure. Now I have to go back, my parents will be very worried.”
                </div>
            </div>
            <div class="texto-agregar" id="p18-2">
                <div id="p18-2-1">
                    The animals accompany <?php echo $nombre; ?> back to the tunnel. <?php echo $nombre; ?> says goodbye with a big hug, and starts making <?php echo ($sexo == "h" ? "his" : "her"); ?> way back. 
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <?php
            if(isset($_GET['img_4'])){  ?>
                <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/31.png">
                <img class="libro-img-atras" id="imga-18-p" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_4'];  ?>.png">
            <?php }else{  ?>
                <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/31-sinpapa.png">
            <?php }  ?>
            <img class="libro-img-atras" id="imga-18-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p18-3">
                <div id="p18-3-1">
                    As <?php echo ($sexo == "h" ? "he" : "she"); ?> comes out of the tunnel, <?php echo ($sexo == "h" ? "his" : "her"); ?> parents say to <?php echo ($sexo == "h" ? "him" : "her"); ?>, <br>
                    "<?php echo $nombre; ?>, we lost sight of you for a few minutes, did you feel the tremor?”
                </div>
            </div>
        </div>
    </div>
</div>
<div class="libro-pagina" id="pag-18" data-id="18">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <?php
            if(isset($_GET['img_3'])){  ?>
                <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/32.png">
                <img class="libro-img-atras" id="imga-19-m" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_3'];  ?>.png">
            <?php }else{  ?>
                <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/32-sinmama.png">
            <?php }  ?>
            <img class="libro-img-atras" id="imga-19-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
            <div class="texto-agregar" id="p19-1">
                <div id="p19-1-1">
                    A black feather passes by in front of <?php echo ($sexo == "h" ? "him" : "her"); ?>. Before <?php echo ($sexo == "h" ? "he" : "she"); ?> can grab it, it flies up to the sky.
                </div>
            </div>
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/33-ing.png">
            <div class="texto-agregar" id="p19-2">
                <div id="p19-2-1">
                    And remember,<br>
                    an adventure<br> 
                    awaits everywhere...
                </div>
            </div>
        </div>
    </div>
</div>




<div class="libro-pagina" id="pag-19" data-id="19">
    <div class="libro-hoja hoja-adelante">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/23.jpg">
        </div>
    </div>
    <div class="libro-hoja hoja-atras">
        <div class="libro-img-wrapp">
            <img class="libro-img" src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/portada-izq-ing.png">
            <div class="texto-agregar" id="p20-1">
                <div id="p20-1-1">
                    info@soycuentaconmigo.com
                </div>
            </div>
        </div>
    </div>
</div>
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>










                        </div>

                    </div>
                    <div class="mobile-info-wrapp">
                        <div class="mobile-info-inn">
                            <div class="mobile-info">
                                <div class="uso-libr-info">
                                    Click on your book to turn the pages
                                </div>
                                <div class="uso-libr-tip">
                                    <small>Tip: rotate the device to see the book bigger</small>
                                </div>
                            </div>
                            <div class="botones-mobile-wrapp">
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <a
                                                onclick="jQuery( '.formato-tapa-wrapp,.botones-mobile-wrapp,.mobile-info' ).toggle()"
                                                class="btn-crearm">
                                            ADD TO<br>CART
                                        </a>
                                        <a
                                            class="btn-outline-naranja btn-crearm-empieza"
                                        >
                                            EDIT<br>BOOK
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="formato-tapa-wrapp">
                                <div class="formato-tapa-inn">
                                    <div class="formato-tapa-tit">
                                        Choose your book format:
                                    </div>
                                    <div class="opciones-lib">
                                        <a data-href="<?php echo $link."&variation_id=104&attribute_pa_tapa=pdf-digital&attribute_pa_tamano=2020&nombre_nino=$nombre&book_url=".urlencode($_SERVER['REQUEST_URI']) ?>" class="link-tipo-tapa" id="pedir-ctapa-104" onclick="setdedicatoria('104','pdf-digital','2020');">
                                            <h4>Printable version</h4>
                                            <p>
                                                <?php
                                                $variacion=get_variation_price_by_id(64,104);
                                                $precio=$variacion->display_price;
                                                echo $precio;
                                                ?>
                                            </p>
                                        </a>

                                        <a data-href="<?php echo $link."&variation_id=69&attribute_pa_tapa=blanda&attribute_pa_tamano=2020&nombre_nino=$nombre&book_url=".urlencode($_SERVER['REQUEST_URI']) ?>" class="link-tipo-tapa" id="pedir-ctapa-69" onclick="setdedicatoria('69','blanda','2020');">
                                            <h4>Softcover</h4>
                                            <p>
                                                <?php
                                                $variacion=get_variation_price_by_id(64,69);
                                                $precio=$variacion->display_price;
                                                echo $precio;
                                                ?>
                                            </p>
                                        </a>

                                        <a data-href="<?php echo $link."&variation_id=67&attribute_pa_tapa=dura&attribute_pa_tamano=2020&nombre_nino=$nombre&book_url=".urlencode($_SERVER['REQUEST_URI']) ?>" class="link-tipo-tapa" id="pedir-ctapa-67" onclick="setdedicatoria('67','dura','2020');">
                                            <h4>Hardcover</h4>
                                            <p>
                                                <?php
                                                $variacion=get_variation_price_by_id(64,67);
                                                $precio=$variacion->display_price;
                                                echo $precio;
                                                ?>
                                            </p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    

                    <?php
                        /*
                    <div class="adicionales-libro-wrapp">
                        <div class="adicionales">
                            <h1>Story created for <span class="nombre-libro"><?php echo $nombre; ?></span></h1>
                            <p class="parrafo">Discover with <?php echo $nombre; ?> the magic of this incredible adventure.</p>
                            <form action="">
                                <div class="campo">
                                    <p>Nombre de<?php echo ($sexo == "h" ? "l niño" : " la niña"); ?>:</p>
                                    <input type="text" id="nombre-nino-libro" value="<?php echo $nombre; ?>">
                                </div>
                                <div class="form-group input-group date campo" id="fecha-formcont-wrapp">
                                    <p>Cumpleaños de<?php echo ($sexo == "h" ? "l niño" : " la niña"); ?>:</p>
                                    <input type="text" class="" id="fecha-formcont" onchange="verificarfecha('fecha-formcont', 'fecha-formcont-wrapp')"  value="<?php echo $fecha_nino; ?>">
                                    <span class="input-group-addon" id="btn-dp-1" style="visibility: hidden;position: relative;left:5px;top: -78px;display: inline-block;">
                        </span>
                                </div>
                                <div class="campo">
                                    <p>Nombre del padre:</p>
                                    <input type="text" id="nombre-padre-libro" value="<?php echo (isset($_GET["nbr_pap"]) ? $nombre_padre : ""); ?>">
                                </div>
                                <div class="campo">
                                    <p>Nombre de la madre:</p>
                                    <input type="text" id="nombre-madre-libro" value="<?php echo (isset($_GET["nbr_mad"]) ? $nombre_madre : ""); ?>">
                                </div>
                                <div class="campo">
                                    <p>Gato mascota:</p>
                                    <select class="form-control campo-select" id="sexmasc-familiar-libro">
                                        <option value="1" <?php echo ( isset($_GET["sex_gat"]) ? ( $_GET["sex_gat"] == "1" ? "selected" : "") : ""); ?>>Gatito</option>
                                        <option value="2" <?php echo ( isset($_GET["sex_gat"]) ? ( $_GET["sex_gat"] == "2" ? "selected" : "") : ""); ?>>Gatita</option>
                                    </select>
                                    <input type="text" id="gato-madre-libro" value="<?php echo (isset($_GET["nbr_gat"]) ? $nombre_gato : "Tommy"); ?>">
                                </div>
                                <div class="campo">
                                    <p>Familiar más cercana:</p>
                                    <select class="form-control campo-select" id="tipo-familiar-libro">
                                        <option value="1" <?php echo ( isset($_GET["tip_fam"]) ? ( $_GET["tip_fam"] == "1" ? "selected" : "") : ""); ?>>Tía</option>
                                        <option value="2" <?php echo ( isset($_GET["tip_fam"]) ? ( $_GET["tip_fam"] == "2" ? "selected" : "") : ""); ?>>Abuelita</option>
                                        <option value="3" <?php echo ( isset($_GET["tip_fam"]) ? ( $_GET["tip_fam"] == "3" ? "selected" : "") : ""); ?>>Prima</option>
                                    </select>
                                    <input type="text" id="nombre-familiar-libro" value="<?php echo (isset($_GET["nbr_fam"]) ? $nombre_familiar : "Nichola"); ?>">
                                </div>
                                <div class="campo campo-dedicatoria">
                                    <p>Dedication:</p>
                                    <textarea type="text" id="dedicatoria-libro" rows="3" class="inputs-texto" data-cuenta="cuentachar-ded" data-max="140"><?php
                                        if(isset($_GET["ded"])){
                                            $dedicatoria = str_ireplace($breaks, "\r\n", $dedicatoria);
                                            echo $dedicatoria;
                                        }else{
                                            echo "Dear $nombre,\nReal magic to come true your dreams is in you!";
                                        }
                                        ?></textarea>
                                    <div>(<span id="cuentachar-ded">0</span>/140 characters)</div>
                                </div>
                                <div class="campo">
                                    <p>Biography:</p>
                                    <p class="form-subcampo">Ciudad de nacimiento:</p>
                                    <input type="text" id="ciudad-nacimiento-libro" value="<?php echo (isset($_GET["ciu_nac"]) ? $_GET["ciu_nac"] : ""); ?>">
                                    <p class="form-subcampo">Canción favorita:</p>
                                    <input style="text-transform:lowercase" type="text" id="cancion-favorita-libro" value="<?php echo (isset($_GET["can_fav"]) ? $_GET["can_fav"] : ""); ?>">
                                    <p class="form-subcampo">Comida favorita:</p>
                                    <input style="text-transform:lowercase" type="text" id="comida-favorita-libro" value="<?php echo (isset($_GET["com_fav"]) ? $_GET["com_fav"] : ""); ?>">
                                    <p class="form-subcampo">Juguete favorito:</p>
                                    <input style="text-transform:lowercase" type="text" id="juguete-favorito-libro" value="<?php echo (isset($_GET["jug_fav"]) ? $_GET["jug_fav"] : ""); ?>">
                                    <p class="form-subcampo">Actividad favorita:</p>
                                    <input style="text-transform:lowercase" type="text" id="actividad-favorita-libro" value="<?php echo (isset($_GET["act_fav"]) ? $_GET["act_fav"] : ""); ?>">
                                    <p class="form-subcampo">Adicional a biografía:</p>
                                    <textarea type="text" id="biografia-libro" rows="2" class="inputs-texto" data-cuenta="cuentachar-bio" data-max="30"><?php
                                        if(isset($_GET["bio"])){
                                            $biografia = str_ireplace($breaks, "\r\n", $biografia);
                                            echo $biografia;
                                        } ?></textarea>
                                    <div>(<span id="cuentachar-bio">0</span>/30 characters)</div>
                                </div>
                                <div class="btn-form-mod">
                                    <div id="modificar">
                                        Modify
                                    </div>
                                </div>
                                <div class="btn-form-mod">
                                    <a id="ordenar" data-target="#elegir-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin"> Order

                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>

                        */
                    ?>
                    
                    
                    <div class="nube-inter adorno">
                        <img src="../wp-content/imagenes/Nubes1.png" alt="nubes">

                    </div>
                </div>


            </div>
        </article>
    <?php endwhile; endif; ?>
</main>


<!-- edit book -->


<div class="btn-pasosm-wrapp">
    <div class="btn-pasosm-inn">

        <div class="btnclosem-wrapp">
            <div class="btnclosem-inn">
                <div class="btnclosem">X</div>
            </div>
        </div>

        <div class="slidem-wrapp">

            <div class="slidem-inn" id="slidem-inn-1">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="titm-wrapp">
                    <div class="titm-inn">
                        Who's the book for?
                    </div>
                </div>

                <div class="inpm-wrapp">
                    <div class="inpm-inn">
                        <input type="text" class="inpm" placeholder="Name" style="margin: 0 ">
                    </div>
                </div>

                <div class="titm-wrapp">
                    <div class="titm-inn">
                        Gender
                    </div>
                </div>

                <div class="btnsexom-wrapp">
                    <div class="btnsexom-inn">
                        <div class="btnsexom btnsexom-h sexo-el" data-sexo="h">
                            <img src="/wp-content/imagenes/nino-icon.png" class="sexoicon-mbh"
                                alt="logo de Cuenta conmigo" data-imgsexo="h">
                            <div class="sexodscr-mbh">Boy</div>
                        </div>
                        <div class="btnsexom btnsexom-m sexo-el" data-sexo="m">
                            <img src="/wp-content/imagenes/nina-icon.png" class="sexoicon-mbh"
                                alt="logo de Cuenta conmigo" data-imgsexo="m">
                            <div class="sexodscr-mbh">Girl</div>
                        </div>
                    </div>
                </div>



            </div>
            <div class="slidem-inn" id="slidem-inn-2" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="titm-wrapp">
                    <div class="titm-inn" style="font-weight:bold">
                        Now, choose <span class="nombrem"></span>'s story 
                    </div>
                </div>

                <div class="banner-ini-wrapp biw-m">
                    <div class="banner-modal estrecho" style="height: inherit; max-width: 100%">
                        <div class="banner-ini-inner">
                            <div class="single-item2">
                                <div class="carousel-inicio-item">
                                    <div class="ciimimg-wrapp">
                                        <div class="">
                                            <div class="libro-img-wrapp" style="overflow:hidden">
                                                <img class="libro-img"
                                                    src="../wp-content/themes/generic/img_lib/595px/h/book-creation-banner.png"
                                                    style="position: relative">
                                                <div class="texto-agregar p1-cor" id="p1-1" style="top:13%;font-size: 7vw !important; left: 0">
                                                    <div id="p1-1-1" ><span
                                                            class="nombrem nombrem-coronacion"></span>'s
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ciim-wrapp">
                                        <div class="ciim-tit">The Coronation</div>
                                        <div class="ciim-dscr">
                                            A fantastic adventure of friendship, courage, and magic. On your coronation day, a fierce dragon has kidnapped your best friend: the cat Tommy. Can you rescue him?
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-inicio-item">
                                    <div class="ciimimg-wrapp-2">
                                        <div class="">
                                            <div class="libro-img-wrapp" style="overflow:hidden">
                                                <img class="libro-img-2"
                                                    src="../wp-content/themes/generic/img_lib_2/595px/h/book-2-creation-banner.png"
                                                    style="position: relative">
                                                <div class="texto-agregar" id="p1-1-adv" style="top:13%">
                                                    <div id="p1-1-1-adv">
                                                        <span class="nombrem nombrem-browm"
                                                            style="rgb(79,0,0) !important"></span>'s
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ciim-wrapp">
                                        <div class="ciim-tit">Adventure in Machu Picchu</div>
                                        <div class="ciim-dscr">
                                            An exciting time travel adventure in Machu Picchu. A terrible earthquake will happen soon. To save the city, you must find the sacred animals of the Incas.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="btn-crearm-wrapp">
                            <div class="btn-crearm-inn">
                                <label class="btn-crearm btn-libro-elegido"
                                    onclick="jQuery( '.btnsgtem' ).click();">
                                    PERSONALISE IT <b>&#x2192;</b>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="btn-car-in prev" aria-label="Previous"><i class="arrow left"></i></div>
                    <div class="btn-car-in next" aria-label="Next"><i class="arrow right"></i></div>
                </div>
            </div>

            <div class="slidem-inn" id="slidem-inn-3" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">
                        <img class="img-pers-in capucha-nino"
                            src="../wp-content/themes/generic/img_lib/595px/h/velo-2.png"
                            style="width: 200px;">
                    </div>
                </div>
                <div class="titm-wrapp">
                    <div class="titm-inn">
                        Upload photo here!<br>Upload a photo of <span
                            class="nombrem"></span>
                    </div>
                </div>
                <details class="detalis-ver-consejos">
                    <summary>See tips</summary>
                    <p style="text-align:center">
                        Use a high-resolution photo (min 300KB<br>
                        but less than 2M). Your child must be<br>
                        facing directly into the camera. Ensure the<br>
                        picture has good lighting and no blur.
                        <img class="consejo-img-nino"
                            src="../wp-content/themes/generic/img/tips-image.png">
                    </p>
                </details>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <label class="btn-crearm" for="upload_image">
                            UPLOAD PHOTO
                        </label>
                        <input style="display: none" type="file" name="upload_image"
                            id="upload_image" />
                    </div>
                </div>

            </div>

            <div class="slidem-inn" id="slidem-inn-4" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">

                        <div class="row">
                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                <div class="descr-ajustar-img dscr-r-red">
                                    Rotate left/right
                                </div>
                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo">-90°</div>
                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo">+90°</div>
                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo"
                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                    aria-valuenow="50">
                            </div>
                            <div class="col-xs-12 text-center">
                                <div class="descr-ajustar-img dscr-a-red">
                                    Scale down/ up
                                </div>
                                <div id="image_demo" style="width:100%; margin-top:80px">
                                    <div class="img-cropp-imgdel imgcrp-nino"><img class="capucha-nino"
                                            src="../wp-content/themes/generic/img_lib/595px/h/velo-2.png">
                                    </div>

                                </div>
                            </div>
                            <div class="titm-inn" style="font-weight:bold;">
                                Adjust <span class="nombrem"></span>'s photo
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <a href="#" class="btn-crearm crop_image_inicio" data-id="0" for="upload_image">
                            NEXT PHOTO
                        </a>
                        <a class="btn-outline-naranja" href="#"
                            onclick="jQuery( '.btnantem' ).click();">
                            CHANGE PHOTO
                        </a>
                    </div>
                </div>

            </div>

            <div class="slidem-inn" id="slidem-inn-5" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">
                        <img class="img-pers-in"
                            src="../wp-content/themes/generic/img_lib/595px/veloreina-2.png"
                            id="capucha-madre-pre">
                    </div>
                </div>
                <div class="titm-wrapp">
                    <div class="titm-inn">
                        Upload Mum’s photo (optional)
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <label class="btn-crearm" for="upload_image4">
                            UPLOAD PHOTO
                        </label>
                        <input style="display: none" type="file" name="upload_image4"
                            id="upload_image4" />
                        <a class="btn-outline-naranja" href="#"
                            onclick="jQuery( '.btnsgtem' ).click();jQuery( '.btnsgtem' ).click();">
                            SKIP
                        </a>
                    </div>
                </div>

            </div>

            <div class="slidem-inn" id="slidem-inn-6" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">

                        <div class="row">
                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                <div class="descr-ajustar-img dscr-r-red">
                                    Rotate left/right
                                </div>
                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo4">-90°</div>
                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo4">+90°</div>
                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo4"
                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                    aria-valuenow="50">
                            </div>
                            <div class="col-xs-12 text-center">
                                <div class="descr-ajustar-img dscr-a-red">
                                    Scale down/ up
                                </div>
                                <div id="image_demo4"
                                    style="width:100%; margin-top:20px;position: relative">
                                    <div class="img-cropp-imgdel imgcrp-madre"><img
                                            src="../wp-content/themes/generic/img_lib/595px/veloreina-2.png">
                                    </div>

                                </div>
                            </div>
                            <div class="titm-inn" style="font-weight:bold;">
                                Adjust Mum's photo
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <a href="#" class="btn-crearm crop_image_inicio" data-id="3">
                            NEXT PHOTO
                        </a>
                        <a class="btn-outline-naranja" href="#"
                            onclick="jQuery( '.btnantem' ).click();">
                            CHANGE PHOTO
                        </a>
                    </div>
                </div>

            </div>

            <div class="slidem-inn" id="slidem-inn-7" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">
                        <img class="img-pers-in"
                            src="../wp-content/themes/generic/img_lib/595px/velorey-2.png"
                            id="capucha-padre-pre">
                    </div>
                </div>
                <div class="titm-wrapp">
                    <div class="titm-inn">
                        Upload Dad’s photo (optional)
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <label class="btn-crearm" for="upload_image5">
                            UPLOAD PHOTO
                        </label>
                        <input style="display: none" type="file" name="upload_image5"
                            id="upload_image5" />
                        <a class="btn-outline-naranja" href="#"
                            onclick="jQuery( '.btnsgtem' ).click();jQuery( '.btnsgtem' ).click();">
                            SKIP
                        </a>
                    </div>
                </div>

            </div>

            <div class="slidem-inn" id="slidem-inn-8" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">

                        <div class="row">
                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                <div class="descr-ajustar-img dscr-r-red">
                                    Rotate left/right
                                </div>
                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo5">-90°</div>
                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo5">+90°</div>
                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo5"
                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                    aria-valuenow="50">
                            </div>
                            <div class="col-xs-12 text-center">
                                <div class="descr-ajustar-img dscr-a-red">
                                    Scale down/ up
                                </div>
                                <div id="image_demo5" style="width:100%; position: relative">
                                    <div class="img-cropp-imgdel imgcrp-padre"><img
                                            src="../wp-content/themes/generic/img_lib/595px/velorey-2.png">
                                    </div>

                                </div>
                            </div>
                            <div class="titm-inn" style="font-weight:bold;">
                                Adjust Dad's photo
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <a href="#" class="btn-crearm crop_image_inicio" data-id="4">
                            NEXT PHOTO
                        </a>
                        <a class="btn-outline-naranja" href="#"
                            onclick="jQuery( '.btnantem' ).click();">
                            CHANGE PHOTO
                        </a>
                    </div>
                </div>
            </div>

            <div class="slidem-inn" id="slidem-inn-9" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">
                        <div class="cuadro-imgsubir" id="retrato-familiar-pre"></div>
                    </div>
                </div>
                <div class="titm-wrapp">
                    <div class="titm-inn">
                        Upload a family photo (optional)
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <label class="btn-crearm" for="upload_image2">
                            UPLOAD PHOTO
                        </label>
                        <input style="display: none" type="file" name="upload_image2"
                            id="upload_image2" />
                        <a class="btn-outline-naranja" href="#"
                            onclick="jQuery( '.btnsgtem' ).click();jQuery( '.btnsgtem' ).click();">
                            SKIP
                        </a>
                    </div>
                </div>

            </div>

            <div class="slidem-inn" id="slidem-inn-10" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">

                        <div class="row">
                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                <div class="descr-ajustar-img dscr-r-red">
                                    Rotate left/right
                                </div>
                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo2">-90°</div>
                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo2">+90°</div>
                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo2"
                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                    aria-valuenow="50">
                            </div>
                            <div class="col-xs-12 text-center cropp-wrapp-rosa">
                                <div class="descr-ajustar-img dscr-a-red">
                                    Scale down/ up
                                </div>
                                <div id="image_demo2"
                                    style="width:100%; margin-top:20px;position: relative">

                                </div>
                            </div>
                            <div class="titm-inn" style="font-weight:bold;">
                                Adjust family photo
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <a href="#" class="btn-crearm crop_image_inicio" data-id="1">
                            NEXT PHOTO
                        </a>
                        <a class="btn-outline-naranja" href="#"
                            onclick="jQuery( '.btnantem' ).click();">
                            CHANGE PHOTO
                        </a>
                    </div>
                </div>

            </div>

            <div class="slidem-inn" id="slidem-inn-11" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">
                        <div class="cuadro-imgsubir" id="retrato-nino-pre"></div>
                    </div>
                </div>
                <div class="titm-wrapp">
                    <div class="titm-inn">
                        Upload a portrait of <span class="nombrem"></span> (optional)
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <label class="btn-crearm" for="upload_image3">
                            UPLOAD PHOTO
                        </label>
                        <input style="display: none" type="file" name="upload_image3"
                            id="upload_image3" />
                        <a class="btn-outline-naranja" href="#"
                            onclick="jQuery( '.btnsgtem' ).click();jQuery( '.btnsgtem' ).click();">
                            SKIP
                        </a>
                    </div>
                </div>

            </div>

            <div class="slidem-inn" id="slidem-inn-12" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">

                        <div class="row">
                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                <div class="descr-ajustar-img dscr-r-red">
                                    Rotate left/right
                                </div>
                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo3">-90°</div>
                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo3">+90°</div>
                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo3"
                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                    aria-valuenow="50">
                            </div>
                            <div class="col-xs-12 text-center cropp-wrapp-rosa">
                                <div class="descr-ajustar-img dscr-a-red">
                                    Scale down/ up
                                </div>
                                <div id="image_demo3"
                                    style="width:100%; margin-top:20px;position: relative">

                                </div>
                            </div>
                            <div class="titm-inn" style="font-weight:bold;">
                                Adjust <span class="nombrem"></span>'s portrait
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <a href="#" class="btn-crearm crop_image_inicio" data-id="2">
                            NEXT STEP
                        </a>
                        <a class="btn-outline-naranja" href="#"
                            onclick="jQuery( '.btnantem' ).click();">
                            CHANGE PHOTO
                        </a>
                    </div>
                </div>

            </div>

            <div class="slidem-inn" id="slidem-inn-13" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>
                <div class="titm-wrapp">
                    <div class="titm-inn">
                        Finally, add a dedication and other details (optional)
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">

                        <div class="row">
                            
                            

                            <form action="" class="form-info-libroinic">
                                <div class="campo">
                                    <p>Dedication:</p>
                                    <textarea type="text" id="dedicatoria-libro" rows="3"
                                        class="inputs-texto" data-cuenta="cuentachar-ded"
                                        data-max="140">Dear @Kid,
The real magic to make all your dreams come true is within you!</textarea>
                                    <div>(<span id="cuentachar-ded">0</span>/140 characters)</div>
                                </div>


                                <details class="detalis-ver-consejos campo">
                                    <summary>Details of the story</summary>
                                    <div>



                                        <div class="form-group input-group date campo"
                                            id="fecha-formcont-wrapp">
                                            <p>Child's birthday:</p>
                                            <input type="text" class="" id="fecha-formcont"
                                                onchange="verificarfecha('fecha-formcont', 'fecha-formcont-wrapp')"
                                                value="">
                                            <span class="input-group-addon" id="btn-dp-1"
                                                style="visibility: hidden;position: relative;left:5px;top: -78px;display: inline-block;">
                                            </span>
                                        </div>
                                        <div class="campo">
                                            <p>Dad's name:</p>
                                            <input type="text" id="nombre-padre-libro" value="">
                                        </div>
                                        <div class="campo">
                                            <p>Mum's name:</p>
                                            <input type="text" id="nombre-madre-libro" value="">
                                        </div>
                                        <div class="campo">
                                            <p>Cat's name:</p>
                                            <select class="form-control campo-select"
                                                id="sexmasc-familiar-libro">
                                                <option value="1">Male cat</option>
                                                <option value="2">Female cat</option>
                                            </select>
                                            <input type="text" id="gato-madre-libro" value="">
                                        </div>
                                        <div class="campo">
                                            <p>Closest relative:</p>
                                            <select class="form-control campo-select" id="tipo-familiar-libro">
                                                <option value="1">Aunt</option>
                                                <option value="2">Grandmother</option>
                                                <option value="3">Cousin</option>
                                            </select>
                                            <input type="text" id="nombre-familiar-libro" value="Nichola">
                                        </div>
                                        <div class="campo">
                                            <p>Biography:</p>
                                            <p class="form-subcampo">Birth town:</p>
                                            <input type="text" id="ciudad-nacimiento-libro" value="">
                                            <p class="form-subcampo">Favourite song:</p>
                                            <input style="text-transform:lowercase" type="text"
                                                id="cancion-favorita-libro" value="">
                                            <p class="form-subcampo">Favourite food:</p>
                                            <input style="text-transform:lowercase" type="text"
                                                id="comida-favorita-libro" value="">
                                            <p class="form-subcampo">Favourite toy:</p>
                                            <input style="text-transform:lowercase" type="text"
                                                id="juguete-favorito-libro" value="">
                                            <p class="form-subcampo">Favourite hobby:</p>
                                            <input style="text-transform:lowercase" type="text"
                                                id="actividad-favorita-libro" value="">
                                            <p class="form-subcampo">Additional interests:</p>
                                            <textarea type="text" id="biografia-libro" rows="2"
                                                class="inputs-texto" data-cuenta="cuentachar-bio"
                                                data-max="30"></textarea>
                                            <div>(<span id="cuentachar-bio">0</span>/30 characters)</div>
                                        </div>



                                    </div>
                                </details>


                            </form>
                            
                            
                            

                        </div>
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <a href="#" class="btn-crearm" id="ver-cuento">
                            PREVIEW
                        </a>
                    </div>
                </div>

            </div>

        </div>
        <div class="btnsgtem-wrapp">
            <div class="btnsgtem-inn" data-current="1">
                <div class="btnmovem btnsgtem" data-current="1" data-next="2">Siguiente &#10230;</div>
                <div class="btnmovem btnantem" data-current="2" data-next="1">&#10229; Go back</div>
            </div>
        </div>



    </div>
</div>


<!-- edit book -->


<?php get_footer(); ?>

<div id="subir-foto-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title">Upload photo</h4>
            </div>
            <div class="modal-body">
                <div class="subir-foto-wrapp">
                    <div class="btn-group btn-group-vertical btn-group-subfoto">
                        <div class="btn-group-subfotoinn">
                            <div class="btn-group-subfotoimg">
                                <img src="../wp-content/themes/generic/img/rostro-nino.png">
                            </div>
                            <div class="btn-group-subfotodscr">
                                <label class="btn btn-sub-fotomd" for="upload_image"><?php echo ($sexo == "h" ? "Boy" : "Girl"); ?>'s<br>face</label>
                            </div>

                        </div>
                        <div class="btn-group-subfotoinn" style="display: none">
                            <div class="btn-group-subfotoimg">
                                <img src="../wp-content/themes/generic/img/retrato-familia.jpg">
                            </div>
                            <div class="btn-group-subfotodscr">
                                <label class="btn btn-sub-fotomd" for="upload_image2">Family<br>portrait</label>
                            </div>

                        </div>
                        <div class="btn-group-subfotoinn">
                            <div class="btn-group-subfotoimg">
                                <img src="../wp-content/themes/generic/img/retrato-nino.png">
                            </div>
                            <div class="btn-group-subfotodscr">
                                <label class="btn btn-sub-fotomd" for="upload_image3"><?php echo ($sexo == "h" ? "Boy" : "Girl"); ?>'s<br>portrait</label>
                            </div>

                        </div>
                        <div class="btn-group-subfotoinn">
                            <div class="btn-group-subfotoimg">
                                <img src="../wp-content/themes/generic/img/rostro-mama.png">
                            </div>
                            <div class="btn-group-subfotodscr">
                                <label class="btn btn-sub-fotomd" for="upload_image4">Mum's<br>face</label>
                            </div>

                        </div>
                        <div class="btn-group-subfotoinn">
                            <div class="btn-group-subfotoimg">
                                <img src="../wp-content/themes/generic/img/rostro-papa.png">
                            </div>
                            <div class="btn-group-subfotodscr">
                                <label class="btn btn-sub-fotomd" for="upload_image5">Dad's<br>face</label>
                            </div>

                        </div>
                    </div>
                    <div style="display:none" data-dscr="Grupo de botones para subir las fotos">
                        <input type="file" name="upload_image" id="upload_image" />
                        <input type="file" name="upload_image2" id="upload_image2" />
                        <input type="file" name="upload_image3" id="upload_image3" />
                        <input type="file" name="upload_image4" id="upload_image4" />
                        <input type="file" name="upload_image5" id="upload_image5" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<style>
    #subir-foto-mod .modal-content{
        border-radius: 25px;
        border: 4px solid rgba(68, 216, 190, 1);
    }
    #subir-foto-mod .modal-header .close-modal{
        height: 30px;
        width: 30px;
        padding: 0 0 7px 0;
        background-color: rgb(68, 216, 190);
        border-radius: 100%;
        color: #fff;
        font-size: 35px;
        opacity: 1;
        line-height: 21px;
    }
    .btn-group-subfoto{
        display: inline-flex;
        justify-content: center;
        width: 100%;
    }
    .btn-group-subfotoinn{
        width: calc(100% / 5);
        padding: 15px;
    }
    .btn-group-subfotoimg{
        height: 85px;
    }
    .btn-group-subfotodscr{

    }
    .btn-group-subfotodscr label{
        white-space: unset;
        padding: 6px 5px;
    }
    .btn-group-subfotoinn:nth-child(1) label{
        background-color: rgb(170,15,177);
        border: 1px solid rgb(170,15,177)!important;
    }
    .btn-group-subfotoinn:nth-child(2) label{
        background-color: rgb(68,216,190);
        border: 1px solid rgb(68,216,190)!important;
    }
    .btn-group-subfotoinn:nth-child(3) label{
        background-color: rgb(255,151,2);
        border: 1px solid rgb(255,151,2)!important;
    }
    .btn-group-subfotoinn:nth-child(4) label{
        background-color: rgb(255,100,0);
        border: 1px solid rgb(255,100,0)!important;
    }
    .btn-group-subfotoinn:nth-child(5) label{
        background-color: rgb(253,206,14);
        border: 1px solid rgb(253,206,14)!important;
    }
    .link-tipo-tapa{
        cursor: pointer;
    }
    .adicionales form .campo:not(.campo-dedicatoria){
        display: none;
    }
</style>
<div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo ($sexo == "h" ? "BOY" : "GIRL"); ?>'S FACE</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo" type="range" step="10" aria-label="zoom" min="0" max="100" aria-valuenow="50">
                    </div>
                    <div class="col-xs-12 text-center">
                        <div id="image_demo" style="width:100%;">
                            <div class="img-cropp-imgdel imgcrp-nin<?php echo ($sexo == "h" ? "o" : "a"); ?>"><img src="../wp-content/themes/generic/img_lib_2/595px/<?php echo $sexo; ?>/capuchita.png" ></div>

                        </div>
                    </div>
                    <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <button class="btn crop_image" data-id="0">Upload image</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="uploadimageModal2" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">FAMILY PORTRAIT</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo2" type="range" step="10" aria-label="zoom" min="0" max="100" aria-valuenow="50">
                    </div>
                    <div class="col-xs-12 text-center cropp-wrapp-rosa">
                        <div id="image_demo2" style="width:100%; margin-top:30px">

                        </div>
                    </div>
                    <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <button class="btn crop_image" data-id="1">Upload image</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="uploadimageModal3" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo ($sexo == "h" ? "BOY" : "GIRL"); ?>'S PORTRAIT</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo3" type="range" step="10" aria-label="zoom" min="0" max="100" aria-valuenow="50">
                    </div>
                    <div class="col-xs-12 text-center cropp-wrapp-rosa">
                        <div id="image_demo3" style="width:100%; margin-top:30px">

                        </div>
                    </div>
                    <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <button class="btn crop_image" data-id="2">Upload image</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="uploadimageModal4" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">MUM'S FACE</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo4" type="range" step="10" aria-label="zoom" min="0" max="100" aria-valuenow="50">
                    </div>
                    <div class="col-xs-12 text-center">
                        <div id="image_demo4" style="width:100%; margin-top:20px;position:relative;">
                            <div class="img-cropp-imgdel imgcrp-madre"><img src="../wp-content/themes/generic/img_lib_2/595px/capuchita-mama.png" ></div>

                        </div>
                    </div>
                    <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <button class="btn crop_image" data-id="3">Upload image</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="uploadimageModal5" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">DAD'S FACE</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo5" type="range" step="10" aria-label="zoom" min="0" max="100" aria-valuenow="50">
                    </div>
                    <div class="col-xs-12 text-center">
                        <div id="image_demo5" style="width:100%; margin-top:0">
                            <div class="img-cropp-imgdel imgcrp-padre"><img src="../wp-content/themes/generic/img_lib_2/595px/capuchita-papa.png" ></div>

                        </div>
                    </div>
                    <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <button class="btn crop_image" data-id="4">Upload image</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script src="../wp-content/themes/generic/js/croppie.js"></script>
<script src="../wp-content/themes/generic/js/princ.js"></script>
<script src="../wp-content/themes/generic/js/moment-with-locales.js"></script>
<script src="../wp-content/themes/generic/js/bootstrap-datetimepicker.js"></script>
<style>
    <?php
    if(!isset($_GET['img_0'])){
    ?>
    .libro-img-wrapp{
        background-color: #d5e465;
    }
    <?php
    }
    ?>
</style>
<script>
var sexo_libro = "<?php echo $_GET['sexo'];  ?>";
var libro_sel = 1;
var nombre_nino = "<?php echo $_GET['nombre'];  ?>";

    var estado_carga = 0;
    var num_msg_carga = 0;
    var pagina_actual=1;
    var paginatotal = $( ".libro-pagina" ).length;

    var mensaje_subir_error="";

    console.log(paginatotal);
    console.log(location.search);
    var img_1= <?php echo (isset($_GET['img_0'])? $_GET['img_0'] : 0);  ?>;
    console.log(img_1);
    var img_2= <?php echo (isset($_GET['img_1'])? $_GET['img_1'] : 0);  ?>;
    console.log(img_2);
    var img_3= <?php echo (isset($_GET['img_2'])? $_GET['img_2'] : 0);  ?>;
    console.log(img_3);
    var img_4= <?php echo (isset($_GET['img_3'])? $_GET['img_3'] : 0);  ?>;
    console.log(img_4);
    var img_5= <?php echo (isset($_GET['img_4'])? $_GET['img_4'] : 0);  ?>;
    console.log(img_5);
    var imgs_arr = [img_1, img_2, img_3, img_4, img_5];
    console.log(imgs_arr);


    
    
/*HOME PAGE EDIT FUNCTIONS*/
jQuery(".btn-crearm-empieza").on("click", function() {
    jQuery(".btn-pasosm-wrapp").css("display", "flex");
    jQuery("body").css("overflow", "hidden");
});
jQuery(".btnclosem").on("click", function() {
    jQuery(".btn-pasosm-wrapp").css("display", "none");
    jQuery("body").css("overflow", "inherit");
});
/*Función que se ejecuta cuando se presion en siguiente,
  en los pasos para crear un nuevo cuento
*/
jQuery(".btnsgtem").on("click", function() {
    console.log("Presionó");

    let current_slide = parseInt(jQuery(".btnsgtem-inn").attr('data-current'));
    let next_slide = current_slide + 1;
    console.log(next_slide);

    if (current_slide == 8 && libro_sel == 2) {
        jQuery("#slidem-inn-8").hide();
        current_slide = 10;
        next_slide = 11;
    }

    if (current_slide == 1) {
        let nombre = jQuery(".inpm").val();
        if (!(sexo_libro && nombre)) {
            return alert("Por favor ingrese un nombre y seleccione un sexo");
        }

        nombre_nino = nombre;
        jQuery(".nombrem").html(nombre);
    }

    console.log("mostrar: " + next_slide);
    console.log("esconder: " + current_slide);

    jQuery("#slidem-inn-" + current_slide).hide();
    jQuery("#slidem-inn-" + next_slide).show();

    if (current_slide == 1) {
        console.log("slider");

        if (!jQuery(".single-item2").hasClass("slick-initialized")) {
            jQuery('.single-item2').slick({
                infinite: false,
                autoplay: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                prevArrow: jQuery('.slidem-wrapp .prev'),
                nextArrow: jQuery('.slidem-wrapp .next'),
            });

            jQuery('.single-item2').on('afterChange', function() {
                var dataId = jQuery('.single-item2 .slick-current').attr("data-slick-index");

                libro_sel = ++dataId;
            });
            mostrarDatosLibroIngles(libro_sel, sexo_libro, nombre_nino);
        }

        /*
        $image_crop3 = croppieImagenRetrato(libro_sel);
        $image_crop5 = croppieImagenPapa(libro_sel);

        mostrarDatosLibroIngles(libro_sel, sexo_libro, nombre_nino);
        */
        console.log(libro_sel);
    }


    jQuery(".btnantem").show();

    jQuery(".btnsgtem-inn").attr('data-current', next_slide);



});

    
/*Función que se ejecuta cuando se presiona en anterior,
  en los pasos para crear un nuevo cuento
*/
jQuery(".btn-libro-elegido").on("click", function() {

    $image_crop3 = croppieImagenRetrato(libro_sel);
    $image_crop5 = croppieImagenPapa(libro_sel);

    mostrarDatosLibroIngles(libro_sel, sexo_libro, nombre_nino);
});
jQuery(".btnantem").on("click", function() {
    console.log("Presionó");

    let current_slide = parseInt(jQuery(".btnsgtem-inn").attr('data-current'));
    let next_slide = current_slide - 1;

    if (next_slide == 1) {
        jQuery(".btnantem").hide();
    } else {
        jQuery(".btnantem").show();
    }

    console.log("mostrar: " + next_slide);
    console.log("esconder: " + current_slide);

    jQuery("#slidem-inn-" + current_slide).hide();
    jQuery("#slidem-inn-" + next_slide).show();

    jQuery(".btnsgtem-inn").attr('data-current', next_slide);
});
jQuery(".sexo-el").on("click", function() {
    jQuery(".sexo-el").css("color", "#fff");
    jQuery('.sexoicon-mbh').css("filter", "inherit");
    var sexo = jQuery(this).attr("data-sexo");
    //variable global
    sexo_libro = sexo;

    jQuery(".lnk-libro").attr("data-sexo", sexo);
    jQuery('[data-sexo="' + sexo + '"]').css("color", "rgb(0, 0, 0)");
    jQuery('[data-imgsexo="' + sexo + '"]').css("filter", "invert(1)");

    let src_capucha = '/wp-content/themes/generic/img_lib/595px/' + sexo + '/velo-2.png';
    let url_caratula = '/wp-content/themes/generic/img_lib/595px/'+sexo+'/1.jpg';
    let url_caratula_2 = '/wp-content/themes/generic/img_lib_2/595px/'+sexo+'/libro-2-banner-creacion.png';

    let nombre = jQuery(".inpm").val();

    jQuery('.capucha-nino').attr("src", src_capucha);
    jQuery('.btn-pasosm-wrapp .libro-img').attr("src",url_caratula);
    jQuery('.btn-pasosm-wrapp .libro-img-2').attr("src",url_caratula_2);

    if (sexo == 'm') {
        jQuery('.capucha-nino').closest('.imgcrp-nino').css({
            "top": "-30px",
            "height": "330px",
        });
        jQuery('.capucha-nino').closest('#image_demo').css("margin-top", "25px");
        jQuery('.libro-img-wrapp .nombrem-coronacion').css("color", "#653f88");
    }

});
jQuery(".lnk-libro").on("click", function() {

    var sexo = jQuery(this).attr("data-sexo");
    var nombre = jQuery("#input-crea-libro").val();
    var len_nombre = nombre.length;
    var regex = new RegExp(/^[a-zA-ZñÑ\s]+$/);
    var libro = parseInt(jQuery(".slick-active .carousel-inicio-item").attr("data-libro"));
    if(libro == 1){
       libro = "book";
    console.log("Libro seleccionado: "+libro);
    }else if(libro == 2){
       libro = "book-2";
    }
    

    if (len_nombre == 0) {
        alert("Debe ingresar un nombre");
    } else if (!regex.test(nombre)) {
        alert("Debe ingresar sólo letras.");
    } else if (len_nombre < 3) {
        alert(
            "El nombre es muy corto. Por favor contáctate con nosotros para poder brindarte una opción especial."
        );
    } else if (len_nombre > 10) {
        alert(
            "El nombre es muy largo. Por favor contáctate con nosotros para poder brindarte una opción especial."
        );
    } else if (!sexo) {
        alert("Debe seleccionar un sexo");
    } else {
        var link_href = "/"+libro+"/?nombre=" + nombre + "&sexo=" + sexo + "&tkn=0";
        window.location.href = link_href;
    }
});

    


/*SUBIR IMAGENES MODALES*/

$image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 200,
        type: 'square' //circle
    },
    boundary: {
        width: 300,
        height: 300
    },
    enableOrientation: true
});
$image_crop2 = $('#image_demo2').croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 270,
        type: 'square'
    },
    boundary: {
        width: 260,
        height: 330
    },
    enableOrientation: true
});
/*
DEPENDE DEL LIBRO, SE SELECCIONA EN croppieImagenRetrato(libro)
$image_crop3 = $('#image_demo3').croppie({
    enableExif: true,
    viewport: {
        width:205,
        height:295,
        type:'square'
    },
    boundary:{
        width:260,
        height:330
    },
    enableOrientation: true
});
*/
$image_crop4 = $('#image_demo4').croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 200,
        type: 'square'
    },
    boundary: {
        width: 300,
        height: 300
    },
    enableOrientation: true
});
/*
DEPENDE DEL LIBRO, SE SELECCIONA EN croppieImagenPapa(libro)
$image_crop5 = $('#image_demo5').croppie({
    enableExif: true,
    viewport: {
        width:200,
        height:200,
        type:'square'
    },
    boundary:{
        width:300,
        height:300
    },
    enableOrientation: true
});
*/
$('#upload_image').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
$('#upload_image2').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop2.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
$('#upload_image3').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop3.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
$('#upload_image4').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop4.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
$('#upload_image5').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop5.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
    

$('.crop_image_inicio').click(function(event) {
    $(this).css({
        "opacity": "0.5",
        "pointer-events": "none"
    });
    var num_img = parseInt($(this).attr("data-id"));
    var width_sz;
    var height_sz;
    var image_crop;
    switch (num_img) {
        case 0:
            image_crop = $image_crop;
            width_sz = 200;
            height_sz = 200;
            break;
        case 1:
            image_crop = $image_crop2;
            width_sz = 400;
            height_sz = 540;
            break;
        case 2:
            image_crop = $image_crop3;
            if (libro_sel == 1) {
                width_sz = 410;
                height_sz = 590;
            } else if (libro_sel == 2) {
                width_sz = 590;
                height_sz = 410;
            }
            break;
        case 3:
            image_crop = $image_crop4;
            width_sz = 200;
            height_sz = 200;
            break;
        case 4:
            image_crop = $image_crop5;
            if (libro_sel == 1) {
                width_sz = 200;
                height_sz = 200;
            } else if (libro_sel == 2) {
                width_sz = 150;
                height_sz = 200;
            }
            break;
        default:
            image_crop = $image_crop;
            break;
    }
    image_crop.croppie('result', {
        type: 'canvas',
        size: {
            width: width_sz,
            height: height_sz
        }
    }).then(function(response) {
        console.log("response: " + response);
        $.ajax({
            url: "/wp-content/themes/generic/upload.php",
            type: "POST",
            data: {
                "image": response
            },
            success: function(data) {
                $(".crop_image_inicio").css({
                    "opacity": "1",
                    "pointer-events": "inherit"
                });

                imgs_arr[num_img] = data;
                jQuery(".btnsgtem").click();

                console.log(imgs_arr);
            }
        });
    })
});


$('#ver-cuento').click(function(event) {
    var link_red = "";


    let nombre_nino = jQuery(".inpm").val();
    let sexo = sexo_libro;


    var fecha_nino = $("#fecha-formcont").val();
    var nombre_padre = $("#nombre-padre-libro").val();
    var nombre_madre = $("#nombre-madre-libro").val();
    var sexo_mascota = $("#sexmasc-familiar-libro").val();
    var nombre_mascota = $("#gato-madre-libro").val();
    var tipo_familiar = $("#tipo-familiar-libro").val();
    var nombre_familiar = $("#nombre-familiar-libro").val();

    var dedicatoria = $("#dedicatoria-libro").val();
    dedicatoria = dedicatoria.replace(/\r?\n/g, '<br />');

    //BIOGRAFIA();
    var ciudad_nacimiento = $("#ciudad-nacimiento-libro").val();
    var cancion_favorita = $("#cancion-favorita-libro").val();
    var comida_favorita = $("#comida-favorita-libro").val();
    var juguete_favorito = $("#juguete-favorito-libro").val();
    var actividad_favorita = $("#actividad-favorita-libro").val();
    var biografia = $("#biografia-libro").val();
    biografia = biografia.replace(/\r?\n/g, '<br />');

    if (verificarnombre(nombre_nino, "nombre del niño") && verificarnombre(nombre_padre, "nombre del padre") &&
        verificarnombre(nombre_madre, "nombre de la madre") && verificarnombre(nombre_mascota,
            "nombre de la mascota") && verificarnombre(nombre_familiar, "nombre de la familiar") &&
        verificardedicatoria(dedicatoria, "dedicatoria") && verificarbiografia(ciudad_nacimiento,
            "ciudad de nacimiento") && verificarbiografia(cancion_favorita, "canción favorita") &&
        verificarbiografia(comida_favorita, "comida favorita") && verificarbiografia(juguete_favorito,
            "juguete favorito") && verificarbiografia(actividad_favorita, "actividad favorita") &&
        verificarbiografia(biografia, "biografía")) {

        link_red += ("&nombre=" + encodeURIComponent(nombre_nino));
        link_red += ("&sexo=" + sexo);

        if (fecha_nino) {
            link_red += ("&fec_nin=" + encodeURIComponent(fecha_nino));
        }
        if (nombre_padre) {
            link_red += ("&nbr_pap=" + encodeURIComponent(nombre_padre));
        }
        if (nombre_madre) {
            link_red += ("&nbr_mad=" + encodeURIComponent(nombre_madre));
        }
        if (sexo_mascota) {
            link_red += ("&sex_gat=" + encodeURIComponent(sexo_mascota));
        }
        if (nombre_mascota) {
            link_red += ("&nbr_gat=" + encodeURIComponent(nombre_mascota));
        }
        if (tipo_familiar) {
            link_red += ("&tip_fam=" + encodeURIComponent(tipo_familiar));
        }
        if (nombre_familiar) {
            link_red += ("&nbr_fam=" + encodeURIComponent(nombre_familiar));
        }
        if (dedicatoria) {
            link_red += ("&ded=" + encodeURIComponent(dedicatoria));
        }
        if (ciudad_nacimiento) {
            link_red += ("&ciu_nac=" + encodeURIComponent(ciudad_nacimiento));
        }
        if (cancion_favorita) {
            link_red += ("&can_fav=" + encodeURIComponent(cancion_favorita));
        }
        if (comida_favorita) {
            link_red += ("&com_fav=" + encodeURIComponent(comida_favorita));
        }
        if (juguete_favorito) {
            link_red += ("&jug_fav=" + encodeURIComponent(juguete_favorito));
        }
        if (actividad_favorita) {
            link_red += ("&act_fav=" + encodeURIComponent(actividad_favorita));
        }
        if (biografia) {
            link_red += ("&bio=" + encodeURIComponent(biografia));
        }

        for (var i = 0; i < imgs_arr.length; i++) {
            if (imgs_arr[i] > 0) {
                link_red += ("&img_" + i + "=" + encodeURIComponent(imgs_arr[i]));
            }
        }

        let libro_nombre = "book";
        if (libro_sel == 2) {
            libro_nombre = "book-2"
        }

        window.location.href = "/" + libro_nombre + "/index.php?" + link_red;

    } else {
        alert(mensaje_subir_error);
    }



});


    
/*HOME PAGE EDIT FUNCTIONS*/

    

    function ingresarMensaje () {


        if(estado_carga == 1){

        } else {

            var msg_carga = "";
            switch (num_msg_carga) {
                case 0:
                    msg_carga = "The book is loading";
                    break;
                case 1:
                    msg_carga = "Create something beauty takes time :)";
                    break;
                case 2:
                    msg_carga = "Looks like the connection is slow.";
                    break;
                case 3:
                    msg_carga = "By the way, your story is looking nice!";
                    break;
                case 4:
                    msg_carga = "It's almost ready.";
                    break;
                default:
                    msg_carga = "The book is loading";
                    break;
            }
            $(".libro-estado-txt").html(msg_carga);
            console.log(estado_carga);
            if(num_msg_carga < 4){
                num_msg_carga++;
            }else{
                num_msg_carga =0;
            }
            setTimeout(ingresarMensaje, 8000); //wait 50 ms, then try again
        }
    }

    function setdedicatoria(variation, tapa, tamano){

        var nombre_nino = "<?php echo $_GET["nombre"]; ?>";
        var sexo = "<?php echo $sexo; ?>";
        var fecha_nino = $("#fecha-formcont").val();
        var nombre_padre = $("#nombre-padre-libro").val();
        var nombre_madre = $("#nombre-madre-libro").val();
        var sexo_mascota = $("#sexmasc-familiar-libro").val();
        var nombre_mascota = $("#gato-madre-libro").val();
        var tipo_familiar = $("#tipo-familiar-libro").val();
        var nombre_familiar = $("#nombre-familiar-libro").val();

        var dedicatoria = $("#dedicatoria-libro").val();
        dedicatoria = dedicatoria.replace(/\r?\n/g, '<br />');

        //BIOGRAFIA();
        var ciudad_nacimiento = $("#ciudad-nacimiento-libro").val();
        var cancion_favorita = $("#cancion-favorita-libro").val();
        var comida_favorita = $("#comida-favorita-libro").val();
        var juguete_favorito = $("#juguete-favorito-libro").val();
        var actividad_favorita = $("#actividad-favorita-libro").val();
        var biografia = $("#biografia-libro").val();
        biografia = biografia.replace(/\r?\n/g, '<br />');

        if(verificarnombre(nombre_nino, "nombre del niño") && verificarnombre(nombre_padre, "nombre del padre") && verificarnombre(nombre_madre, "nombre de la madre") && verificarnombre(nombre_mascota, "nombre de la mascota") && verificarnombre(nombre_familiar, "nombre de la familiar") && verificardedicatoria(dedicatoria, "dedicatoria") && verificarbiografia(ciudad_nacimiento, "ciudad de nacimiento") && verificarbiografia(cancion_favorita, "canción favorita") && verificarbiografia(comida_favorita, "comida favorita") && verificarbiografia(juguete_favorito, "juguete favorito")&& verificarbiografia(actividad_favorita, "actividad favorita") && verificarbiografia(biografia, "biografía")){


            if(nombre_nino){
                nombre_nino=encodeURIComponent(nombre_nino);
            }else{
                nombre_nino=encodeURIComponent("<?php echo $nombre; ?>");
            }
            console.log(nombre_nino);


            if(fecha_nino){
                fecha_nino=encodeURIComponent(fecha_nino);
            }
            console.log(fecha_nino);

            if(nombre_padre){
                nombre_padre=encodeURIComponent(nombre_padre);
            }
            console.log(nombre_padre);

            if(nombre_madre){
                nombre_madre=encodeURIComponent(nombre_madre);
            }
            console.log(nombre_madre);

            if(sexo_mascota){
                sexo_mascota=encodeURIComponent(sexo_mascota);
            }
            console.log(sexo_mascota);

            if(nombre_mascota){
                nombre_mascota=encodeURIComponent(nombre_mascota);
            }
            console.log(nombre_mascota);

            if(tipo_familiar){
                tipo_familiar=encodeURIComponent(tipo_familiar);
            }
            console.log(tipo_familiar);

            if(nombre_familiar){
                nombre_familiar=encodeURIComponent(nombre_familiar);
            }
            console.log(nombre_familiar);

            if(dedicatoria){
                dedicatoria=encodeURIComponent(dedicatoria);
            }

            if(ciudad_nacimiento){
                ciudad_nacimiento=encodeURIComponent(ciudad_nacimiento);
            }
            console.log(ciudad_nacimiento);

            if(cancion_favorita){
                cancion_favorita=encodeURIComponent(cancion_favorita);
            }
            console.log(cancion_favorita);

            if(comida_favorita){
                comida_favorita=encodeURIComponent(comida_favorita);
            }
            console.log(comida_favorita);

            if(juguete_favorito){
                juguete_favorito=encodeURIComponent(juguete_favorito);
            }
            console.log(juguete_favorito);

            if(actividad_favorita){
                actividad_favorita=encodeURIComponent(actividad_favorita);
            }
            console.log(actividad_favorita);

            if(biografia){
                biografia=encodeURIComponent(biografia);
            }
            console.log(biografia);

            jQuery.ajax({
                type:"POST",
                url: "../wp-content/themes/generic/setvariables.php",
                data: {
                    sexo: sexo,
                    nombre: nombre_nino,
                    fec_nin: fecha_nino,
                    nbr_pap: nombre_padre,
                    nbr_mad: nombre_madre,
                    sex_gat: sexo_mascota,
                    nbr_gat: nombre_mascota,
                    tip_fam: tipo_familiar,
                    nbr_fam: nombre_familiar,
                    ded: dedicatoria,
                    ciu_nac: ciudad_nacimiento,
                    can_fav: cancion_favorita,
                    com_fav: comida_favorita,
                    jug_fav: juguete_favorito,
                    act_fav: actividad_favorita,
                    bio: biografia,
                    img_0: img_1,
                    img_1: img_2,
                    img_2: img_3,
                    img_3: img_4,
                    img_4: img_5,
                    variation: variation,
                    tapa: tapa,
                    tamano: tamano
                },
                cache: false,
                success: function(result){
                    console.log("Resultado: "+result);
                    var link_href = jQuery("#pedir-ctapa-"+variation).attr("data-href");
                    console.log("Link: "+link_href);
                    window.location.href= link_href;
                },
                error: function(error){
                    console.log(JSON.stringify(error));
                }


            });




        }else{
            alert(mensaje_subir_error);
        }


    }

    //FUNCION PARA ROTAR
    function drawRotated(id_cr_container, degrees){
        var canvas=$("#"+id_cr_container+" canvas")[0];
        var ctx=canvas.getContext("2d");

        // Create a temp canvas to store our data (because we need to clear the other box after rotation.
        var tempCanvas = document.createElement("canvas"),
            tempCtx = tempCanvas.getContext("2d");

        tempCanvas.width = canvas.width;
        tempCanvas.height = canvas.height;
        // put our data onto the temp canvas
        tempCtx.drawImage(canvas,0,0,canvas.width,canvas.height);

        // Append for debugging purposes, just to show what the canvas did look like before the transforms.
        document.body.appendChild(tempCanvas);

        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.save();

        //Trasladamos el punto de rotacion
        ctx.translate((canvas.width)/2,(canvas.height)/2);
        //Rotamos
        ctx.rotate(degrees*Math.PI/180);
        //Regresamos el punto de rotación
        ctx.translate(-(canvas.width)/2,-(canvas.height)/2);
        //Dibujamos el temporal
        ctx.drawImage(tempCanvas,0,0,canvas.width,canvas.height);
        ctx.restore();
        // Append for debugging purposes, just to show what the canvas did look like before the transforms.
        document.body.removeChild(tempCanvas);
    }
    //END FUNCION PARA ROTAR
    $(document).ready(function(){
        ingresarMensaje();

        $( ".inputs-texto" ).each(function() {
            var texto = $(this).val();
            var leng_texto = texto.length;
            var elem_muestr = $(this).attr("data-cuenta");
            var max_char = $(this).attr("data-max");

            if(leng_texto < max_char){
                $("#"+elem_muestr).html(leng_texto);
            }else{
                $("#"+elem_muestr).html("<span style='color:red;font-weight:bold'>"+leng_texto+"</span>");
            }
        });


        $('.btn-sub-fotomd').on('click', function(){
            $('#subir-foto-mod').modal('toggle');
        });
        
        $('.inputs-texto').on('change input', function(){
            var texto = $(this).val();
            var leng_texto = texto.length;
            var elem_muestr = $(this).attr("data-cuenta");
            var max_char = $(this).attr("data-max");

            if(leng_texto < max_char){
                $("#"+elem_muestr).html(leng_texto);
            }else{
                $("#"+elem_muestr).html("<span style='color:red;font-weight:bold'>"+leng_texto+"</span>");
            }
        });



        //AJAX

        $('.crop_image').click(function(event){
            $( this ).css({
                "opacity": "0.5",
                "pointer-events": "none"
            });
            var num_img= parseInt($(this).attr("data-id"));
            var width_sz;
            var height_sz;
            var image_crop;
            switch (num_img) {
                case 0:
                    image_crop= $image_crop;
                    width_sz = 200;
                    height_sz = 200;
                    break;
                case 1:
                    image_crop= $image_crop2;
                    width_sz = 400;
                    height_sz = 540;
                    break;
                case 2:
                    image_crop= $image_crop3;
                    width_sz = 590;
                    height_sz = 410;
                    break;
                case 3:
                    image_crop= $image_crop4;
                    width_sz = 200;
                    height_sz = 200;
                    break;
                case 4:
                    image_crop= $image_crop5;
                    width_sz = 150;
                    height_sz = 200;
                    break;
                default:
                    image_crop= $image_crop;
                    break;
            }
            image_crop.croppie('result', {
                type: 'canvas',
                size: { width: width_sz, height: height_sz }
            }).then(function(response){
                console.log("response: "+response);
                $.ajax({
                    url:"/wp-content/themes/generic/upload.php",
                    type: "POST",
                    data:{"image": response},
                    success:function(data)
                    {
                        $('#uploadimageModal').modal('hide');

                        //Acá se tiene que poner la nueva acción
                        //window.location = "/libro.php";
                        //$('#uploaded_image').html(data);
                        var link_red = "";
                        var num_links = 0;

                        var nombre_nino = $("#nombre-nino-libro").val();
                        var fecha_nino = $("#fecha-formcont").val();
                        var nombre_padre = $("#nombre-padre-libro").val();
                        var nombre_madre = $("#nombre-madre-libro").val();
                        var sexo_mascota = $("#sexmasc-familiar-libro").val();
                        var nombre_mascota = $("#gato-madre-libro").val();
                        var tipo_familiar = $("#tipo-familiar-libro").val();
                        var nombre_familiar = $("#nombre-familiar-libro").val();

                        var dedicatoria = $("#dedicatoria-libro").val();
                        dedicatoria = dedicatoria.replace(/\r?\n/g, '<br />');

                        //BIOGRAFIA();
                        var ciudad_nacimiento = $("#ciudad-nacimiento-libro").val();
                        var cancion_favorita = $("#cancion-favorita-libro").val();
                        var comida_favorita = $("#comida-favorita-libro").val();
                        var juguete_favorito = $("#juguete-favorito-libro").val();
                        var actividad_favorita = $("#actividad-favorita-libro").val();
                        var biografia = $("#biografia-libro").val();
                        biografia = biografia.replace(/\r?\n/g, '<br />');

                        if(nombre_nino){
                            link_red+=("&nombre="+encodeURIComponent(nombre_nino));
                        }else{
                            link_red+=("&nombre="+encodeURIComponent("<?php echo $nombre; ?>"));
                        }
                        if(fecha_nino){
                            link_red+=("&fec_nin="+encodeURIComponent(fecha_nino));
                        }
                        if(nombre_padre){
                            link_red+=("&nbr_pap="+encodeURIComponent(nombre_padre));
                        }
                        if(nombre_madre){
                            link_red+=("&nbr_mad="+encodeURIComponent(nombre_madre));
                        }
                        if(sexo_mascota){
                            link_red+=("&sex_gat="+encodeURIComponent(sexo_mascota));
                        }
                        if(nombre_mascota){
                            link_red+=("&nbr_gat="+encodeURIComponent(nombre_mascota));
                        }
                        if(tipo_familiar){
                            link_red+=("&tip_fam="+encodeURIComponent(tipo_familiar));
                        }
                        if(nombre_familiar){
                            link_red+=("&nbr_fam="+encodeURIComponent(nombre_familiar));
                        }
                        if(dedicatoria){
                            link_red+=("&ded="+encodeURIComponent(dedicatoria));
                        }
                        if(ciudad_nacimiento){
                            link_red+=("&ciu_nac="+encodeURIComponent(ciudad_nacimiento));
                        }
                        if(cancion_favorita){
                            link_red+=("&can_fav="+encodeURIComponent(cancion_favorita));
                        }
                        if(comida_favorita){
                            link_red+=("&com_fav="+encodeURIComponent(comida_favorita));
                        }
                        if(juguete_favorito){
                            link_red+=("&jug_fav="+encodeURIComponent(juguete_favorito));
                        }
                        if(actividad_favorita){
                            link_red+=("&act_fav="+encodeURIComponent(actividad_favorita));
                        }
                        if(biografia){
                            link_red+=("&bio="+encodeURIComponent(biografia));
                        }


                        imgs_arr[num_img]= data;

                        for (var i = 0; i < imgs_arr.length; i++) {

                            if(imgs_arr[i] > 0 ){
                                link_red+=("&img_"+i+"="+imgs_arr[i]);
                                num_links++;
                            }
                        }
                        //$("body").append("index.php?"+link_red);
                        window.location.href = "index.php?sexo=<?php echo $sexo; ?>"+link_red;
                        //console.log("mensaje:" +data);
                    }
                });
            })
        });


        $('#modificar').click(function(event){
            var link_red = "";


            var nombre_nino = $("#nombre-nino-libro").val();
            var fecha_nino = $("#fecha-formcont").val();
            var nombre_padre = $("#nombre-padre-libro").val();
            var nombre_madre = $("#nombre-madre-libro").val();
            var sexo_mascota = $("#sexmasc-familiar-libro").val();
            var nombre_mascota = $("#gato-madre-libro").val();
            var tipo_familiar = $("#tipo-familiar-libro").val();
            var nombre_familiar = $("#nombre-familiar-libro").val();

            var dedicatoria = $("#dedicatoria-libro").val();
            dedicatoria = dedicatoria.replace(/\r?\n/g, '<br />');

            //BIOGRAFIA();
            var ciudad_nacimiento = $("#ciudad-nacimiento-libro").val();
            var cancion_favorita = $("#cancion-favorita-libro").val();
            var comida_favorita = $("#comida-favorita-libro").val();
            var juguete_favorito = $("#juguete-favorito-libro").val();
            var actividad_favorita = $("#actividad-favorita-libro").val();
            var biografia = $("#biografia-libro").val();
            biografia = biografia.replace(/\r?\n/g, '<br />');

            if(verificarnombre(nombre_nino, "nombre del niño") && verificarnombre(nombre_padre, "nombre del padre") && verificarnombre(nombre_madre, "nombre de la madre") && verificarnombre(nombre_mascota, "nombre de la mascota") && verificarnombre(nombre_familiar, "nombre de la familiar") && verificardedicatoria(dedicatoria, "dedicatoria") && verificarbiografia(ciudad_nacimiento, "ciudad de nacimiento") && verificarbiografia(cancion_favorita, "canción favorita") && verificarbiografia(comida_favorita, "comida favorita") && verificarbiografia(juguete_favorito, "juguete favorito")&& verificarbiografia(actividad_favorita, "actividad favorita") && verificarbiografia(biografia, "biografía")){

                if(nombre_nino){
                    link_red+=("&nombre="+encodeURIComponent(nombre_nino));
                }else{
                    link_red+=("&nombre="+encodeURIComponent("<?php echo $nombre; ?>"));
                }
                if(fecha_nino){
                    link_red+=("&fec_nin="+encodeURIComponent(fecha_nino));
                }
                if(nombre_padre){
                    link_red+=("&nbr_pap="+encodeURIComponent(nombre_padre));
                }
                if(nombre_madre){
                    link_red+=("&nbr_mad="+encodeURIComponent(nombre_madre));
                }
                if(sexo_mascota){
                    link_red+=("&sex_gat="+encodeURIComponent(sexo_mascota));
                }
                if(nombre_mascota){
                    link_red+=("&nbr_gat="+encodeURIComponent(nombre_mascota));
                }
                if(tipo_familiar){
                    link_red+=("&tip_fam="+encodeURIComponent(tipo_familiar));
                }
                if(nombre_familiar){
                    link_red+=("&nbr_fam="+encodeURIComponent(nombre_familiar));
                }
                if(dedicatoria){
                    link_red+=("&ded="+encodeURIComponent(dedicatoria));
                }
                if(ciudad_nacimiento){
                    link_red+=("&ciu_nac="+encodeURIComponent(ciudad_nacimiento));
                }
                if(cancion_favorita){
                    link_red+=("&can_fav="+encodeURIComponent(cancion_favorita));
                }
                if(comida_favorita){
                    link_red+=("&com_fav="+encodeURIComponent(comida_favorita));
                }
                if(juguete_favorito){
                    link_red+=("&jug_fav="+encodeURIComponent(juguete_favorito));
                }
                if(actividad_favorita){
                    link_red+=("&act_fav="+encodeURIComponent(actividad_favorita));
                }
                if(biografia){
                    link_red+=("&bio="+encodeURIComponent(biografia));
                }

                for (var i = 0; i < imgs_arr.length; i++) {
                    if(imgs_arr[i] > 0 ){
                        link_red+=("&img_"+i+"="+encodeURIComponent(imgs_arr[i]));
                    }
                }

                window.location.href = "index.php?sexo=<?php echo $sexo; ?>"+link_red;

            }else{
                alert(mensaje_subir_error);
            }



        });

        console.log("Cargo todo1");

    });

    window.onload = function() {
        $(".libro-estado-wrapp").hide();
        $(".libro-wrapper").show();
        estado_carga = 1;
        console.log("Cargo todo2");
    };
</script>