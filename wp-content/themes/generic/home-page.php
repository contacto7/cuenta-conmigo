<?php /* Template Name: Home Page */ ?>
<?php get_header(); ?>
<main id="content">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <link rel="stylesheet" href="../wp-content/themes/generic/css/bootstrap-mix.css">
        <link href="../wp-content/themes/generic/css/croppie.css" rel="stylesheet">
        <link rel="stylesheet" href="/wp-content/themes/generic/css/slick.css" type="text/css" media="all">
        <link rel="stylesheet" href="/wp-content/themes/generic/css/slick-theme.css" type="text/css" media="all">
        <link href="../wp-content/themes/generic/css/bootstrap-datetimepicker.css" rel="stylesheet">
        <div class="entry-content">
            <div class="crea-libro seccion marco">
                <div class="titulo-pagina seccion-inicio">
                    <h1 class="tit-inicio-mobile">
                        Create your personalised book in 1 minute
                    </h1>
                </div>
                <div class="banner-ini-wrapp banner-principal">
                    <div class="banner estrecho">
                        <div class="banner-ini-inner">
                            <div class="single-item">
                                 <?php
                                if(get_post_meta( 2, '_hp_imagen_1', true )){
                                ?>
                                <div class="carousel-inicio-item" data-libro="1">
                                    <img class="img-carousel-inicio"
                                        alt="cuentos personalizados para niños"
                                        src="<?php echo get_post_meta( 2, '_hp_imagen_1', true ); ?>">
                                </div>
                                 <?php
                                }
                                ?>
                                 <?php
                                if(get_post_meta( 2, '_hp_imagen_2', true )){
                                ?>
                                <div class="carousel-inicio-item" data-libro="2">
                                    <img class="img-carousel-inicio"
                                        alt="cuentos personalizados para niños"
                                        src="<?php echo get_post_meta( 2, '_hp_imagen_2', true ); ?>">
                                </div>
                                 <?php
                                }
                                ?>
                                 <?php
                                if(get_post_meta( 2, '_hp_imagen_3', true )){
                                ?>
                                <div class="carousel-inicio-item" data-libro="3">
                                    <img class="img-carousel-inicio"
                                        alt="cuentos personalizados para niños"
                                        src="<?php echo get_post_meta( 2, '_hp_imagen_3', true ); ?>">
                                </div>
                                 <?php
                                }
                                ?>
                                 <?php
                                if(get_post_meta( 2, '_hp_imagen_4', true )){
                                ?>
                                <div class="carousel-inicio-item" data-libro="3">
                                    <img class="img-carousel-inicio"
                                        alt="cuentos personalizados para niños"
                                        src="<?php echo get_post_meta( 2, '_hp_imagen_4', true ); ?>">
                                </div>
                                 <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="btn-car-in prev" aria-label="Previous"><i class="arrow left"></i></div>
                    <div class="btn-car-in next" aria-label="Next"><i class="arrow right"></i></div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <a class="btn-crearm btn-crearm-empieza" href="#">
                            Start Now
                        </a>
                    </div>
                </div>
                <div class="btn-pasosm-wrapp">
                    <div class="btn-pasosm-inn">

                        <div class="btnclosem-wrapp">
                            <div class="btnclosem-inn">
                                <div class="btnclosem">X</div>
                            </div>
                        </div>

                        <div class="slidem-wrapp">

                            <div class="slidem-inn" id="slidem-inn-1">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="titm-wrapp">
                                    <div class="titm-inn">
                                        Who's the book for?
                                    </div>
                                </div>

                                <div class="inpm-wrapp">
                                    <div class="inpm-inn">
                                        <input type="text" class="inpm" placeholder="Name" style="margin: 0 ">
                                    </div>
                                </div>

                                <div class="titm-wrapp">
                                    <div class="titm-inn">
                                        Gender
                                    </div>
                                </div>

                                <div class="btnsexom-wrapp">
                                    <div class="btnsexom-inn">
                                        <div class="btnsexom btnsexom-h sexo-el" data-sexo="h">
                                            <img src="/wp-content/imagenes/nino-icon.png" class="sexoicon-mbh"
                                                alt="logo de Cuenta conmigo" data-imgsexo="h">
                                            <div class="sexodscr-mbh">Boy</div>
                                        </div>
                                        <div class="btnsexom btnsexom-m sexo-el" data-sexo="m">
                                            <img src="/wp-content/imagenes/nina-icon.png" class="sexoicon-mbh"
                                                alt="logo de Cuenta conmigo" data-imgsexo="m">
                                            <div class="sexodscr-mbh">Girl</div>
                                        </div>
                                    </div>
                                </div>



                            </div>
                            <div class="slidem-inn" id="slidem-inn-2" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="titm-wrapp">
                                    <div class="titm-inn" style="font-weight:bold">
                                        Now, choose <span class="nombrem"></span>'s story 
                                    </div>
                                </div>

                                <div class="banner-ini-wrapp biw-m">
                                    <div class="banner-modal estrecho" style="height: inherit; max-width: 100%">
                                        <div class="banner-ini-inner">
                                            <div class="single-item2">
                                                <div class="carousel-inicio-item">
                                                    <div class="ciimimg-wrapp">
                                                        <div class="libro-hoja hoja-adelante">
                                                            <div class="libro-img-wrapp" style="overflow:hidden">
                                                                <img class="libro-img"
                                                                    src="../wp-content/themes/generic/img_lib/595px/h/book-creation-banner.png"
                                                                    style="position: relative">
                                                                <div class="texto-agregar p1-cor" id="p1-1">
                                                                    <div id="p1-1-1" ><span
                                                                            class="nombrem nombrem-coronacion"></span>'s
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ciim-wrapp">
                                                        <div class="ciim-tit">The Coronation</div>
                                                        <div class="ciim-dscr">
                                                            A fantastic adventure of friendship, courage, and magic. On your coronation day, a fierce dragon has kidnapped your best friend: the cat Tommy. Can you rescue him?
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="carousel-inicio-item">
                                                    <div class="ciimimg-wrapp-2">
                                                        <div class="libro-hoja hoja-adelante">
                                                            <div class="libro-img-wrapp" style="overflow:hidden">
                                                                <img class="libro-img-2"
                                                                    src="../wp-content/themes/generic/img_lib_2/595px/h/book-2-creation-banner.png"
                                                                    style="position: relative">
                                                                <div class="texto-agregar" id="p1-1-adv">
                                                                    <div id="p1-1-1-adv">
                                                                        <span class="nombrem nombrem-browm"
                                                                            style="rgb(79,0,0) !important"></span>'s
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ciim-wrapp">
                                                        <div class="ciim-tit">Adventure in Machu Picchu</div>
                                                        <div class="ciim-dscr">
                                                            An exciting time travel adventure in Machu Picchu. A terrible earthquake will happen soon. To save the city, you must find the sacred animals of the Incas.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-crearm-wrapp">
                                            <div class="btn-crearm-inn">
                                                <label class="btn-crearm btn-libro-elegido"
                                                    onclick="jQuery( '.btnsgtem' ).click();">
                                                    PERSONALISE IT <b>&#x2192;</b>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="btn-car-in prev" aria-label="Previous"><i class="arrow left"></i></div>
                                    <div class="btn-car-in next" aria-label="Next"><i class="arrow right"></i></div>
                                </div>
                            </div>

                            <div class="slidem-inn" id="slidem-inn-3" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">
                                        <img class="img-pers-in capucha-nino"
                                            src="../wp-content/themes/generic/img_lib/595px/h/velo-2.png"
                                            style="width: 200px;">
                                    </div>
                                </div>
                                <div class="titm-wrapp">
                                    <div class="titm-inn">
                                        Upload photo here!<br>Upload a photo of <span
                                            class="nombrem"></span>
                                    </div>
                                </div>
                                <details class="detalis-ver-consejos">
                                    <summary>See tips</summary>
                                    <p style="text-align:center">
                                        Use a high-resolution photo (min 300KB<br>
                                        but less than 2M). Your child must be<br>
                                        facing directly into the camera. Ensure the<br>
                                        picture has good lighting and no blur.
                                        <img class="consejo-img-nino"
                                            src="../wp-content/themes/generic/img/tips-image.png">
                                    </p>
                                </details>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <label class="btn-crearm" for="upload_image">
                                            UPLOAD PHOTO
                                        </label>
                                        <input style="display: none" type="file" name="upload_image"
                                            id="upload_image" />
                                    </div>
                                </div>

                            </div>

                            <div class="slidem-inn" id="slidem-inn-4" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">

                                        <div class="row">
                                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                                <div class="descr-ajustar-img dscr-r-red">
                                                    Rotate left/right
                                                </div>
                                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo">-90°</div>
                                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo">+90°</div>
                                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo"
                                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                                    aria-valuenow="50">
                                            </div>
                                            <div class="col-xs-12 text-center">
                                                <div class="descr-ajustar-img dscr-a-red">
                                                    Scale down/ up
                                                </div>
                                                <div id="image_demo" style="width:100%; margin-top:80px">
                                                    <div class="img-cropp-imgdel imgcrp-nino"><img class="capucha-nino"
                                                            src="../wp-content/themes/generic/img_lib/595px/h/velo-2.png">
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="titm-inn" style="font-weight:bold;">
                                                Adjust <span class="nombrem"></span>'s photo
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <a href="#" class="btn-crearm crop_image_inicio" data-id="0" for="upload_image">
                                            NEXT PHOTO
                                        </a>
                                        <a class="btn-outline-naranja" href="#"
                                            onclick="jQuery( '.btnantem' ).click();">
                                            CHANGE PHOTO
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="slidem-inn" id="slidem-inn-5" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">
                                        <img class="img-pers-in"
                                            src="../wp-content/themes/generic/img_lib/595px/veloreina-2.png"
                                            id="capucha-madre-pre">
                                    </div>
                                </div>
                                <div class="titm-wrapp">
                                    <div class="titm-inn">
                                        Upload Mum’s photo (optional)
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <label class="btn-crearm" for="upload_image4">
                                            UPLOAD PHOTO
                                        </label>
                                        <input style="display: none" type="file" name="upload_image4"
                                            id="upload_image4" />
                                        <a class="btn-outline-naranja" href="#"
                                            onclick="jQuery( '.btnsgtem' ).click();jQuery( '.btnsgtem' ).click();">
                                            SKIP
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="slidem-inn" id="slidem-inn-6" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">

                                        <div class="row">
                                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                                <div class="descr-ajustar-img dscr-r-red">
                                                    Rotate left/right
                                                </div>
                                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo4">-90°</div>
                                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo4">+90°</div>
                                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo4"
                                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                                    aria-valuenow="50">
                                            </div>
                                            <div class="col-xs-12 text-center">
                                                <div class="descr-ajustar-img dscr-a-red">
                                                    Scale down/ up
                                                </div>
                                                <div id="image_demo4"
                                                    style="width:100%; margin-top:20px;position: relative">
                                                    <div class="img-cropp-imgdel imgcrp-madre"><img
                                                            src="../wp-content/themes/generic/img_lib/595px/veloreina-2.png">
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="titm-inn" style="font-weight:bold;">
                                                Adjust Mum's photo
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <a href="#" class="btn-crearm crop_image_inicio" data-id="3">
                                            NEXT PHOTO
                                        </a>
                                        <a class="btn-outline-naranja" href="#"
                                            onclick="jQuery( '.btnantem' ).click();">
                                            CHANGE PHOTO
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="slidem-inn" id="slidem-inn-7" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">
                                        <img class="img-pers-in"
                                            src="../wp-content/themes/generic/img_lib/595px/velorey-2.png"
                                            id="capucha-padre-pre">
                                    </div>
                                </div>
                                <div class="titm-wrapp">
                                    <div class="titm-inn">
                                        Upload Dad’s photo (optional)
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <label class="btn-crearm" for="upload_image5">
                                            UPLOAD PHOTO
                                        </label>
                                        <input style="display: none" type="file" name="upload_image5"
                                            id="upload_image5" />
                                        <a class="btn-outline-naranja" href="#"
                                            onclick="jQuery( '.btnsgtem' ).click();jQuery( '.btnsgtem' ).click();">
                                            SKIP
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="slidem-inn" id="slidem-inn-8" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">

                                        <div class="row">
                                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                                <div class="descr-ajustar-img dscr-r-red">
                                                    Rotate left/right
                                                </div>
                                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo5">-90°</div>
                                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo5">+90°</div>
                                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo5"
                                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                                    aria-valuenow="50">
                                            </div>
                                            <div class="col-xs-12 text-center">
                                                <div class="descr-ajustar-img dscr-a-red">
                                                    Scale down/ up
                                                </div>
                                                <div id="image_demo5" style="width:100%; position: relative">
                                                    <div class="img-cropp-imgdel imgcrp-padre"><img
                                                            src="../wp-content/themes/generic/img_lib/595px/velorey-2.png">
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="titm-inn" style="font-weight:bold;">
                                                Adjust Dad's photo
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <a href="#" class="btn-crearm crop_image_inicio" data-id="4">
                                            NEXT PHOTO
                                        </a>
                                        <a class="btn-outline-naranja" href="#"
                                            onclick="jQuery( '.btnantem' ).click();">
                                            CHANGE PHOTO
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="slidem-inn" id="slidem-inn-9" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">
                                        <div class="cuadro-imgsubir" id="retrato-familiar-pre"></div>
                                    </div>
                                </div>
                                <div class="titm-wrapp">
                                    <div class="titm-inn">
                                        Upload a family photo (optional)
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <label class="btn-crearm" for="upload_image2">
                                            UPLOAD PHOTO
                                        </label>
                                        <input style="display: none" type="file" name="upload_image2"
                                            id="upload_image2" />
                                        <a class="btn-outline-naranja" href="#"
                                            onclick="jQuery( '.btnsgtem' ).click();jQuery( '.btnsgtem' ).click();">
                                            SKIP
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="slidem-inn" id="slidem-inn-10" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">

                                        <div class="row">
                                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                                <div class="descr-ajustar-img dscr-r-red">
                                                    Rotate left/right
                                                </div>
                                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo2">-90°</div>
                                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo2">+90°</div>
                                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo2"
                                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                                    aria-valuenow="50">
                                            </div>
                                            <div class="col-xs-12 text-center cropp-wrapp-rosa">
                                                <div class="descr-ajustar-img dscr-a-red">
                                                    Scale down/ up
                                                </div>
                                                <div id="image_demo2"
                                                    style="width:100%; margin-top:20px;position: relative">

                                                </div>
                                            </div>
                                            <div class="titm-inn" style="font-weight:bold;">
                                                Adjust family photo
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <a href="#" class="btn-crearm crop_image_inicio" data-id="1">
                                            NEXT PHOTO
                                        </a>
                                        <a class="btn-outline-naranja" href="#"
                                            onclick="jQuery( '.btnantem' ).click();">
                                            CHANGE PHOTO
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="slidem-inn" id="slidem-inn-11" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">
                                        <div class="cuadro-imgsubir" id="retrato-nino-pre"></div>
                                    </div>
                                </div>
                                <div class="titm-wrapp">
                                    <div class="titm-inn">
                                        Upload a portrait of <span class="nombrem"></span> (optional)
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <label class="btn-crearm" for="upload_image3">
                                            UPLOAD PHOTO
                                        </label>
                                        <input style="display: none" type="file" name="upload_image3"
                                            id="upload_image3" />
                                        <a class="btn-outline-naranja" href="#"
                                            onclick="jQuery( '.btnsgtem' ).click();jQuery( '.btnsgtem' ).click();">
                                            SKIP
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="slidem-inn" id="slidem-inn-12" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">

                                        <div class="row">
                                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                                <div class="descr-ajustar-img dscr-r-red">
                                                    Rotate left/right
                                                </div>
                                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo3">-90°</div>
                                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo3">+90°</div>
                                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo3"
                                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                                    aria-valuenow="50">
                                            </div>
                                            <div class="col-xs-12 text-center cropp-wrapp-rosa">
                                                <div class="descr-ajustar-img dscr-a-red">
                                                    Scale down/ up
                                                </div>
                                                <div id="image_demo3"
                                                    style="width:100%; margin-top:20px;position: relative">

                                                </div>
                                            </div>
                                            <div class="titm-inn" style="font-weight:bold;">
                                                Adjust <span class="nombrem"></span>'s portrait
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <a href="#" class="btn-crearm crop_image_inicio" data-id="2">
                                            NEXT STEP
                                        </a>
                                        <a class="btn-outline-naranja" href="#"
                                            onclick="jQuery( '.btnantem' ).click();">
                                            CHANGE PHOTO
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="slidem-inn" id="slidem-inn-13" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="titm-wrapp">
                                    <div class="titm-inn">
                                        Finally, add a dedication and other details (optional)
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">

                                        <div class="row">
                                            <form action="" class="form-info-libroinic">
                                                <div class="campo">
                                                    <p>Dedication:</p>
                                                    <textarea type="text" id="dedicatoria-libro" rows="3"
                                                        class="inputs-texto" data-cuenta="cuentachar-ded"
                                                        data-max="140">Dear @Kid,
The real magic to make all your dreams come true is within you!</textarea>
                                                    <div>(<span id="cuentachar-ded">0</span>/140 characters)</div>
                                                </div>
                                                
                                                
                                                <details class="detalis-ver-consejos campo">
                                                    <summary>Details of the story</summary>
                                                    <div>



                                                        <div class="form-group input-group date campo"
                                                            id="fecha-formcont-wrapp">
                                                            <p>Child's birthday:</p>
                                                            <input type="text" class="" id="fecha-formcont"
                                                                onchange="verificarfecha('fecha-formcont', 'fecha-formcont-wrapp')"
                                                                value="">
                                                            <span class="input-group-addon" id="btn-dp-1"
                                                                style="visibility: hidden;position: relative;left:5px;top: -78px;display: inline-block;">
                                                            </span>
                                                        </div>
                                                        <div class="campo">
                                                            <p>Dad's name:</p>
                                                            <input type="text" id="nombre-padre-libro" value="">
                                                        </div>
                                                        <div class="campo">
                                                            <p>Mum's name:</p>
                                                            <input type="text" id="nombre-madre-libro" value="">
                                                        </div>
                                                        <div class="campo">
                                                            <p>Cat's name:</p>
                                                            <select class="form-control campo-select"
                                                                id="sexmasc-familiar-libro">
                                                                <option value="1">Male cat</option>
                                                                <option value="2">Female cat</option>
                                                            </select>
                                                            <input type="text" id="gato-madre-libro" value="">
                                                        </div>
                                                        <div class="campo">
                                                            <p>Closest relative:</p>
                                                            <select class="form-control campo-select" id="tipo-familiar-libro">
                                                                <option value="1">Aunt</option>
                                                                <option value="2">Grandmother</option>
                                                                <option value="3">Cousin</option>
                                                            </select>
                                                            <input type="text" id="nombre-familiar-libro" value="Nichola">
                                                        </div>
                                                        <div class="campo">
                                                            <p>Biography:</p>
                                                            <p class="form-subcampo">Birth town:</p>
                                                            <input type="text" id="ciudad-nacimiento-libro" value="">
                                                            <p class="form-subcampo">Favourite song:</p>
                                                            <input style="text-transform:lowercase" type="text"
                                                                id="cancion-favorita-libro" value="">
                                                            <p class="form-subcampo">Favourite food:</p>
                                                            <input style="text-transform:lowercase" type="text"
                                                                id="comida-favorita-libro" value="">
                                                            <p class="form-subcampo">Favourite toy:</p>
                                                            <input style="text-transform:lowercase" type="text"
                                                                id="juguete-favorito-libro" value="">
                                                            <p class="form-subcampo">Favourite hobby:</p>
                                                            <input style="text-transform:lowercase" type="text"
                                                                id="actividad-favorita-libro" value="">
                                                            <p class="form-subcampo">Additional interests:</p>
                                                            <textarea type="text" id="biografia-libro" rows="2"
                                                                class="inputs-texto" data-cuenta="cuentachar-bio"
                                                                data-max="30"></textarea>
                                                            <div>(<span id="cuentachar-bio">0</span>/30 characters)</div>
                                                        </div>



                                                    </div>
                                                </details>

                                                
                                            </form>


                                        </div>
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <a href="#" class="btn-crearm" id="ver-cuento">
                                            PREVIEW
                                        </a>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="btnsgtem-wrapp">
                            <div class="btnsgtem-inn" data-current="1">
                                <div class="btnmovem btnsgtem" data-current="1" data-next="2">Siguiente &#10230;</div>
                                <div class="btnmovem btnantem" data-current="2" data-next="1">&#10229; Go back</div>
                            </div>
                        </div>



                    </div>
                </div>
                
                <div class="nube-inter adorno">
                    <img src="/wp-content/imagenes/Nubes1.png" alt="nubes">

                </div>
            </div>


            <div class="dale-play seccion marco">
                <div class="unicornio"></div>
                <h1 class="titulo">Super personalised children's books starring your family</h1>
                <p class="introduccion">Fun to create, featuring child's, mum's & dad's name and photo throughout.</p>
                <div class="video"
                    style="background:#000;webkit-border-radius: 20px;-moz-border-radius: 20px;border-radius: 20px;margin:0 auto;overflow:hidden;">
                    <?php $value = get_post_meta( 2, '_inicio_video', true ); 
                $inicio=strpos($value,"?v=")+3;
                $auto= substr($value,$inicio,strlen($value));
                $str = str_replace("watch?v=", "embed/", $value)."?modestbranding=1&autohide=1&showinfo=0&autoplay=1&mute=1&controls=0&rel=0&loop=1&playlist=".$auto    ;           
            ?>

                    <iframe width="600" height="350" src="<?php echo $str;?>" frameborder="0"></iframe>
                </div>
                <div class="nina-d adorno">
                    <img src="/wp-content/imagenes/Niña naranja.png" alt="orange girl">
                </div>
                <div class="unicornio-d adorno">
                    <img src="/wp-content/imagenes/Unicornio.png" alt="unicorn">

                </div>
                <div class="nube-inter adorno">
                    <img src="/wp-content/imagenes/Nubes2.png" alt="clouds">

                </div>
            </div>
            <div class="libros-interesantes seccion marco">
                <h1 class="titulo">Discover our stories</h1>
                <div class="libros estrecho">
                    <div class="libro estrecho">
                        <div class="textual">
                            <h2 class="nombre">
                                The Coronation </h2>
                            <p class="descripcion">
                                A fantastic adventure of friendship, courage, and magic. On your coronation day, a
                                fierce dragon has kidnapped your best friend: the cat Tommy. Can you rescue him?
                            </p>
                        </div>
                        <div class="miniatura"
                            style="background-image: url(/wp-content/uploads/sites/2/2020/05/paso-5-imagen-min.jpg);">

                        </div>
                    </div>
                    <div class="libro estrecho">
                        <div class="miniatura"
                            style="background-image: url(/wp-content/uploads/2020/04/Adventure.png);">

                        </div>
                        <div class="textual">
                            <h2 class="nombre">
                                Adventure in Machu Picchu </h2>
                            <p class="descripcion">
                                An exciting time travel adventure in Machu Picchu. A terrible earthquake will happen
                                soon. To save the city, you must find the sacred animals of the Incas. Join the condor,
                                the puma, and the snake on this quest!
                            </p>
                        </div>


                    </div>
                    <div class="nina-l1 adorno">
                        <img src="/wp-content/imagenes/Niña Lila.png" alt="niña lila">
                    </div>
                    <div class="nube-inter adorno">
                        <img src="/wp-content/imagenes/Nubes3.png" alt="nubes">

                    </div>

                </div>
            </div>

            <div class="testimonios seccion marco">
                <h1 class="titulo">Our customers love our work</h1>
                <div class="lista-testimonios estrecho marco">
                    <div class="testimonio">
                        <div class="foto-testigo testigo-1"></div>
                        <h2 class="testigo">Isela Vivanco</h2>
                        <p class="comentario">
                            My eldest daughter was very excited to see herself as the protagonist. She still takes
                            out
                            her book to read it anytime a friend comes home. Moreover, books are beautiful and of
                            excellent quality.
                        </p>
                    </div>
                    <div class="testimonio">
                        <div class="foto-testigo testigo-2"></div>
                        <h2 class="testigo">Sandra Antara</h2>
                        <p class="comentario">
                            Seeing your face, and that of your child as the protagonists of a story is very
                            exciting! I
                            love this project! The books are beautiful. My little boy loves his personalized book
                            and me
                            too!
                        </p>
                    </div>
                    <div class="testimonio">
                        <div class="foto-testigo testigo-3"></div>
                        <h2 class="testigo">Rossy Lena Pérez</h2>
                        <p class="comentario">
                            Delighted with the excellent work received: perfect, original, speechless. Of course, I
                            go
                            for more, to perpetuate the celebrations of my important events!
                        </p>
                    </div>

                    <div class="nube-t1 adorno">
                        <img src="/wp-content/imagenes/nube1izq.png" alt="nubes">
                    </div>
                    <div class="nube-t2 adorno">
                        <img src="/wp-content/imagenes/nube2cent.png" alt="nubes">
                    </div>
                    <div class="nube-t3 adorno">
                        <img src="/wp-content/imagenes/nube3dere.png" alt="nubes">
                    </div>
                </div>
                <div class="nino-t1 adorno">
                    <img src="/wp-content/imagenes/Niño azul.png" alt="nubes">
                </div>
                <div class="nino-t2 adorno">
                    <img src="/wp-content/imagenes/Niño naranja.png" alt="nubes">
                </div>
                <div class="nube-inter adorno">
                    <img src="/wp-content/imagenes/Nubes4.png" alt="nubes">
                </div>

            </div>
            <div class="preguntas seccion">
                <h1 class="titulo">Frequently Asked Questions</h1>
                <div class="lista-preguntas">
                    <div class="pregunta">
                        <h2 class="duda">
                            How does it work? </h2>
                        <p class="respuesta">
                            Very easy! Write the name of the child and choose your favorite story. Then, upload the
                            photos of the child, mum, and dad, and see how they automatically appear as protagonists
                            of
                            the story. It seems like magic! Finally, get inspired and write a dedication that leaves
                            its
                            mark.
                        </p>
                    </div>
                    <div class="pregunta">
                        <h2 class="duda">
                            Who's it for? </h2>
                        <p class="respuesta">
                            These stories are for children between 1 and 8 years old, although many of our clients
                            order
                            our books for older children or to read to their children still in Mummy's belly. </p>
                    </div>
                    <div class="pregunta">
                        <h2 class="duda">
                            How long does it take? </h2>
                        <p class="respuesta">
                            Each of our stories is a unique gift, specially printed for your little one with the
                            latest
                            technology and a top-quality finish. The total delivery time depends on your location,
                            we
                            typically handle 10-20 business days in delivery. </p>
                    </div>
                    <div class="pregunta">
                        <h2 class="duda">
                            How are they made? </h2>
                        <p class="respuesta">
                            Our stories are artistically illustrated and printed with the best quality. The
                            softcover
                            format is printed under the latest digital printing technology. The hardcover format is
                            designed to last as it comes with a rigid cover for a more luxurious finish. </p>
                    </div>
                </div>
            </div>
        </div>
    </article>
    <?php endwhile; endif; ?>
</main>
<script src="../wp-content/themes/generic/js/princ.js?ver=<?php echo INOLOOP_VERSION; ?>"></script>
<script src="../wp-content/themes/generic/js/croppie.js"></script>
<script src="../wp-content/themes/generic/js/moment-with-locales.js"></script>
<script src="../wp-content/themes/generic/js/bootstrap-datetimepicker.js"></script>
<script>
var sexo_libro;
var libro_sel = 1;
var nombre_nino;
var imgs_arr = [0, 0, 0, 0, 0];
let $image_crop3;
let $image_crop5;

jQuery(".btn-crearm-empieza").on("click", function() {
    jQuery(".btn-pasosm-wrapp").css("display", "flex");
    jQuery("body").css("overflow", "hidden");
});
jQuery(".btnclosem").on("click", function() {
    jQuery(".btn-pasosm-wrapp").css("display", "none");
    jQuery("body").css("overflow", "inherit");
});
/*Función que se ejecuta cuando se presion en siguiente,
  en los pasos para crear un nuevo cuento
*/
jQuery(".btnsgtem").on("click", function() {
    console.log("Presionó");

    let current_slide = parseInt(jQuery(".btnsgtem-inn").attr('data-current'));
    let next_slide = current_slide + 1;
    console.log(next_slide);

    if (current_slide == 8 && libro_sel == 2) {
        jQuery("#slidem-inn-8").hide();
        current_slide = 10;
        next_slide = 11;
    }

    if (current_slide == 1) {
        let nombre = jQuery(".inpm").val();
        if (!(sexo_libro && nombre)) {
            return alert("Por favor ingrese un nombre y seleccione un sexo");
        }

        nombre_nino = nombre;
        jQuery(".nombrem").html(nombre);
    }

    console.log("mostrar: " + next_slide);
    console.log("esconder: " + current_slide);

    jQuery("#slidem-inn-" + current_slide).hide();
    jQuery("#slidem-inn-" + next_slide).show();

    if (current_slide == 1) {
        console.log("slider");

        if (!jQuery(".single-item2").hasClass("slick-initialized")) {
            jQuery('.single-item2').slick({
                infinite: false,
                autoplay: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                prevArrow: jQuery('.slidem-wrapp .prev'),
                nextArrow: jQuery('.slidem-wrapp .next'),
            });

            jQuery('.single-item2').on('afterChange', function() {
                var dataId = jQuery('.single-item2 .slick-current').attr("data-slick-index");

                libro_sel = ++dataId;
            });
            mostrarDatosLibroIngles(libro_sel, sexo_libro, nombre_nino);
        }

        /*
        $image_crop3 = croppieImagenRetrato(libro_sel);
        $image_crop5 = croppieImagenPapa(libro_sel);

        mostrarDatosLibroIngles(libro_sel, sexo_libro, nombre_nino);
        */
        console.log(libro_sel);
    }


    jQuery(".btnantem").show();

    jQuery(".btnsgtem-inn").attr('data-current', next_slide);



});

jQuery(document).ready(function() {
    jQuery('.single-item').slick({
        prevArrow: jQuery('.banner-principal .prev'),
        nextArrow: jQuery('.banner-principal .next'),
    });

});
/*Función que se ejecuta cuando se presiona en anterior,
  en los pasos para crear un nuevo cuento
*/
jQuery(".btn-libro-elegido").on("click", function() {

    $image_crop3 = croppieImagenRetrato(libro_sel);
    $image_crop5 = croppieImagenPapa(libro_sel);

    mostrarDatosLibroIngles(libro_sel, sexo_libro, nombre_nino);
});
jQuery(".btnantem").on("click", function() {
    console.log("Presionó");

    let current_slide = parseInt(jQuery(".btnsgtem-inn").attr('data-current'));
    let next_slide = current_slide - 1;

    if (next_slide == 1) {
        jQuery(".btnantem").hide();
    } else {
        jQuery(".btnantem").show();
    }

    console.log("mostrar: " + next_slide);
    console.log("esconder: " + current_slide);

    jQuery("#slidem-inn-" + current_slide).hide();
    jQuery("#slidem-inn-" + next_slide).show();

    jQuery(".btnsgtem-inn").attr('data-current', next_slide);
});
jQuery(".sexo-el").on("click", function() {
    jQuery(".sexo-el").css("color", "#fff");
    jQuery('.sexoicon-mbh').css("filter", "inherit");
    var sexo = jQuery(this).attr("data-sexo");
    //variable global
    sexo_libro = sexo;

    jQuery(".lnk-libro").attr("data-sexo", sexo);
    jQuery('[data-sexo="' + sexo + '"]').css("color", "rgb(0, 0, 0)");
    jQuery('[data-imgsexo="' + sexo + '"]').css("filter", "invert(1)");

    let src_capucha = './wp-content/themes/generic/img_lib/595px/' + sexo + '/velo-2.png';
    let src_caratula = './wp-content/themes/generic/img_lib/595px/' + sexo + '/1.jpg';

    let nombre = jQuery(".inpm").val();

    jQuery('.capucha-nino').attr("src", src_capucha);
    jQuery('.libro-img').attr("src", src_caratula);

    if (sexo == 'm') {
        jQuery('.capucha-nino').closest('.imgcrp-nino').css({
            "top": "-30px",
            "height": "330px",
        });
        jQuery('.capucha-nino').closest('#image_demo').css("margin-top", "25px");
        jQuery('.libro-img-wrapp .nombrem-coronacion').css("color", "#653f88");
    }

});
jQuery(".lnk-libro").on("click", function() {

    var sexo = jQuery(this).attr("data-sexo");
    var nombre = jQuery("#input-crea-libro").val();
    var len_nombre = nombre.length;
    var regex = new RegExp(/^[a-zA-ZñÑ\s]+$/);
    var libro = parseInt(jQuery(".slick-active .carousel-inicio-item").attr("data-libro"));
    if(libro == 1){
       libro = "book";
    console.log("Libro seleccionado: "+libro);
    }else if(libro == 2){
       libro = "book-2";
    }
    

    if (len_nombre == 0) {
        alert("Debe ingresar un nombre");
    } else if (!regex.test(nombre)) {
        alert("Debe ingresar sólo letras.");
    } else if (len_nombre < 3) {
        alert(
            "El nombre es muy corto. Por favor contáctate con nosotros para poder brindarte una opción especial."
        );
    } else if (len_nombre > 10) {
        alert(
            "El nombre es muy largo. Por favor contáctate con nosotros para poder brindarte una opción especial."
        );
    } else if (!sexo) {
        alert("Debe seleccionar un sexo");
    } else {
        var link_href = "/"+libro+"/?nombre=" + nombre + "&sexo=" + sexo + "&tkn=0";
        window.location.href = link_href;
    }
});

/*SUBIR IMAGENES MODALES*/

$image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 200,
        type: 'square' //circle
    },
    boundary: {
        width: 300,
        height: 300
    },
    enableOrientation: true
});
$image_crop2 = $('#image_demo2').croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 270,
        type: 'square'
    },
    boundary: {
        width: 260,
        height: 330
    },
    enableOrientation: true
});
/*
DEPENDE DEL LIBRO, SE SELECCIONA EN croppieImagenRetrato(libro)
$image_crop3 = $('#image_demo3').croppie({
    enableExif: true,
    viewport: {
        width:205,
        height:295,
        type:'square'
    },
    boundary:{
        width:260,
        height:330
    },
    enableOrientation: true
});
*/
$image_crop4 = $('#image_demo4').croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 200,
        type: 'square'
    },
    boundary: {
        width: 300,
        height: 300
    },
    enableOrientation: true
});
/*
DEPENDE DEL LIBRO, SE SELECCIONA EN croppieImagenPapa(libro)
$image_crop5 = $('#image_demo5').croppie({
    enableExif: true,
    viewport: {
        width:200,
        height:200,
        type:'square'
    },
    boundary:{
        width:300,
        height:300
    },
    enableOrientation: true
});
*/
$('#upload_image').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
$('#upload_image2').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop2.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
$('#upload_image3').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop3.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
$('#upload_image4').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop4.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
$('#upload_image5').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop5.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
$('.crop_image_inicio').click(function(event) {
    $(this).css({
        "opacity": "0.5",
        "pointer-events": "none"
    });
    var num_img = parseInt($(this).attr("data-id"));
    var width_sz;
    var height_sz;
    var image_crop;
    switch (num_img) {
        case 0:
            image_crop = $image_crop;
            width_sz = 200;
            height_sz = 200;
            break;
        case 1:
            image_crop = $image_crop2;
            width_sz = 400;
            height_sz = 540;
            break;
        case 2:
            image_crop = $image_crop3;
            if (libro_sel == 1) {
                width_sz = 410;
                height_sz = 590;
            } else if (libro_sel == 2) {
                width_sz = 590;
                height_sz = 410;
            }
            break;
        case 3:
            image_crop = $image_crop4;
            width_sz = 200;
            height_sz = 200;
            break;
        case 4:
            image_crop = $image_crop5;
            if (libro_sel == 1) {
                width_sz = 200;
                height_sz = 200;
            } else if (libro_sel == 2) {
                width_sz = 150;
                height_sz = 200;
            }
            break;
        default:
            image_crop = $image_crop;
            break;
    }
    image_crop.croppie('result', {
        type: 'canvas',
        size: {
            width: width_sz,
            height: height_sz
        }
    }).then(function(response) {
        console.log("response: " + response);
        $.ajax({
            url: "/wp-content/themes/generic/upload.php",
            type: "POST",
            data: {
                "image": response
            },
            success: function(data) {
                $(".crop_image_inicio").css({
                    "opacity": "1",
                    "pointer-events": "inherit"
                });

                imgs_arr[num_img] = data;
                jQuery(".btnsgtem").click();

                console.log(imgs_arr);
            }
        });
    })
});



$('#ver-cuento').click(function(event) {
    var link_red = "";


    let nombre_nino = jQuery(".inpm").val();
    let sexo = sexo_libro;


    var fecha_nino = $("#fecha-formcont").val();
    var nombre_padre = $("#nombre-padre-libro").val();
    var nombre_madre = $("#nombre-madre-libro").val();
    var sexo_mascota = $("#sexmasc-familiar-libro").val();
    var nombre_mascota = $("#gato-madre-libro").val();
    var tipo_familiar = $("#tipo-familiar-libro").val();
    var nombre_familiar = $("#nombre-familiar-libro").val();

    var dedicatoria = $("#dedicatoria-libro").val();
    dedicatoria = dedicatoria.replace(/\r?\n/g, '<br />');

    //BIOGRAFIA();
    var ciudad_nacimiento = $("#ciudad-nacimiento-libro").val();
    var cancion_favorita = $("#cancion-favorita-libro").val();
    var comida_favorita = $("#comida-favorita-libro").val();
    var juguete_favorito = $("#juguete-favorito-libro").val();
    var actividad_favorita = $("#actividad-favorita-libro").val();
    var biografia = $("#biografia-libro").val();
    biografia = biografia.replace(/\r?\n/g, '<br />');

    if (verificarnombre(nombre_nino, "nombre del niño") && verificarnombre(nombre_padre, "nombre del padre") &&
        verificarnombre(nombre_madre, "nombre de la madre") && verificarnombre(nombre_mascota,
            "nombre de la mascota") && verificarnombre(nombre_familiar, "nombre de la familiar") &&
        verificardedicatoria(dedicatoria, "dedicatoria") && verificarbiografia(ciudad_nacimiento,
            "ciudad de nacimiento") && verificarbiografia(cancion_favorita, "canción favorita") &&
        verificarbiografia(comida_favorita, "comida favorita") && verificarbiografia(juguete_favorito,
            "juguete favorito") && verificarbiografia(actividad_favorita, "actividad favorita") &&
        verificarbiografia(biografia, "biografía")) {

        link_red += ("&nombre=" + encodeURIComponent(nombre_nino));
        link_red += ("&sexo=" + sexo);

        if (fecha_nino) {
            link_red += ("&fec_nin=" + encodeURIComponent(fecha_nino));
        }
        if (nombre_padre) {
            link_red += ("&nbr_pap=" + encodeURIComponent(nombre_padre));
        }
        if (nombre_madre) {
            link_red += ("&nbr_mad=" + encodeURIComponent(nombre_madre));
        }
        if (sexo_mascota) {
            link_red += ("&sex_gat=" + encodeURIComponent(sexo_mascota));
        }
        if (nombre_mascota) {
            link_red += ("&nbr_gat=" + encodeURIComponent(nombre_mascota));
        }
        if (tipo_familiar) {
            link_red += ("&tip_fam=" + encodeURIComponent(tipo_familiar));
        }
        if (nombre_familiar) {
            link_red += ("&nbr_fam=" + encodeURIComponent(nombre_familiar));
        }
        if (dedicatoria) {
            link_red += ("&ded=" + encodeURIComponent(dedicatoria));
        }
        if (ciudad_nacimiento) {
            link_red += ("&ciu_nac=" + encodeURIComponent(ciudad_nacimiento));
        }
        if (cancion_favorita) {
            link_red += ("&can_fav=" + encodeURIComponent(cancion_favorita));
        }
        if (comida_favorita) {
            link_red += ("&com_fav=" + encodeURIComponent(comida_favorita));
        }
        if (juguete_favorito) {
            link_red += ("&jug_fav=" + encodeURIComponent(juguete_favorito));
        }
        if (actividad_favorita) {
            link_red += ("&act_fav=" + encodeURIComponent(actividad_favorita));
        }
        if (biografia) {
            link_red += ("&bio=" + encodeURIComponent(biografia));
        }

        for (var i = 0; i < imgs_arr.length; i++) {
            if (imgs_arr[i] > 0) {
                link_red += ("&img_" + i + "=" + encodeURIComponent(imgs_arr[i]));
            }
        }

        let libro_nombre = "book";
        if (libro_sel == 2) {
            libro_nombre = "book-2"
        }

        window.location.href = "/" + libro_nombre + "/index.php?" + link_red;

    } else {
        alert(mensaje_subir_error);
    }



});




/*SUBIR IMAGENES MODALES*/
</script>
<style>
.texto-agregar {
    font-family: "itckrist";
}

.libro-img-wrapp {
    position: relative;
    overflow: hidden;
}

.libro-img {
    position: relative;
    z-index: 2;
    width: 100%;
    min-width: 250px;
}

.texto-agregar {
    min-width: 0;
    position: absolute;
    font-size: 0.85vw;
    z-index: 3;
}
    
</style>

<?php get_template_part( 'footer', 'en' ); ?>