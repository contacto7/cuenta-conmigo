<?php /* Template Name: Inicio */ ?>
<?php get_header(); ?>
<main id="content">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <link rel="stylesheet" href="../wp-content/themes/generic/css/bootstrap-mix.css">
        <link href="../wp-content/themes/generic/css/croppie.css" rel="stylesheet">
        <link rel="stylesheet" href="/wp-content/themes/generic/css/slick.css" type="text/css" media="all">
        <link rel="stylesheet" href="/wp-content/themes/generic/css/slick-theme.css" type="text/css" media="all">
        <link href="../wp-content/themes/generic/css/bootstrap-datetimepicker.css" rel="stylesheet">
        <div class="entry-content">
            <div class="crea-libro seccion marco">
                <div class="titulo-pagina seccion-inicio">
                    <h1 class="tit-inicio-mobile">
                        Crea tu cuento personalizado en 1 minuto
                    </h1>
                </div>
                <div class="banner-ini-wrapp banner-principal">
                    <div class="banner estrecho">
                        <div class="banner-ini-inner">
                            <div class="single-item">
                                 <?php
                                if(get_post_meta( 6, '_inicio_imagen_1', true )){
                                ?>
                                <div class="carousel-inicio-item" data-libro="1">
                                    <img class="img-carousel-inicio"
                                        alt="cuentos personalizados para niños"
                                        src="<?php echo get_post_meta( 6, '_inicio_imagen_1', true ); ?>">
                                </div>
                                 <?php
                                }
                                ?>
                                 <?php
                                if(get_post_meta( 6, '_inicio_imagen_2', true )){
                                ?>
                                <div class="carousel-inicio-item" data-libro="2">
                                    <img class="img-carousel-inicio"
                                        alt="cuentos personalizados para niños"
                                        src="<?php echo get_post_meta( 6, '_inicio_imagen_2', true ); ?>">
                                </div>
                                 <?php
                                }
                                ?>
                                 <?php
                                if(get_post_meta( 6, '_inicio_imagen_3', true )){
                                ?>
                                <div class="carousel-inicio-item" data-libro="3">
                                    <img class="img-carousel-inicio"
                                        alt="cuentos personalizados para niños"
                                        src="<?php echo get_post_meta( 6, '_inicio_imagen_3', true ); ?>">
                                </div>
                                 <?php
                                }
                                ?>
                                ?>
                                 <?php
                                if(get_post_meta( 6, '_inicio_imagen_4', true )){
                                ?>
                                <div class="carousel-inicio-item" data-libro="3">
                                    <img class="img-carousel-inicio"
                                        alt="cuentos personalizados para niños"
                                        src="<?php echo get_post_meta( 6, '_inicio_imagen_4', true ); ?>">
                                </div>
                                 <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="btn-car-in prev" aria-label="Previous"><i class="arrow left"></i></div>
                    <div class="btn-car-in next" aria-label="Next"><i class="arrow right"></i></div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <a class="btn-crearm btn-crearm-empieza" href="#">
                            EMPIEZA AHORA
                        </a>
                    </div>
                </div>
                <div class="btn-pasosm-wrapp">
                    <div class="btn-pasosm-inn">

                        <div class="btnclosem-wrapp">
                            <div class="btnclosem-inn">
                                <div class="btnclosem">X</div>
                            </div>
                        </div>

                        <div class="slidem-wrapp">

                            <div class="slidem-inn" id="slidem-inn-1">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="titm-wrapp">
                                    <div class="titm-inn">
                                        ¿Para quién es el cuento?
                                    </div>
                                </div>

                                <div class="inpm-wrapp">
                                    <div class="inpm-inn">
                                        <input type="text" class="inpm" placeholder="Nombre" style="margin: 0 ">
                                    </div>
                                </div>

                                <div class="titm-wrapp">
                                    <div class="titm-inn">
                                        Selecciona si es
                                    </div>
                                </div>

                                <div class="btnsexom-wrapp">
                                    <div class="btnsexom-inn">
                                        <div class="btnsexom btnsexom-h sexo-el" data-sexo="h">
                                            <img src="/wp-content/imagenes/nino-icon.png" class="sexoicon-mbh"
                                                alt="logo de Cuenta conmigo" data-imgsexo="h">
                                            <div class="sexodscr-mbh">Niño</div>
                                        </div>
                                        <div class="btnsexom btnsexom-m sexo-el" data-sexo="m">
                                            <img src="/wp-content/imagenes/nina-icon.png" class="sexoicon-mbh"
                                                alt="logo de Cuenta conmigo" data-imgsexo="m">
                                            <div class="sexodscr-mbh">Niña</div>
                                        </div>
                                    </div>
                                </div>



                            </div>
                            <div class="slidem-inn" id="slidem-inn-2" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="titm-wrapp">
                                    <div class="titm-inn">
                                        Ahora, elige el cuento de <span class="nombrem"></span>
                                    </div>
                                </div>

                                <div class="banner-ini-wrapp biw-m">
                                    <div class="banner-modal estrecho" style="height: inherit; max-width: 100%">
                                        <div class="banner-ini-inner">
                                            <div class="single-item2">
                                                <div class="carousel-inicio-item">
                                                    <div class="ciimimg-wrapp">
                                                        <div class="libro-hoja hoja-adelante">
                                                            <div class="libro-img-wrapp" style="overflow:hidden">
                                                                <img class="libro-img"
                                                                    src="../wp-content/themes/generic/img_lib/595px/h/1.jpg"
                                                                    style="position: relative">
                                                                <div class="texto-agregar p1-cor" id="p1-1" style="top: 19%">
                                                                    <div id="p1-1-1"><span
                                                                            class="nombrem nombrem-coronacion"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ciim-wrapp">
                                                        <div class="ciim-tit">La coronación</div>
                                                        <div class="ciim-dscr">Una fantástica aventura de amistad,
                                                            valentía y
                                                            magia. En el día de tu coronación un feroz
                                                            dragón ha raptado a tu mejor amigo: el gato
                                                            Tommy. ¿Podrás rescatarlo? </div>
                                                    </div>
                                                </div>
                                                <div class="carousel-inicio-item">
                                                    <div class="ciimimg-wrapp-2">
                                                        <div class="libro-hoja hoja-adelante">
                                                            <div class="libro-img-wrapp" style="overflow:hidden">
                                                                <img class="libro-img-2"
                                                                    src="../wp-content/themes/generic/img_lib_2/595px/h/libro-2-banner-creacion.png"
                                                                    style="position: relative">
                                                                <div class="texto-agregar" id="p1-1-adv" style="top: 21%;">
                                                                    <div id="p1-1-1-adv">
                                                                        de <span class="nombrem nombrem-browm"
                                                                            style="rgb(79,0,0) !important"></span>
                                                                        en<br>
                                                                        Machu Picchu
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ciim-wrapp">
                                                        <div class="ciim-tit">Aventura en Machu Picchu</div>
                                                        <div class="ciim-dscr">
                                                            Viaja en el tiempo a una emocionante aventura en Machu Picchu. Un terremoto se aproxima. Para salvar la ciudad, debes encontrar los animales sagrados de los incas.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-crearm-wrapp">
                                            <div class="btn-crearm-inn">
                                                <label class="btn-crearm btn-libro-elegido"
                                                    onclick="jQuery( '.btnsgtem' ).click();">
                                                    ELEGIR CUENTO
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="btn-car-in prev" aria-label="Previous"><i class="arrow left"></i></div>
                                    <div class="btn-car-in next" aria-label="Next"><i class="arrow right"></i></div>
                                </div>
                            </div>

                            <div class="slidem-inn" id="slidem-inn-3" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">
                                        <img class="img-pers-in capucha-nino"
                                            src="../wp-content/themes/generic/img_lib/595px/h/velo-2.png"
                                            style="width: 200px;">
                                    </div>
                                </div>
                                <div class="titm-wrapp">
                                    <div class="titm-inn">
                                        ¡Aquí subiremos las fotos!<br>Sube una foto de <span class="nombrem"></span>
                                    </div>
                                </div>
                                <details class="detalis-ver-consejos">
                                    <summary>Ver consejos</summary>
                                    <p style="text-align:center">
                                        Usa una foto en alta resolución (mínimo<br>
                                        300KB y máximo 2 MB). El rostro del<br>
                                        protagonista debe estar totalmente de frente.<br>
                                        Asegúrate que la foto tenga buena<br>
                                        iluminación y no esté borrosa.
                                        <img class="consejo-img-nino"
                                            src="../wp-content/themes/generic/img/tips-image.png">
                                    </p>
                                </details>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <label class="btn-crearm" for="upload_image">
                                            SUBIR FOTO
                                        </label>
                                        <input style="display: none" type="file" name="upload_image"
                                            id="upload_image" />
                                    </div>
                                </div>

                            </div>

                            <div class="slidem-inn" id="slidem-inn-4" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">

                                        <div class="row">
                                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                                <div class="descr-ajustar-img dscr-r-red">
                                                    Rotar izq./der.
                                                </div>
                                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo">-90°</div>
                                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo">+90°</div>
                                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo"
                                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                                    aria-valuenow="50">
                                            </div>
                                            <div class="col-xs-12 text-center">
                                                <div class="descr-ajustar-img dscr-a-red">
                                                    Reducir/ Ampliar
                                                </div>
                                                <div id="image_demo" style="width:100%; margin-top:80px">
                                                    <div class="img-cropp-imgdel imgcrp-nino"><img class="capucha-nino"
                                                            src="../wp-content/themes/generic/img_lib/595px/h/velo-2.png">
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="titm-inn" style="font-weight:bold;">
                                                Encaja la foto de <span class="nombrem"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <a href="#" class="btn-crearm crop_image_inicio" data-id="0" for="upload_image">
                                            SIGUIENTE FOTO
                                        </a>
                                        <a class="btn-outline-naranja" href="#"
                                            onclick="jQuery( '.btnantem' ).click();">
                                            CAMBIAR FOTO
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="slidem-inn" id="slidem-inn-5" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">
                                        <img class="img-pers-in"
                                            src="../wp-content/themes/generic/img_lib/595px/veloreina-2.png"
                                            id="capucha-madre-pre">
                                    </div>
                                </div>
                                <div class="titm-wrapp">
                                    <div class="titm-inn">
                                        Sube una foto de mamá (opcional)
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <label class="btn-crearm" for="upload_image4">
                                            SUBIR FOTO
                                        </label>
                                        <input style="display: none" type="file" name="upload_image4"
                                            id="upload_image4" />
                                        <a class="btn-outline-naranja" href="#"
                                            onclick="jQuery( '.btnsgtem' ).click();jQuery( '.btnsgtem' ).click();">
                                            SALTAR PASO
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="slidem-inn" id="slidem-inn-6" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">

                                        <div class="row">
                                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                                <div class="descr-ajustar-img dscr-r-red">
                                                    Rotar izq./der.
                                                </div>
                                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo4">-90°</div>
                                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo4">+90°</div>
                                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo4"
                                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                                    aria-valuenow="50">
                                            </div>
                                            <div class="col-xs-12 text-center">
                                                <div class="descr-ajustar-img dscr-a-red">
                                                    Reducir/ Ampliar
                                                </div>
                                                <div id="image_demo4"
                                                    style="width:100%; margin-top:20px;position: relative">
                                                    <div class="img-cropp-imgdel imgcrp-madre"><img
                                                            src="../wp-content/themes/generic/img_lib/595px/veloreina-2.png">
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="titm-inn" style="font-weight:bold;">
                                                Encaja la foto de mamá
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <a href="#" class="btn-crearm crop_image_inicio" data-id="3">
                                            SIGUIENTE FOTO
                                        </a>
                                        <a class="btn-outline-naranja" href="#"
                                            onclick="jQuery( '.btnantem' ).click();">
                                            CAMBIAR FOTO
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="slidem-inn" id="slidem-inn-7" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">
                                        <img class="img-pers-in"
                                            src="../wp-content/themes/generic/img_lib/595px/velorey-2.png"
                                            id="capucha-padre-pre">
                                    </div>
                                </div>
                                <div class="titm-wrapp">
                                    <div class="titm-inn">
                                        Sube una foto de papá (opcional)
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <label class="btn-crearm" for="upload_image5">
                                            SUBIR FOTO
                                        </label>
                                        <input style="display: none" type="file" name="upload_image5"
                                            id="upload_image5" />
                                        <a class="btn-outline-naranja" href="#"
                                            onclick="jQuery( '.btnsgtem' ).click();jQuery( '.btnsgtem' ).click();">
                                            SALTAR PASO
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="slidem-inn" id="slidem-inn-8" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">

                                        <div class="row">
                                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                                <div class="descr-ajustar-img dscr-r-red">
                                                    Rotar izq./der.
                                                </div>
                                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo5">-90°</div>
                                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo5">+90°</div>
                                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo5"
                                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                                    aria-valuenow="50">
                                            </div>
                                            <div class="col-xs-12 text-center">
                                                <div class="descr-ajustar-img dscr-a-red">
                                                    Reducir/ Ampliar
                                                </div>
                                                <div id="image_demo5" style="width:100%; position: relative">
                                                    <div class="img-cropp-imgdel imgcrp-padre"><img
                                                            src="../wp-content/themes/generic/img_lib/595px/velorey-2.png">
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="titm-inn" style="font-weight:bold;">
                                                Encaja la foto de papá
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <a href="#" class="btn-crearm crop_image_inicio" data-id="4">
                                            SIGUIENTE FOTO
                                        </a>
                                        <a class="btn-outline-naranja" href="#"
                                            onclick="jQuery( '.btnantem' ).click();">
                                            CAMBIAR FOTO
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="slidem-inn" id="slidem-inn-9" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">
                                        <div class="cuadro-imgsubir" id="retrato-familiar-pre"></div>
                                    </div>
                                </div>
                                <div class="titm-wrapp">
                                    <div class="titm-inn">
                                        Sube una foto familiar (opcional)
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <label class="btn-crearm" for="upload_image2">
                                            SUBIR FOTO
                                        </label>
                                        <input style="display: none" type="file" name="upload_image2"
                                            id="upload_image2" />
                                        <a class="btn-outline-naranja" href="#"
                                            onclick="jQuery( '.btnsgtem' ).click();jQuery( '.btnsgtem' ).click();">
                                            SALTAR PASO
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="slidem-inn" id="slidem-inn-10" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">

                                        <div class="row">
                                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                                <div class="descr-ajustar-img dscr-r-red">
                                                    Rotar izq./der.
                                                </div>
                                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo2">-90°</div>
                                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo2">+90°</div>
                                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo2"
                                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                                    aria-valuenow="50">
                                            </div>
                                            <div class="col-xs-12 text-center cropp-wrapp-rosa">
                                                <div class="descr-ajustar-img dscr-a-red">
                                                    Reducir/ Ampliar
                                                </div>
                                                <div id="image_demo2"
                                                    style="width:100%; margin-top:20px;position: relative">

                                                </div>
                                            </div>
                                            <div class="titm-inn" style="font-weight:bold;">
                                                Encaja la foto familiar
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <a href="#" class="btn-crearm crop_image_inicio" data-id="1">
                                            SIGUIENTE FOTO
                                        </a>
                                        <a class="btn-outline-naranja" href="#"
                                            onclick="jQuery( '.btnantem' ).click();">
                                            CAMBIAR FOTO
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="slidem-inn" id="slidem-inn-11" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">
                                        <div class="cuadro-imgsubir" id="retrato-nino-pre"></div>
                                    </div>
                                </div>
                                <div class="titm-wrapp">
                                    <div class="titm-inn">
                                        Sube un retrato de <span class="nombrem"></span> (opcional)
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <label class="btn-crearm" for="upload_image3">
                                            SUBIR FOTO
                                        </label>
                                        <input style="display: none" type="file" name="upload_image3"
                                            id="upload_image3" />
                                        <a class="btn-outline-naranja" href="#"
                                            onclick="jQuery( '.btnsgtem' ).click();jQuery( '.btnsgtem' ).click();">
                                            SALTAR PASO
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="slidem-inn" id="slidem-inn-12" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">

                                        <div class="row">
                                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                                <div class="descr-ajustar-img dscr-r-red">
                                                    Rotar izq./der.
                                                </div>
                                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo3">-90°</div>
                                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo3">+90°</div>
                                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo3"
                                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                                    aria-valuenow="50">
                                            </div>
                                            <div class="col-xs-12 text-center cropp-wrapp-rosa">
                                                <div class="descr-ajustar-img dscr-a-red">
                                                    Reducir/ Ampliar
                                                </div>
                                                <div id="image_demo3"
                                                    style="width:100%; margin-top:20px;position: relative">

                                                </div>
                                            </div>
                                            <div class="titm-inn" style="font-weight:bold;">
                                                Encaja el retrato de <span class="nombrem"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <a href="#" class="btn-crearm crop_image_inicio" data-id="2">
                                            SIGUIENTE PASO
                                        </a>
                                        <a class="btn-outline-naranja" href="#"
                                            onclick="jQuery( '.btnantem' ).click();">
                                            CAMBIAR FOTO
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="slidem-inn" id="slidem-inn-13" style="display: none">
                                <div class="estrellasm-wrapp">
                                    <div class="estrellasm-inn">
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">1</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">2</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">3</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">4</div>
                                        </div>
                                        <div class="estrellasm">
                                            <div class="star-five star-five-active"></div>
                                            <div class="star-number">5</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="titm-wrapp">
                                    <div class="titm-inn">
                                        Finalmente, personaliza la dedicatoria y otros detalles (opcional)
                                    </div>
                                </div>

                                <div class="img-pers-wrapp">
                                    <div class="img-pers-inn">

                                        <div class="row">
                                            <form action="" class="form-info-libroinic">
                                                <div class="campo">
                                                    <p>Dedicatoria:</p>
                                                    <textarea type="text" id="dedicatoria-libro" rows="3"
                                                        class="inputs-texto" data-cuenta="cuentachar-ded"
                                                        data-max="140">Querido @niño,
¡La verdadera magia para hacer realidad todos tus sueños está en tu interior!</textarea>
                                                    <div>(<span id="cuentachar-ded">0</span>/140 caracteres)</div>
                                                </div>
                                                
                                                
                                                <details class="detalis-ver-consejos campo">
                                                    <summary>Detalles de la historia</summary>
                                                    <div>
                                                        
                                                        
                                                        
                                                        
                                                        
                                                <div class="form-group input-group date campo"
                                                    id="fecha-formcont-wrapp">
                                                    <p>Cumpleaños del niño:</p>
                                                    <input type="text" class="" id="fecha-formcont"
                                                        onchange="verificarfecha('fecha-formcont', 'fecha-formcont-wrapp')"
                                                        value="">
                                                    <span class="input-group-addon" id="btn-dp-1"
                                                        style="visibility: hidden;position: relative;left:5px;top: -78px;display: inline-block;">
                                                    </span>
                                                </div>
                                                <div class="campo">
                                                    <p>Nombre del padre:</p>
                                                    <input type="text" id="nombre-padre-libro" value="">
                                                </div>
                                                <div class="campo">
                                                    <p>Nombre de la madre:</p>
                                                    <input type="text" id="nombre-madre-libro" value="">
                                                </div>
                                                <div class="campo">
                                                    <p>Gato mascota:</p>
                                                    <select class="form-control campo-select"
                                                        id="sexmasc-familiar-libro">
                                                        <option value="1">Gatito</option>
                                                        <option value="2">Gatita</option>
                                                    </select>
                                                    <input type="text" id="gato-madre-libro" value="">
                                                </div>
                                                <div class="campo">
                                                    <p>Familiar más cercana:</p>
                                                    <select class="form-control campo-select" id="tipo-familiar-libro">
                                                        <option value="1">Tía</option>
                                                        <option value="2">Abuelita</option>
                                                        <option value="3">Prima</option>
                                                    </select>
                                                    <input type="text" id="nombre-familiar-libro" value="Nichola">
                                                </div>
                                                <div class="campo">
                                                    <p>Biografía:</p>
                                                    <p class="form-subcampo">Ciudad de nacimiento:</p>
                                                    <input type="text" id="ciudad-nacimiento-libro" value="">
                                                    <p class="form-subcampo">Canción favorita:</p>
                                                    <input style="text-transform:lowercase" type="text"
                                                        id="cancion-favorita-libro" value="">
                                                    <p class="form-subcampo">Comida favorita:</p>
                                                    <input style="text-transform:lowercase" type="text"
                                                        id="comida-favorita-libro" value="">
                                                    <p class="form-subcampo">Juguete favorito:</p>
                                                    <input style="text-transform:lowercase" type="text"
                                                        id="juguete-favorito-libro" value="">
                                                    <p class="form-subcampo">Actividad favorita:</p>
                                                    <input style="text-transform:lowercase" type="text"
                                                        id="actividad-favorita-libro" value="">
                                                    <p class="form-subcampo">Adicional a biografía:</p>
                                                    <textarea type="text" id="biografia-libro" rows="2"
                                                        class="inputs-texto" data-cuenta="cuentachar-bio"
                                                        data-max="30"></textarea>
                                                    <div>(<span id="cuentachar-bio">0</span>/30 caracteres)</div>
                                                </div>
                                                        
                                                        
                                                        
                                                    </div>
                                                </details>
                                            </form>


                                        </div>
                                    </div>
                                </div>
                                <div class="btn-crearm-wrapp">
                                    <div class="btn-crearm-inn">
                                        <a href="#" class="btn-crearm" id="ver-cuento">
                                            VER CUENTO
                                        </a>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="btnsgtem-wrapp">
                            <div class="btnsgtem-inn" data-current="1">
                                <div class="btnmovem btnsgtem" data-current="1" data-next="2">Siguiente &#10230;</div>
                                <div class="btnmovem btnantem" data-current="2" data-next="1">&#10229; Volver al paso
                                    anterior</div>
                            </div>
                        </div>



                    </div>
                </div>
                <div class="nube-inter adorno">
                    <img src="/wp-content/imagenes/Nubes1.png" alt="nubes">

                </div>
            </div>
            <div class="dale-play seccion marco">
                <div class="unicornio"></div>
                <h1 class="titulo">Cuentos personalizados protagonizados por la familia</h1>
                <p class="introduccion">Divertidos de crear, incluyen el nombre y la foto del niño, mamá y papá en la
                    historia.</p>
                <div class="video"
                    style="background:#000;webkit-border-radius: 20px;-moz-border-radius: 20px;border-radius: 20px;margin:0 auto;overflow:hidden;">
                    <?php $value = get_post_meta( 6, '_inicio_video', true );
                            $inicio=strpos($value,"?v=")+3;
                            $auto= substr($value,$inicio,strlen($value));
                            $str = str_replace("watch?v=", "embed/", $value)."?modestbranding=1&autohide=1&showinfo=0&autoplay=1&mute=1&controls=0&rel=0&loop=1&playlist=".$auto    ;
                            ?>

                    <iframe width="600" height="350" src="<?php echo $str;?>" frameborder="0"></iframe>
                </div>
                <div class="nina-d adorno">
                    <img src="/wp-content/imagenes/Niña naranja.png" alt="niña naranja">
                </div>
                <div class="unicornio-d adorno">
                    <img src="/wp-content/imagenes/Unicornio.png" alt="niña naranja">

                </div>
                <div class="nube-inter adorno">
                    <img src="/wp-content/imagenes/Nubes2.png" alt="nubes">

                </div>
            </div>
            <div class="libros-interesantes seccion marco">
                <h1 class="titulo">Descubre nuestras historias</h1>
                <!-- <p class="introduccion">Aquí encontrarás todo nuestro catálogo. ¡Muy pronto te sorprenderemos con nuevas
                            e increíbles historias!</p> -->
                <div class="libros estrecho">
                    <div class="libro estrecho">
                        <div class="textual">
                            <h2 class="nombre">
                                La coronación
                            </h2>
                            <p class="descripcion">
                                Sé el protagonista de una aventura fantástica de amistad, valentía y magia. Es el día de
                                tu coronación y tus papás, los reyes han preparado una fiesta espectacular en palacio.
                                Pero… ¡Oh No! Un malvado dragón se las ha ingeniado para raptar a tu mejor amigo, el
                                gato Tommy. ¿Podrás rescatarlo y devolver la tranquilidad al reino?
                            </p>
                        </div>
                        <div class="miniatura"
                            style="background-image: url(/wp-content/uploads/2020/04/Coronacion.png);">

                        </div>
                    </div>
                    <div class="libro estrecho">
                        <div class="miniatura" style="background-image: url(/wp-content/uploads/2020/04/Aventura.png);">

                        </div>
                        <div class="textual">
                            <h2 class="nombre">
                                Aventura en Machu Picchu </h2>
                            <p class="descripcion">
                                Viaja en el tiempo y vive una emocionante aventura en Machu Picchu.
                                Un terrible terremoto ocurrirá pronto. Para salvar la ciudad, debes encontrar
                                los animales sagrados de los incas. ¡Únete al cóndor, al puma y
                                a la serpiente en esta misión!
                            </p>
                        </div>

                    </div>

                </div>
                <div class="nina-l1 adorno">
                    <img src="/wp-content/imagenes/Niña Lila.png" alt="niña lila">
                </div>
                <div class="nube-inter adorno">
                    <img src="/wp-content/imagenes/Nubes3.png" alt="nubes">

                </div>
                <!-- <div class="nube-l2 adorno">

                </div>
                <div class="nube-l3 adorno">

                </div>
                <div class="nube-l4 adorno">

                </div> -->
            </div>
            <div class="testimonios seccion marco">
                <h1 class="titulo">Testimonios</h1>
                <div class="lista-testimonios estrecho marco">
                    <div class="testimonio">
                        <div class="foto-testigo testigo-1"></div>
                        <h2 class="testigo">Isela Vivanco</h2>
                        <p class="comentario">
                            Mi hija mayor se emocionó un montón al verse como protagonista, y hasta hoy saca su
                            cuento y el de sus hermanitos para leerlos cuando viene a casa una amiga suya, los tíos
                            y/o primos, por otro lado, las historias son lindas y los cuentos de excelente calidad.
                        </p>
                    </div>
                    <div class="testimonio">
                        <div class="foto-testigo testigo-2"></div>
                        <h2 class="testigo">Sandra Antara</h2>
                        <p class="comentario">
                            ¡Ver tu cara y la de tu hijo como protagonistas de un cuento es una sensación muy
                            emocionante! Me encanta este proyecto, sus libros son hermosos. ¡Mi hijito ama su cuento
                            personalizado y yo también!
                        </p>
                    </div>
                    <div class="testimonio">
                        <div class="foto-testigo testigo-3"></div>
                        <h2 class="testigo">Rossy Lena Pérez</h2>
                        <p class="comentario">
                            Encantadísima con el excelente trabajo recibido: perfecto, original, sin palabras. ¡Por
                            supuesto que voy por más, para perennizar las celebraciones de mis acontecimientos
                            importantes!
                        </p>
                    </div>

                    <div class="nube-t1 adorno">
                        <img src="/wp-content/imagenes/nube1izq.png" alt="nubes">
                    </div>
                    <div class="nube-t2 adorno">
                        <img src="/wp-content/imagenes/nube2cent.png" alt="nubes">
                    </div>
                    <div class="nube-t3 adorno">
                        <img src="/wp-content/imagenes/nube3dere.png" alt="nubes">
                    </div>
                </div>
                <div class="nino-t1 adorno">
                    <img src="/wp-content/imagenes/Niño azul.png" alt="nubes">
                </div>
                <div class="nino-t2 adorno">
                    <img src="/wp-content/imagenes/Niño naranja.png" alt="nubes">
                </div>
                <div class="nube-inter adorno">
                    <img src="/wp-content/imagenes/Nubes4.png" alt="nubes">
                </div>

            </div>
            <div class="preguntas seccion">
                <h1 class="titulo">Preguntas frecuentes</h1>
                <div class="lista-preguntas">
                    <div class="pregunta">
                        <h2 class="duda">
                            ¿Cómo funciona?
                        </h2>
                        <p class="respuesta">
                            ¡Muy fácil! Escribe el nombre del protagonista y elige tu historia favorita. Luego, sube las
                            fotos del niño/a, mamá y papá y mira como aparecen automáticamente como protagonistas del
                            cuento, ¡parece magia! Finalmente, inspírate y escribe una dedicatoria que deje huella.
                    </div>
                    <div class="pregunta">
                        <h2 class="duda">
                            ¿Para quién es?
                        </h2>
                        <p class="respuesta">
                            Las historias son para niños de entre 1 a 8 años, aunque muchos de nuestros clientes nos los
                            piden para niños mayores o para leerles a sus hijitos desde la pancita de mamá.
                        </p>
                    </div>
                    <div class="pregunta">
                        <h2 class="duda">
                            ¿Cómo es?
                        </h2>
                        <p class="respuesta">
                            Nuestros libros están artísticamente ilustrados e impresos en la mejor calidad. El formato
                            tapa blanda se imprime bajo la última tecnología de impresión digital. El formato tapa dura
                            está pensado para durar pues viene con una cubierta rígida para un acabado más lujoso.
                        </p>
                    </div>
                    <div class="pregunta">
                        <h2 class="duda">
                            ¿Cuánto tarda?
                        </h2>
                        <p class="respuesta">
                            Cada uno de nuestros cuentos es un regalo único, impreso especialmente para tu pequeño con
                            la última tecnología y un acabado de primera. El tiempo total de entrega depende de tu
                            ubicación, manejamos normalmente 10-20 días útiles en la entrega.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </article>
    <!--MODALES SUBIR FOTO-->
    <!--MODALES SUBIR FOTO-->
    <?php endwhile; endif; ?>
</main>
<script src="../wp-content/themes/generic/js/princ.js?ver=<?php echo INOLOOP_VERSION; ?>"></script>
<script src="../wp-content/themes/generic/js/croppie.js"></script>
<script src="../wp-content/themes/generic/js/moment-with-locales.js"></script>
<script src="../wp-content/themes/generic/js/bootstrap-datetimepicker.js"></script>
<script>
var sexo_libro;
var libro_sel = 1;
var nombre_nino;
var imgs_arr = [0, 0, 0, 0, 0];
let $image_crop3;
let $image_crop5;

jQuery(".btn-crearm-empieza").on("click", function() {
    jQuery(".btn-pasosm-wrapp").css("display", "flex");
    jQuery("body").css("overflow", "hidden");
});
jQuery(".btnclosem").on("click", function() {
    jQuery(".btn-pasosm-wrapp").css("display", "none");
    jQuery("body").css("overflow", "inherit");
});
/*Función que se ejecuta cuando se presion en siguiente,
  en los pasos para crear un nuevo cuento
*/
jQuery(".btnsgtem").on("click", function() {
    console.log("Presionó");

    let current_slide = parseInt(jQuery(".btnsgtem-inn").attr('data-current'));
    let next_slide = current_slide + 1;
    console.log(next_slide);

    if (current_slide == 8 && libro_sel == 2) {
        jQuery("#slidem-inn-8").hide();
        current_slide = 10;
        next_slide = 11;
    }

    if (current_slide == 1) {
        let nombre = jQuery(".inpm").val();
        if (!(sexo_libro && nombre)) {
            return alert("Por favor ingrese un nombre y seleccione un sexo");
        }

        nombre_nino = nombre;
        jQuery(".nombrem").html(nombre);
    }

    console.log("mostrar: " + next_slide);
    console.log("esconder: " + current_slide);

    jQuery("#slidem-inn-" + current_slide).hide();
    jQuery("#slidem-inn-" + next_slide).show();

    if (current_slide == 1) {
        console.log("slider");

        if (!jQuery(".single-item2").hasClass("slick-initialized")) {
            jQuery('.single-item2').slick({
                infinite: false,
                autoplay: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                prevArrow: jQuery('.slidem-wrapp .prev'),
                nextArrow: jQuery('.slidem-wrapp .next'),
            });

            jQuery('.single-item2').on('afterChange', function() {
                var dataId = jQuery('.single-item2 .slick-current').attr("data-slick-index");

                libro_sel = ++dataId;
            });
            mostrarDatosLibro(libro_sel, sexo_libro, nombre_nino);
        }

        /*
        $image_crop3 = croppieImagenRetrato(libro_sel);
        $image_crop5 = croppieImagenPapa(libro_sel);

        mostrarDatosLibro(libro_sel, sexo_libro, nombre_nino);
        */
        console.log(libro_sel);
    }


    jQuery(".btnantem").show();

    jQuery(".btnsgtem-inn").attr('data-current', next_slide);



});

jQuery(document).ready(function() {
    jQuery('.single-item').slick({
        prevArrow: jQuery('.banner-principal .prev'),
        nextArrow: jQuery('.banner-principal .next'),
    });

});
/*Función que se ejecuta cuando se presiona en anterior,
  en los pasos para crear un nuevo cuento
*/
jQuery(".btn-libro-elegido").on("click", function() {

    $image_crop3 = croppieImagenRetrato(libro_sel);
    $image_crop5 = croppieImagenPapa(libro_sel);

    mostrarDatosLibro(libro_sel, sexo_libro, nombre_nino);
});
jQuery(".btnantem").on("click", function() {
    console.log("Presionó");

    let current_slide = parseInt(jQuery(".btnsgtem-inn").attr('data-current'));
    let next_slide = current_slide - 1;

    if (next_slide == 1) {
        jQuery(".btnantem").hide();
    } else {
        jQuery(".btnantem").show();
    }

    console.log("mostrar: " + next_slide);
    console.log("esconder: " + current_slide);

    jQuery("#slidem-inn-" + current_slide).hide();
    jQuery("#slidem-inn-" + next_slide).show();

    jQuery(".btnsgtem-inn").attr('data-current', next_slide);
});
jQuery(".sexo-el").on("click", function() {
    jQuery(".sexo-el").css("color", "#fff");
    jQuery('.sexoicon-mbh').css("filter", "inherit");
    var sexo = jQuery(this).attr("data-sexo");
    //variable global
    sexo_libro = sexo;

    jQuery(".lnk-libro").attr("data-sexo", sexo);
    jQuery('[data-sexo="' + sexo + '"]').css("color", "rgb(0, 0, 0)");
    jQuery('[data-imgsexo="' + sexo + '"]').css("filter", "invert(1)");

    let src_capucha = './wp-content/themes/generic/img_lib/595px/' + sexo + '/velo-2.png';
    let src_caratula = './wp-content/themes/generic/img_lib/595px/' + sexo + '/1.jpg';

    let nombre = jQuery(".inpm").val();

    jQuery('.capucha-nino').attr("src", src_capucha);
    jQuery('.libro-img').attr("src", src_caratula);

    if (sexo == 'm') {
        jQuery('.capucha-nino').closest('.imgcrp-nino').css({
            "top": "-30px",
            "height": "330px",
        });
        jQuery('.capucha-nino').closest('#image_demo').css("margin-top", "25px");
        jQuery('.libro-img-wrapp .nombrem-coronacion').css("color", "#653f88");
    }

});
jQuery(".lnk-libro").on("click", function() {

    var sexo = jQuery(this).attr("data-sexo");
    var nombre = jQuery("#input-crea-libro").val();
    var len_nombre = nombre.length;
    var regex = new RegExp(/^[a-zA-ZñÑ\s]+$/);
    var libro = parseInt(jQuery(".slick-active .carousel-inicio-item").attr("data-libro"));
    if(libro == 1){
       libro = "libro";
    console.log("Libro seleccionado: "+libro);
    }else if(libro == 2){
       libro = "libro-2";
    }
    
    
    if (len_nombre == 0) {
        alert("Debe ingresar un nombre");
    } else if (!regex.test(nombre)) {
        alert("Debe ingresar sólo letras.");
    } else if (len_nombre < 3) {
        alert(
            "El nombre es muy corto. Por favor contáctate con nosotros para poder brindarte una opción especial."
            );
    } else if (len_nombre > 10) {
        alert(
            "El nombre es muy largo. Por favor contáctate con nosotros para poder brindarte una opción especial."
            );
    } else if (!sexo) {
        alert("Debe seleccionar un sexo");
    } else {
        var link_href = "/"+libro+"/?nombre=" + nombre + "&sexo=" + sexo + "&tkn=0";
        window.location.href = link_href;
    }
});

/*SUBIR IMAGENES MODALES*/

$image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 200,
        type: 'square' //circle
    },
    boundary: {
        width: 300,
        height: 300
    },
    enableOrientation: true
});
$image_crop2 = $('#image_demo2').croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 270,
        type: 'square'
    },
    boundary: {
        width: 260,
        height: 330
    },
    enableOrientation: true
});
/*
DEPENDE DEL LIBRO, SE SELECCIONA EN croppieImagenRetrato(libro)
$image_crop3 = $('#image_demo3').croppie({
    enableExif: true,
    viewport: {
        width:205,
        height:295,
        type:'square'
    },
    boundary:{
        width:260,
        height:330
    },
    enableOrientation: true
});
*/
$image_crop4 = $('#image_demo4').croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 200,
        type: 'square'
    },
    boundary: {
        width: 300,
        height: 300
    },
    enableOrientation: true
});
/*
DEPENDE DEL LIBRO, SE SELECCIONA EN croppieImagenPapa(libro)
$image_crop5 = $('#image_demo5').croppie({
    enableExif: true,
    viewport: {
        width:200,
        height:200,
        type:'square'
    },
    boundary:{
        width:300,
        height:300
    },
    enableOrientation: true
});
*/
$('#upload_image').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
$('#upload_image2').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop2.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
$('#upload_image3').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop3.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
$('#upload_image4').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop4.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
$('#upload_image5').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop5.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
$('.crop_image_inicio').click(function(event) {
    $(this).css({
        "opacity": "0.5",
        "pointer-events": "none"
    });
    var num_img = parseInt($(this).attr("data-id"));
    var width_sz;
    var height_sz;
    var image_crop;
    switch (num_img) {
        case 0:
            image_crop = $image_crop;
            width_sz = 200;
            height_sz = 200;
            break;
        case 1:
            image_crop = $image_crop2;
            width_sz = 400;
            height_sz = 540;
            break;
        case 2:
            image_crop = $image_crop3;
            if (libro_sel == 1) {
                width_sz = 410;
                height_sz = 590;
            } else if (libro_sel == 2) {
                width_sz = 590;
                height_sz = 410;
            }
            break;
        case 3:
            image_crop = $image_crop4;
            width_sz = 200;
            height_sz = 200;
            break;
        case 4:
            image_crop = $image_crop5;
            if (libro_sel == 1) {
                width_sz = 200;
                height_sz = 200;
            } else if (libro_sel == 2) {
                width_sz = 150;
                height_sz = 200;
            }
            break;
        default:
            image_crop = $image_crop;
            break;
    }
    image_crop.croppie('result', {
        type: 'canvas',
        size: {
            width: width_sz,
            height: height_sz
        }
    }).then(function(response) {
        console.log("response: " + response);
        $.ajax({
            url: "/wp-content/themes/generic/upload.php",
            type: "POST",
            data: {
                "image": response
            },
            success: function(data) {
                $(".crop_image_inicio").css({
                    "opacity": "1",
                    "pointer-events": "inherit"
                });

                imgs_arr[num_img] = data;
                jQuery(".btnsgtem").click();

                console.log(imgs_arr);
            }
        });
    })
});



$('#ver-cuento').click(function(event) {
    var link_red = "";


    let nombre_nino = jQuery(".inpm").val();
    let sexo = sexo_libro;


    var fecha_nino = $("#fecha-formcont").val();
    var nombre_padre = $("#nombre-padre-libro").val();
    var nombre_madre = $("#nombre-madre-libro").val();
    var sexo_mascota = $("#sexmasc-familiar-libro").val();
    var nombre_mascota = $("#gato-madre-libro").val();
    var tipo_familiar = $("#tipo-familiar-libro").val();
    var nombre_familiar = $("#nombre-familiar-libro").val();

    var dedicatoria = $("#dedicatoria-libro").val();
    dedicatoria = dedicatoria.replace(/\r?\n/g, '<br />');

    //BIOGRAFIA();
    var ciudad_nacimiento = $("#ciudad-nacimiento-libro").val();
    var cancion_favorita = $("#cancion-favorita-libro").val();
    var comida_favorita = $("#comida-favorita-libro").val();
    var juguete_favorito = $("#juguete-favorito-libro").val();
    var actividad_favorita = $("#actividad-favorita-libro").val();
    var biografia = $("#biografia-libro").val();
    biografia = biografia.replace(/\r?\n/g, '<br />');

    if (verificarnombre(nombre_nino, "nombre del niño") && verificarnombre(nombre_padre, "nombre del padre") &&
        verificarnombre(nombre_madre, "nombre de la madre") && verificarnombre(nombre_mascota,
            "nombre de la mascota") && verificarnombre(nombre_familiar, "nombre de la familiar") &&
        verificardedicatoria(dedicatoria, "dedicatoria") && verificarbiografia(ciudad_nacimiento,
            "ciudad de nacimiento") && verificarbiografia(cancion_favorita, "canción favorita") &&
        verificarbiografia(comida_favorita, "comida favorita") && verificarbiografia(juguete_favorito,
            "juguete favorito") && verificarbiografia(actividad_favorita, "actividad favorita") &&
        verificarbiografia(biografia, "biografía")) {

        link_red += ("&nombre=" + encodeURIComponent(nombre_nino));
        link_red += ("&sexo=" + sexo);

        if (fecha_nino) {
            link_red += ("&fec_nin=" + encodeURIComponent(fecha_nino));
        }
        if (nombre_padre) {
            link_red += ("&nbr_pap=" + encodeURIComponent(nombre_padre));
        }
        if (nombre_madre) {
            link_red += ("&nbr_mad=" + encodeURIComponent(nombre_madre));
        }
        if (sexo_mascota) {
            link_red += ("&sex_gat=" + encodeURIComponent(sexo_mascota));
        }
        if (nombre_mascota) {
            link_red += ("&nbr_gat=" + encodeURIComponent(nombre_mascota));
        }
        if (tipo_familiar) {
            link_red += ("&tip_fam=" + encodeURIComponent(tipo_familiar));
        }
        if (nombre_familiar) {
            link_red += ("&nbr_fam=" + encodeURIComponent(nombre_familiar));
        }
        if (dedicatoria) {
            link_red += ("&ded=" + encodeURIComponent(dedicatoria));
        }
        if (ciudad_nacimiento) {
            link_red += ("&ciu_nac=" + encodeURIComponent(ciudad_nacimiento));
        }
        if (cancion_favorita) {
            link_red += ("&can_fav=" + encodeURIComponent(cancion_favorita));
        }
        if (comida_favorita) {
            link_red += ("&com_fav=" + encodeURIComponent(comida_favorita));
        }
        if (juguete_favorito) {
            link_red += ("&jug_fav=" + encodeURIComponent(juguete_favorito));
        }
        if (actividad_favorita) {
            link_red += ("&act_fav=" + encodeURIComponent(actividad_favorita));
        }
        if (biografia) {
            link_red += ("&bio=" + encodeURIComponent(biografia));
        }

        for (var i = 0; i < imgs_arr.length; i++) {
            if (imgs_arr[i] > 0) {
                link_red += ("&img_" + i + "=" + encodeURIComponent(imgs_arr[i]));
            }
        }

        let libro_nombre = "libro";
        if (libro_sel == 2) {
            libro_nombre = "libro-2"
        }

        window.location.href = "/" + libro_nombre + "/index.php?" + link_red;

    } else {
        alert(mensaje_subir_error);
    }



});




/*SUBIR IMAGENES MODALES*/
</script>
<style>
.texto-agregar {
    font-family: "itckrist";
}

.libro-img-wrapp {
    position: relative;
    overflow: hidden;
}

.libro-img {
    position: relative;
    z-index: 2;
    width: 100%;
    min-width: 250px;
}

.texto-agregar {
    min-width: 0;
    position: absolute;
    font-size: 0.85vw;
    z-index: 3;
}

#p1-1 {
    width: 100%;
    top: 18%;
    font-size: 3vw;
    text-align: center;
    font-family: "skater";
    color: rgb(0, 108, 181);
}

/*CUSTOM RANGE*/
input[type='range'] {
    display: block;
    width: 250px;
}

input[type='range']:focus {
    outline: none;
}

input[type='range'],
input[type='range']::-webkit-slider-runnable-track,
input[type='range']::-webkit-slider-thumb {
    -webkit-appearance: none;
}

input[type=range]::-webkit-slider-thumb {
    background-color: rgb(254, 205, 7);
    width: 20px;
    height: 20px;
    border: 3px solid rgb(254, 205, 7);
    border-radius: 50%;
    margin-top: -9px;
}

input[type=range]::-moz-range-thumb {
    background-color: rgb(254, 205, 7);
    width: 15px;
    height: 15px;
    border: 3px solid rgb(254, 205, 7);
    border-radius: 50%;
}

input[type=range]::-ms-thumb {
    background-color: rgb(254, 205, 7);
    width: 20px;
    height: 20px;
    border: 3px solid rgb(254, 205, 7);
    border-radius: 50%;
}

input[type=range]::-webkit-slider-runnable-track {
    background-color: rgb(254, 205, 7);
    height: 3px;
}

input[type=range]:focus::-webkit-slider-runnable-track {
    outline: none;
}

input[type=range]::-moz-range-track {
    background-color: rgb(254, 205, 7);
    height: 3px;
}

input[type=range]::-ms-track {
    background-color: rgb(254, 205, 7);
    height: 3px;
}

input[type=range]::-ms-fill-lower {
    background-color: HotPink
}

input[type=range]::-ms-fill-upper {
    background-color: black;
}

input[type=range].cr-slider::before {
    content: "\2D";
    color: #ffffff;
    background-color: #fecd07;
    font-size: 21px;
    border-radius: 100%;
    padding-top: 1px;
    padding-bottom: 6px;
    padding-left: 13px;
    padding-right: 13px;
    vertical-align: middle;
    position: relative;
    top: -3px;
}

input[type=range].cr-slider::after {
    content: "\2B";
    color: #ffffff;
    background-color: #fecd07;
    font-size: 21px;
    border-radius: 100%;
    padding-top: 0px;
    padding-bottom: 5px;
    padding-left: 9px;
    padding-right: 9px;
    vertical-align: middle;
    position: relative;
    top: -3px;
}

input[type=range].range-rotate::before {
    background-color: #fecd07;
    border-radius: 100%;
    background-image: url(../wp-content/themes/generic/img/rotate-24.png);
    background-repeat: no-repeat;
    background-size: 24px 24px;
    background-position: 6px 5px;
    display: inline-block;
    width: 35px;
    height: 35px;
    content: "";
}

input[type=range].range-rotate::after {
    background-color: #fecd07;
    border-radius: 100%;
    background-image: url(../wp-content/themes/generic/img/rotate-24.png);
    transform: rotateY(180deg);
    background-repeat: no-repeat;
    background-size: 24px 24px;
    background-position: 6px 5px;
    display: inline-block;
    width: 35px;
    height: 35px;
    content: "";
}

.range-rotate {
    margin: 0 auto;
    background-color: #fff;
}

/*CUSTOM RANGE*/
.form-info-libroinic select {
    width: 100%;
    padding: 5px;
    margin-bottom: -6px;
    border: 1px solid #ddd;
    margin-top: 10px;
    border-radius: 5px;
    color: #777;
}
    
#p1-1-1{
font-size: 8vw;
}
@media (min-width: 600px){
    #p1-1-1{
    font-size: 44px;
    }
}
</style>
<?php get_footer(); ?>