<?php /* Template Name: Somos */ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="titulo-pagina">
    <h1 style="color: #ff6400;
    font-size: 28px;"><?php the_title(); ?></h1>    
</div>
<main id="content">
    <div style="display:flex;justify-content:center;width:100%">
    <div class="parrafo estrecho">
        <?php the_content(); ?>
    </div>
    </div>
    <div class="familia" style="margin-bottom:40px">
            <img class="principe"src="/wp-content/imagenes/Principe-nube.png" alt="Principe">   
            <img class="mesa"src="/wp-content/imagenes/mesa-regalos.png" alt="Mesa de regalo">    
            <img class="princesa" src="/wp-content/imagenes/Princesa-nube.png" alt="Princesa">    
    </div>
</main>
<?php endwhile; endif; ?>

<?php get_footer(); ?>