<?php get_header(); ?>
<div class="titulo-pagina">
    <h1 style="color: #ff6400;
    font-size: 28px;">Blog</h1>
</div>
<main id="content">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php get_template_part( 'entry' ); ?>
<?php comments_template(); ?>
<?php endwhile; endif; ?>
<?php get_template_part( 'nav', 'below' ); ?>
</main>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
