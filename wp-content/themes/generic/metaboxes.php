<?php

add_action( 'cmb2_init', 'cmb2_add_metabox' );
function cmb2_add_metabox() {
	$prefix = '_inicio_';

	$cmb = new_cmb2_box( array(
		'id'           => $prefix . 'video',
		'title'        => __( 'Video URL', 'cmb2' ),
        'object_types' => array( 'page'),
        'show_on'      => array( 'key' => 'page-template', 'value' => 'inicio.php' ),
		'context'      => 'normal',
		'priority'     => 'default',
	) );

	$cmb->add_field( array(
        'name' => __( 'URL', 'cmb2' ),
		'id' => $prefix . 'video',
		'type' => 'text_url',
	) );
	$cmb->add_field( array(
		'name' => __( 'Imagen 1', 'cmb2' ),
		'id' => $prefix . 'imagen_1',
		'type' => 'file',
	) );
	$cmb->add_field( array(
		'name' => __( 'Imagen 2', 'cmb2' ),
		'id' => $prefix . 'imagen_2',
		'type' => 'file',
	) );
	$cmb->add_field( array(
		'name' => __( 'Imagen 3', 'cmb2' ),
		'id' => $prefix . 'imagen_3',
		'type' => 'file',
	) );
	$cmb->add_field( array(
		'name' => __( 'Imagen 4', 'cmb2' ),
		'id' => $prefix . 'imagen_4',
		'type' => 'file',
	) );
}

add_action( 'cmb2_init', 'cmb2_add_metabox_2' );
function cmb2_add_metabox_2() {
	$prefix = '_hp_';
    
	$cmb = new_cmb2_box( array(
		'id'           => $prefix . 'video',
		'title'        => __( 'Video URL', 'cmb2' ),
        'object_types' => array( 'page'),
        'show_on'      => array( 'key' => 'page-template', 'value' => 'home-page.php' ),
		'context'      => 'normal',
		'priority'     => 'default',
	) );

	$cmb->add_field( array(
        'name' => __( 'URL', 'cmb2' ),
		'id' => $prefix . 'video',
		'type' => 'text_url',
	) );
	$cmb->add_field( array(
		'name' => __( 'Imagen 1', 'cmb2' ),
		'id' => $prefix . 'imagen_1',
		'type' => 'file',
	) );
	$cmb->add_field( array(
		'name' => __( 'Imagen 2', 'cmb2' ),
		'id' => $prefix . 'imagen_2',
		'type' => 'file',
	) );
	$cmb->add_field( array(
		'name' => __( 'Imagen 3', 'cmb2' ),
		'id' => $prefix . 'imagen_3',
		'type' => 'file',
	) );
	$cmb->add_field( array(
		'name' => __( 'Imagen 4', 'cmb2' ),
		'id' => $prefix . 'imagen_4',
		'type' => 'file',
	) );
}
?>