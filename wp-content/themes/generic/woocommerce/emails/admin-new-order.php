<?php
/**
 * Admin new order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/admin-new-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author WooThemes
 * @package WooCommerce/Templates/Emails/HTML
 * @version 2.5.0
 */
session_start();
 if ( ! defined( 'ABSPATH' ) ) {
 	exit;
 }

 /**
  * @hooked WC_Emails::email_header() Output the email header
  */
 do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

 <p><?php printf( __( 'You have received an order from %s. The order is as follows:', 'woocommerce' ), $order->get_formatted_billing_full_name() ); ?></p>

 <?php

 /**
  * @hooked WC_Emails::order_details() Shows the order details table.
  * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
  * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
  * @since 2.5.0
  */
 do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

 /**
  * @hooked WC_Emails::order_meta() Shows order meta data.
  */
 do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

 /**
  * @hooked WC_Emails::customer_details() Shows customer details
  * @hooked WC_Emails::email_address() Shows email address
  */
 do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );
?>
  
  
   
  <div style="display:none">
  <h2 style="color:#04b443;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:18px;font-weight:bold;line-height:130%;margin:0 0 18px;text-align:left">Datos del libro</h2>  
    <b>Url: </b>
    <?php
  $sexo = $_SESSION['sexo'];
  $nombre = $_SESSION['nombre'];
  
$url_sistema = "https://www.soycuentaconmigo.com/wp-content/themes/generic/sistema/?sexo=$sexo&nombre=$nombre";

if(isset($_SESSION['fec_nin'])){
    $fec_nacimiento_nino = $_SESSION["fec_nin"];
    $url_sistema.="&fec_nin=$fec_nacimiento_nino";
}
if(isset($_SESSION['nbr_pap'])){
    $nombre_padre = $_SESSION["nbr_pap"];
    $url_sistema.="&nbr_pap=$nombre_padre";
}
if(isset($_SESSION['nbr_mad'])){
    $nombre_madre = $_SESSION["nbr_mad"];
    $url_sistema.="&nbr_mad=$nombre_madre";
}
if(isset($_SESSION['sex_gat'])){
    $sexo_gato = $_SESSION["sex_gat"];
    $url_sistema.="&sex_gat=$sexo_gato";
}
if(isset($_SESSION['nbr_gat'])){
    $nombre_gato = $_SESSION["nbr_gat"];
    $url_sistema.="&nbr_gat=$nombre_gato";
}
if(isset($_SESSION['tip_fam'])){
    $tipo_familiar = $_SESSION["tip_fam"];
    $url_sistema.="&tip_fam=$tipo_familiar";
}
if(isset($_SESSION['nbr_fam'])){
    $nombre_familiar = $_SESSION["nbr_fam"];
    $url_sistema.="&nbr_fam=$nombre_familiar";
}
if(isset($_SESSION['ded'])){
    $dedicatoria = $_SESSION["ded"];
    $url_sistema.="&ded=$dedicatoria";
}
if(isset($_SESSION['ciu_nac'])){
    $ciudad_nacimiento = $_SESSION["ciu_nac"];
    $url_sistema.="&ciu_nac=$ciudad_nacimiento";
}
if(isset($_SESSION['can_fav'])){
    $cancion_favorita = $_SESSION["can_fav"];
    $url_sistema.="&can_fav=$cancion_favorita";
}
if(isset($_SESSION['com_fav'])){
    $comida_favorita = $_SESSION["com_fav"];
    $url_sistema.="&com_fav=$comida_favorita";
}
if(isset($_SESSION['jug_fav'])){
    $juguete_favorito = $_SESSION["jug_fav"];
    $url_sistema.="&jug_fav=$juguete_favorito";
}
if(isset($_SESSION['act_fav'])){
    $actividad_favorita = $_SESSION["act_fav"];
    $url_sistema.="&act_fav=$actividad_favorita";
}
if(isset($_SESSION['bio'])){
    $biografia = $_SESSION["bio"];
    $url_sistema.="&bio=$biografia";
}
if(isset($_SESSION['img_0'])){
    $rostro_nino = $_SESSION["img_0"];
    $url_sistema.="&img_0=$rostro_nino";
}
if(isset($_SESSION['img_1'])){
    $retrato_familiar = $_SESSION["img_1"];
    $url_sistema.="&img_1=$retrato_familiar";
}
if(isset($_SESSION['img_2'])){
    $retrato_nino = $_SESSION["img_2"];
    $url_sistema.="&img_2=$retrato_nino";
}
if(isset($_SESSION['img_3'])){
    $rostro_madre = $_SESSION["img_3"];
    $url_sistema.="&img_3=$rostro_madre";
}
if(isset($_SESSION['img_4'])){
    $rostro_padre = $_SESSION["img_4"];
    $url_sistema.="&img_4=$rostro_padre";
}
if(isset($_SESSION['variation'])){
    $variation = $_SESSION["variation"];
    $url_sistema.="&variation=$variation";
}
if(isset($_SESSION['tapa'])){
    $tapa = $_SESSION["tapa"];
    $url_sistema.="&tapa=$tapa";
}
if(isset($_SESSION['tamano'])){
    $tamano = $_SESSION["tamano"];
    $url_sistema.="&tamano=$tamano";
}

    echo $url_sistema;
    ?>
    <br>
    <b>Nombre: </b>
    <?php
    echo $_SESSION['nombre'];
    ?>
    <br>
    <b>Sexo: </b>
    <?php
    echo $_SESSION['sexo'];
    ?>
    <br>
    <b>Fecha de nacimiento: </b>
    <?php
    echo $_SESSION['fec_nin'];
    ?>
    <br>
    <b>Ciudad de nacimiento: </b>
    <?php
    echo $_SESSION['ciu_nac'];
    ?>
    <br>
    <b>Canción favorita: </b>
    <?php
    echo $_SESSION['can_fav'];
    ?>
    <br>
    <b>Comida favorita: </b>
    <?php
    echo $_SESSION['com_fav'];
    ?>
    <br>
    <b>Jueguete favorito: </b>
    <?php
    echo $_SESSION['jug_fav'];
    ?>
    <br>
    <b>Actividad favorita: </b>
    <?php
    echo $_SESSION['act_fav'];
    ?>
    <br>
    <b>Biografía: </b>
    <?php
    echo $_SESSION['bio'];
    ?>
    <br>
    <b>Nombre de padres: </b>
    <?php
    echo $_SESSION['nbr_mad'];
    echo " y ";
    echo $_SESSION['nbr_pap'];
    ?>
    <br>
    <b><?php echo ( isset($_SESSION["sex_gat"]) ? ( $_SESSION["sex_gat"] == "1" ? "Gatito" : "Gatita") : "Gato"); ?>: </b>
    <?php
    echo $_SESSION['nbr_gat'];
    ?>
    <br>
    <b>Familiar: </b>
    <?php
    if(isset($_SESSION["tip_fam"])){
        switch ($_SESSION["tip_fam"]) {
            case "1":
                echo "Tía";
                break;
            case "2":
                echo "Abuelita";
                break;
            case "3":
                echo "Prima";
                break;
        }
    }

    echo " ".$_SESSION['nbr_fam'];
    ?>
    <br>
    <b>Dedicatoria: </b>
    <?php
    echo $_SESSION['ded'];
    ?>
    <br>
    <b>Tapa: </b>
    <?php
    if(isset($_SESSION["variation"])){
        switch ($_SESSION["variation"]) {
            case "111":
                echo "Dura de 15x15";
                break;
            case "112":
                echo "Blanda de 20x20";
                break;
            case "113":
                echo "Dura de 20x20";
                break;
        }
    }
    ?>
    <br>
  </div>
  <?php

 /**
  * @hooked WC_Emails::email_footer() Output the email footer
  */
 do_action( 'woocommerce_email_footer', $email );
