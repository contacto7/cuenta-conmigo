<?php
/**
 * Email Order Items
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-items.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

$subdomain = array_shift((explode('.', $_SERVER['HTTP_HOST'])));

$text_align = is_rtl() ? 'right' : 'left';


    global $woocommerce;
    $items = $woocommerce->cart->get_cart();

        foreach($items as $item => $values) { 
            echo "<tr>"; 
            $_product =  wc_get_product( $values['data']->get_id()); 
            //INFO Y URL
            echo "<td>";
            echo "<b>".$_product->get_title()." ".$values['nombre_nino']['value'].'</b> <br>'; 
            $url_1 = $values['book_url']['value'];
            $url_1.=("&variation=".$values['variation']['attribute_pa_tamano']."&tapa=".$values['variation']['attribute_pa_tapa'] );
            $page= "index";
            $page_replace;
            switch ((int) $values['product_id']) {
                case 59:
                    //1er cuento-ingles
                    $page_replace = "/book/?";
                    $page.= "-2";
                    break;
                case 64:
                    //2do cuento-ingles
                    $page_replace = "/book-2/?";
                    $page.= "-3";
                    break;
                case 105:
                    //1er cuento-español
                    $page_replace = "/libro/?";
                    $page.= "";
                    break;
                case 198:
                    //2do cuento-español
                    $page_replace = "/libro-2/?";
                    $page.= "-4";
                    break;
            }
            $page.= ".php";
            $url_2 = str_replace($page_replace, "https://www.soycuentaconmigo.com/wp-content/themes/generic/sistema/".$page."?", $url_1);
            echo "<a href='$url_2' class='link-descarga'>Link de descarga</a><br>";
            $parts = parse_url($url_2);
            parse_str($parts['query'], $query);
            //NOMBRE
            if($subdomain == "en"){
                echo "<div>Name: ".$query['nombre']."<div>";
            }else{
                echo "<div>Nombre: ".$query['nombre']."<div>";
            }
            //SEXO
            if($subdomain == "en"){
                echo "<div>Sex: ".($query['sexo'] == "h"? "Male":"Female")."<div>";
            }else{
                echo "<div>Sexo: ".($query['sexo'] == "h"? "Hombre":"Mujer")."<div>";
            }
            //TAPA
            if($subdomain == "en"){
                $tapa = $values['variation']['attribute_pa_tapa'];
                $tapa_en = "Softcover";
                switch ($tapa){
                    case "blanda":
                        $tapa_en = "Softcover";
                        break;
                    case "dura":
                        $tapa_en = "Hardcover";
                        break;
                    case "pdf-digital":
                        $tapa_en = "Printable";
                        break;
                    default:
                        $tapa_en = "Softcover";
                        break;
                        
                }
                
                echo "<div>Cover: ".$tapa_en." of ".$values['variation']['attribute_pa_tamano']."<div>";
            }else{
                echo "<div>Tapa: ".$values['variation']['attribute_pa_tapa']." de ".$values['variation']['attribute_pa_tamano']."<div>";
            }
            
            //echo "<a>".$values['book_url']['value'].' </td>'; 
            echo "</td>";
            //INFO Y URL
            //echo '<td><pre>' . var_export($values, true) . '</pre></td>'; 
            //echo '<td>'.$values['product_id'].'</td>'; 
            echo "<td>".$values['quantity']."</td>"; 
            echo "<td>".$values['line_total']."</td>"; 
            echo "</tr>"; 
        } 


	?>

