<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

$subdomain = array_shift((explode('.', $_SERVER['HTTP_HOST'])));

do_action( 'woocommerce_before_cart' ); ?>

<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
    <?php do_action( 'woocommerce_before_cart_table' ); ?>

    <table class="shop_table  cart woocommerce-cart-form__contents" cellspacing="0">
        <thead hidden>
        <tr>
            <th class="product-name"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
            <th class="product-subtotal"><?php esc_html_e( 'Total', 'woocommerce' ); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php do_action( 'woocommerce_before_cart_contents' ); ?>

        <?php
        foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
            $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
            $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

            if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                ?>
                <tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
                    <?php //echo '<pre>' . var_export($cart_item, true) . '</pre>'; ?>

                    <td class="product-name" data-title="<?php esc_attr_e( 'Product', 'woocommerce' ); ?>">
                        
                
                        <?php if($subdomain == "en"){ ?>
                            <span class="cart-item-kid-name">
                                <?php echo $cart_item['nombre_nino']['value']; ?>'s
                            </span>
                            <span>
                                <?php echo $_product->get_title() ?>
                            </span> 
                        <?php }else{ ?>
                            <span>
                                <?php echo $_product->get_title() ?>
                            </span> 
                            <span class="cart-item-kid-name">
                                <?php echo $cart_item['nombre_nino']['value']; ?>
                            </span>
                        <?php } ?>

                        <div class="product-thumbnail">
                            <?php
                            
                            $parts = parse_url($cart_item['book_url']['value']);
                            parse_str($parts['query'], $query);
                            $sexo = $query['sexo'];
                            $image_thumbnail = getBookThumbnail($product_id, $sexo);
                
                            ?>
                            <img width="300" height="300" src="<?php echo $image_thumbnail; ?>" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="">
                        </div>
                    </td>

                    <td class="product-subtotal" data-title="<?php esc_attr_e( 'Total', 'woocommerce' ); ?>">
                        <?php
                        echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
                        ?>
                        <br>
                        <a
                            onclick="eliminaryEditar('<?php echo esc_url( wc_get_cart_remove_url( $cart_item_key ) ); ?>', '<?php echo $cart_item['book_url']['value']; ?>')"
                            href="#"
                            class="eliminar-wc-cart-item"
                        ><i class="fa fa-pencil-square-o"></i><?php _e( 'Edit', 'woocommerce' ); ?></a>
                        <br>
                        <?php

                        //echo '<pre>' . var_export($cart_item['data'], true) . '</pre>';
                        // @codingStandardsIgnoreLine
                        echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                            '<a href="%s" class="eliminar-wc-cart-item" aria-label="%s" data-product_id="%s" data-product_sku="%s"><i class="fa fa-trash-o"></i> %s</a>',
                            esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
                            __( 'Remove this item', 'woocommerce' ),
                            esc_attr( $product_id ),
                            esc_attr( $_product->get_sku() ),
                            __( 'Delete', 'woocommerce' )
                        ), $cart_item_key );
                        ?>
                    </td>
                </tr>
                <?php
            }
        }
        ?>

        <?php do_action( 'woocommerce_cart_contents' ); ?>

        <tr>
            <td colspan="6" class="actions">

                <?php if ( wc_coupons_enabled() ) { ?>
                    <div class="coupon">
                        <label for="coupon_code"><?php esc_html_e( 'Coupon:', 'woocommerce' ); ?></label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> <button type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?></button>
                        <?php do_action( 'woocommerce_cart_coupon' ); ?>
                    </div>
                <?php } ?>

                <a class="anadir-cuento-cart" href="/">
                
                    <?php
                        if($subdomain == "en"){
                            echo "Add another book";
                        }else{
                            echo "Añadir otro cuento";
                        }
                    
                    ?>
                </a>
                <?php do_action( 'woocommerce_cart_actions' ); ?>

                <?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
            </td>
        </tr>

        <?php do_action( 'woocommerce_after_cart_contents' ); ?>
        </tbody>
    </table>
    <?php do_action( 'woocommerce_after_cart_table' ); ?>
</form>

<div class="cart-collaterals">
    <?php
    /**
     * Cart collaterals hook.
     *
     * @hooked woocommerce_cross_sell_display
     * @hooked woocommerce_cart_totals - 10
     */
    do_action( 'woocommerce_cart_collaterals' );
    ?>
</div>

<?php do_action( 'woocommerce_after_cart' ); ?>

<script>
function eliminaryEditar(url_eliminar, url_editar) {
    jQuery.ajax({
        type:"POST",
        url: url_eliminar,
        cache: false,
        success: function(result){
            console.log("Resultado: "+result);
            console.log("Link: "+url_editar);
            window.location.href= url_editar;
        },
        error: function(error){
            console.log(JSON.stringify(error));
        }


    });
}
</script>