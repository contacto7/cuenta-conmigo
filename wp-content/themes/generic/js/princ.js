
var $=jQuery.noConflict();

//FUNCION PARA ROTAR
function drawRotated(id_cr_container, degrees){
    var canvas=$("#"+id_cr_container+" canvas")[0];
    var ctx=canvas.getContext("2d");

    // Create a temp canvas to store our data (because we need to clear the other box after rotation.
    var tempCanvas = document.createElement("canvas"),
        tempCtx = tempCanvas.getContext("2d");

    tempCanvas.width = canvas.width;
    tempCanvas.height = canvas.height;
    // put our data onto the temp canvas
    tempCtx.drawImage(canvas,0,0,canvas.width,canvas.height);

    // Append for debugging purposes, just to show what the canvas did look like before the transforms.
    document.body.appendChild(tempCanvas);

    ctx.clearRect(0,0,canvas.width,canvas.height);
    ctx.save();

    //Trasladamos el punto de rotacion
    ctx.translate((canvas.width)/2,(canvas.height)/2);
    //Rotamos
    ctx.rotate(degrees*Math.PI/180);
    //Regresamos el punto de rotación
    ctx.translate(-(canvas.width)/2,-(canvas.height)/2);
    //Dibujamos el temporal
    ctx.drawImage(tempCanvas,0,0,canvas.width,canvas.height);
    ctx.restore();
    // Append for debugging purposes, just to show what the canvas did look like before the transforms.
    document.body.removeChild(tempCanvas);
}
jQuery(document).ready(function(){
    
    var medida_min = 25;
    var medida_med = medida_min*2;
    var medida_max = medida_min*3;
    var medida_unid = "%";
    var medida_css_min = medida_min+medida_unid;
    var medida_css_med = medida_med+medida_unid;
    var medida_css_max = medida_max+medida_unid;
    
    jQuery(".libro-pagina").css("left",medida_css_min);
    
    jQuery(".libro-pagina").click(function(){
        
        var pag_actual = parseInt(jQuery(this).attr("data-id"));
        var pag_siguiente = pag_actual + 1;
        var pag_siguiente_2 = pag_siguiente + 1;
        var pag_anterior = pag_actual - 1;
        var pag_anterior_2 = pag_anterior - 1;
        console.log(pag_actual);
        console.log(pag_siguiente);
        console.log(pag_anterior);        
        /*
        if(pag_actual == 1){
            jQuery(".libro-pagina").css("left","285px");
        }else if(pag_actual == 15){
            jQuery(".libro-pagina").css("left","855px");
        }else{
            jQuery(".libro-pagina").css("left","570px");
        }
           */
        //ACTUALIZMOS LA PÁGINA QUE ESTA DESPUËS DE LA SIGUIENTE
        jQuery("#pag-"+pag_siguiente_2).removeClass("pagina-siguiente");
        //ACTUALIZMOS LA PÁGINA QUE ESTA ANTES DE LA ANTERIOR
        jQuery("#pag-"+pag_anterior_2).removeClass("pagina-siguiente");
        //jQuery(".libro-pagina").css("pointer-events","none");
        
    
           setTimeout(function () {   
                //jQuery(".libro-pagina").css("pointer-events","inherit");
           }, 1000)

        
        if(jQuery(this).hasClass("pagina-volteada")){
            //MOVIMIENTO EN X
            if(pag_actual == 1){
                jQuery(".libro-pagina").animate({
                    left: medida_css_min
                  }, 800, function() {
                    // Animation complete.
                  });
            }else{
                jQuery(".libro-pagina").animate({
                    left: medida_css_med
                  }, 800, function() {
                    // Animation complete.
                  });
            }
            //MOVIMIENTO EN X
            
            //TRANSICION
            jQuery(this).addClass("pagina-regresada");
            jQuery(this).removeClass("pagina-volteada");
            
            //POSICIONAMIENTO EN Z
            jQuery(this).removeClass("pagina-activa-izquierda");
            jQuery(this).addClass("pagina-activa-derecha");
                
            //PAGINA SIGUIENTE
            jQuery("#pag-"+pag_siguiente).addClass("pagina-siguiente");
            jQuery("#pag-"+pag_siguiente).removeClass("pagina-activa-derecha");
            jQuery("#pag-"+pag_siguiente).removeClass("pagina-regresada");
            //PAGINA ANTERIOR
            jQuery("#pag-"+pag_anterior).addClass("pagina-activa-izquierda");
            jQuery("#pag-"+pag_anterior).removeClass("pagina-siguiente");
            pagina_actual = pag_actual;
        }else if(jQuery(this).hasClass("pagina-regresada")){
            //MOVIMIENTO EN X
            if(pag_siguiente == 1){
                jQuery(".libro-pagina").animate({
                    left: medida_css_min
                  }, 800, function() {
                    // Animation complete.
                  });
            }else if(pag_actual == paginatotal){
                jQuery(".libro-pagina").animate({
                    left: medida_css_max
                  }, 800, function() {
                    // Animation complete.
                  });
            }else{
                jQuery(".libro-pagina").animate({
                    left: medida_css_med
                  }, 800, function() {
                    // Animation complete.
                  });
            }
            //MOVIMIENTO EN X
            //TRANSICION
            jQuery(this).addClass("pagina-volteada");
            jQuery(this).removeClass("pagina-regresada");
            
            //POSICIONAMIENTO EN Z
            jQuery(this).removeClass("pagina-activa-derecha");
            jQuery(this).addClass("pagina-activa-izquierda");
            
                
            //PAGINA SIGUIENTE
            jQuery("#pag-"+pag_siguiente).addClass("pagina-activa-derecha");
            jQuery("#pag-"+pag_siguiente).removeClass("pagina-siguiente");
            //PAGINA ANTERIOR
            jQuery("#pag-"+pag_anterior).addClass("pagina-siguiente");
            jQuery("#pag-"+pag_anterior).removeClass("pagina-activa-izquierda");
            pagina_actual = pag_siguiente;
        }else{
            //MOVIMIENTO EN X
            if(pag_actual == paginatotal){
                jQuery(".libro-pagina").animate({
                    left: medida_css_max
                  }, 800, function() {
                    // Animation complete.
                  });
            }else{
                jQuery(".libro-pagina").animate({
                    left: medida_css_med
                  }, 800, function() {
                    // Animation complete.
                  });
             }
            //MOVIMIENTO EN X
            //TRANSICION
            jQuery(this).addClass("pagina-volteada");
            
            //POSICIONAMIENTO EN Z
            jQuery(this).removeClass("pagina-activa-derecha");
            jQuery(this).addClass("pagina-activa-izquierda");
            
            //PAGINA SIGUIENTE
            jQuery("#pag-"+pag_siguiente).addClass("pagina-activa-derecha");
            jQuery("#pag-"+pag_siguiente).removeClass("pagina-siguiente");
            //PAGINA ANTERIOR
            jQuery("#pag-"+pag_anterior).addClass("pagina-siguiente");
            jQuery("#pag-"+pag_anterior).removeClass("pagina-activa-izquierda");
            pagina_actual = pag_siguiente;
        }
    });
    

    jQuery( ".sexo-el" ).on( "click", function() {
       jQuery( ".sexo-el" ).css("color","#fff");
       jQuery( this ).css("color","rgb(255, 210, 21)").addClass('sex-elem-active');
        var sexo = jQuery( this ).attr("data-sexo");
       jQuery( ".lnk-libro" ).attr("data-sexo",sexo);

       if($(this).parents('.btnsexom-wrapp').length){
           jQuery( ".btnsgtem" ).click();
       }

    });
    jQuery( ".lnk-libro" ).on( "click", function() {

        var sexo = jQuery( this ).attr("data-sexo");
        var nombre = jQuery( "#nombre-inp" ).val();
        var len_nombre=nombre.length;
        var regex = new RegExp(/^[a-zA-ZñÑ\s]+$/);

        if(len_nombre == 0){
            alert("Debe ingresar un nombre");
        }else if(!regex.test(nombre)){
            alert("Debe ingresar sólo letras.");
        }else if(len_nombre < 3){
            alert("El nombre es muy corto. Por favor contáctate con nosotros para poder brindarte una opción especial.");
        }else if(len_nombre > 11){
            alert("El nombre es muy largo. Por favor contáctate con nosotros para poder brindarte una opción especial.");
        }else if(!sexo){
            alert("Debe seleccionar un sexo");
        }else{
            var link_href="/libro1/index.php?nombre="+nombre+"&sexo="+sexo+"&tkn=0";
            window.location.href=link_href;
        }
    });

    /*FUNCIÓN PARA ROTAR DE ACUERDO AL INPUT RANGE
    NO IMPORTA EL VALOR DEL RANGE INPUT, SOLO IMPORTA SABER LA DIRECCION
     SIEMPRE SE USARÄ 20 GRADOS DE ROTACION
    */
    jQuery( ".range-rotate" ).on( "input", function() {
        console.log("--------ROTACION----------")
        let img_to_crop = $(this).attr('data-imgcrop');

        let rotacion_aterior = parseInt($(this).attr('data-rot-ant'));
        let rotacion_actual = parseInt($(this).val());

        console.log("rotación anterior: "+rotacion_aterior);
        console.log("rotación actual: "+rotacion_actual);

        if (rotacion_actual >= rotacion_aterior ){
            console.log("Dirección hacia adelante.");
            drawRotated(img_to_crop, 10);
        }else{
            console.log("rotacion en reverso.");
            drawRotated(img_to_crop, -10);
        }

        $(this).attr('data-rot-ant', rotacion_actual);
    });

    /*FUNCIÓN PARA ROTAR 90 grados
    */
    jQuery( ".rotate-90" ).on( "click", function() {
        let img_to_crop = $(this).attr('data-imgcrop');
        let rotation = $(this).attr('data-rotation');
        
        drawRotated(img_to_crop, rotation);
    });


    /*CONTAR LOS CARACTERES EN LOS INPUT CON RESTRICCION DE LONGITUD*/
    $( ".inputs-texto" ).each(function() {
        var texto = $(this).val();
        var leng_texto = texto.length;
        var elem_muestr = $(this).attr("data-cuenta");
        var max_char = $(this).attr("data-max");

        if(leng_texto < max_char){
            $("#"+elem_muestr).html(leng_texto);
        }else{
            $("#"+elem_muestr).html("<span style='color:red;font-weight:bold'>"+leng_texto+"</span>");
        }
    });


    $('.inputs-texto').on('change input', function(){
        var texto = $(this).val();
        var leng_texto = texto.length;
        var elem_muestr = $(this).attr("data-cuenta");
        var max_char = $(this).attr("data-max");

        if(leng_texto < max_char){
            $("#"+elem_muestr).html(leng_texto);
        }else{
            $("#"+elem_muestr).html("<span style='color:red;font-weight:bold'>"+leng_texto+"</span>");
        }
    });
    
    
});

function verificarfecha(elem1,elem2){
    var id_camb_1="#"+elem1;
    var id_camb_2="#"+elem2;
    console.log("aa");
    if($(id_camb_1).val()==''){
        $(id_camb_2).removeClass("has-success");
        $(id_camb_1).removeClass("form-control-success");
        $(id_camb_2).addClass("has-danger");
        $(id_camb_1).addClass("form-control-danger");
        corr_info_fecha=0;
    }else{
        corr_info_fecha=1;
        $(id_camb_2).removeClass("has-danger");
        $(id_camb_1).removeClass("form-control-danger");
        $(id_camb_2).addClass("has-success");
        $(id_camb_1).addClass("form-control-success");
        $(".preview-dv-1").animate({opacity:1,},500,function(){});
        $("#prev-nombre").html($(id_camb_1).val());
    }
}
function verificarnombre(nombre, msg){
    
    console.log(nombre);
    
    var len_nombre=nombre.length;
    var regex = new RegExp(/^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/);

    if(!len_nombre){
        /*
        mensaje_subir_error="Error en "+msg+": Datos obligatorios.";
        return false;
        */
        return true;
    }else if(!regex.test(nombre)){
        mensaje_subir_error="Error en "+msg+": Debe ingresar sólo letras";
        return false;
    }else if(len_nombre > 10){
        mensaje_subir_error="Error en "+msg+": El nombre es muy largo. Por favor ingrese un nombre más corto, o un diminutuivo :)";
        return false;
    }else{
        return true;
    }
    
}
function verificardedicatoria(nombre, msg){
    
    console.log(nombre);
    
    var len_nombre=nombre.length;
    var regex = new RegExp(/[a-zA-ZÀ-ÿ0-9, .'\-_\u00f1\u00d1\n\r!]+$/);

    if(!len_nombre){
        /*
        mensaje_subir_error="Error en "+msg+": Datos obligatorios.";
        return false;
        */
        return true;
    }else if(!regex.test(nombre)){
        mensaje_subir_error="Error en "+msg+": Debe ingresar sólo caracteres alfanuméricos, comillas simples, o guión.";
        return false;
    }else if(len_nombre > 140){
        mensaje_subir_error="Error en "+msg+": El texto es muy largo. Por favor redúzcalo a menos de 140 caracteres.";
        return false;
    }else{
        return true;
    }
    
}
function verificarbiografia(nombre, msg){
    
    console.log(nombre);
    
    var len_nombre=nombre.length;
    //var regex = new RegExp(/^[A-Za-z0-9 .'?!,@$#\-_\n\r]+$/);
    var regex = new RegExp(/[a-zA-ZÀ-ÿ0-9, .'\-_\u00f1\u00d1\n\r!]+$/);

    if(!len_nombre){
        /*
        mensaje_subir_error="Error en "+msg+": Datos obligatorios.";
        return false;
        */
        return true;
    }else if(!regex.test(nombre)){
        mensaje_subir_error="Error en "+msg+": Debe ingresar sólo caracteres alfanuméricos, comillas simples, o guión.";
        return false;
    }else if(len_nombre > 30){
        mensaje_subir_error="Error en "+msg+": El texto es muy largo. Por favor redúzcalo a menos de 30 caracteres.";
        return false;
    }else{
        return true;
    }
    
}
$("#fecha-formcont").unbind().on("focus",function(){
    $('#fecha-formcont-wrapp').datetimepicker({
        viewMode:'years',format:'DD-MM-YYYY',
        locale:'es'
    });
    console.log("se hizo click");
    $('#btn-dp-1').click();
    verificarfecha('fecha-formcont','fecha-formcont-wrapp');
});

function mostrarDatosLibro(libro, sexo, nombre) {
    console.log("Libro: "+libro);
    console.log("Sexo: "+sexo);
    console.log("Nombre: "+nombre);
    /*CSS*/
    /*margins*/
    /**
     * Mama:
     * id="capucha-madre-pre"
     * id="image_demo4"
     * class="imgcrp-madre"
     * imagen: .imgcrp-madre img
     *
     * Papa:
     * id="capucha-padre-pre"
     * id="image_demo5"
     * class="imgcrp-padre"
     * .imgcrp-padre img
     *
     * Retrato familiar:
     * id="image_demo2"
     * id="retrato-familiar-pre"
     * class="cr-viewport cr-vp-square"
     *
     * Retrato niño:
     * id="image_demo3"
     * id="retrato-nino-pre"
     * class="cr-viewport cr-vp-square"
     *
     * */
    let margin_top_image_demo;
    let top_imgcrp_nino;
    let height_imgcrp_nino;
    let left_imgcrp_nino;
    let url_capucha;
    let url_caratula;
    let nombrem_color;
    let pre_dedicatoria = "Querido ";
    let dedicatoria = nombre+",\n¡La verdadera magia para hacer realidad todos tus sueños está en tu interior!";
    let capucha_nino_width;
    let url_capucha_mama;
    let imgcrp_madre_top;
    let imgcrp_madre_left;
    let img_madre_width;
    let url_capucha_papa;
    let image_demo5_margin_top;
    let imgcrp_papa_top;
    let imgcrp_papa_left;
    let imgcrp_papa_height;
    let imgcrp_papa_margin_top;
    let img_papa_width;
    let retrato_nino_pre_width;
    let retrato_nino_pre_height;

    url_caratula = '/wp-content/themes/generic/img_lib/595px/'+sexo+'/1.jpg';
    url_caratula_2 = '/wp-content/themes/generic/img_lib_2/595px/'+sexo+'/libro-2-banner-creacion.png';
    
    
    if (libro == 1){
        console.log("Libro 1 seleccionado.");
        capucha_nino_width = "260px";
        url_capucha = '/wp-content/themes/generic/img_lib/595px/'+sexo+'/velo-2.png';
        left_imgcrp_nino = "7px";
        url_capucha_mama = "/wp-content/themes/generic/img_lib/595px/veloreina-2.png";
        imgcrp_madre_top = "-27px";
        imgcrp_madre_left = "7px";
        img_madre_width = "260px";
        url_capucha_papa = "/wp-content/themes/generic/img_lib/595px/velorey-2.png";
        image_demo5_margin_top = "65px";
        imgcrp_papa_top = "8px";
        imgcrp_papa_left = "7px";
        imgcrp_papa_height = "389px";
        imgcrp_papa_margin_top = "-80px";
        img_papa_width = "260px";
        retrato_nino_pre_width = "260px";
        retrato_nino_pre_height = "300px";

        if (sexo == "m"){
            console.log("Sexo femenino.");
            margin_top_image_demo = "40px";
            top_imgcrp_nino = "11px";
            height_imgcrp_nino = "322px";
            nombrem_color = "#653f88";
            pre_dedicatoria = "Querida ";
        }else if(sexo == "h"){
            console.log("Sexo masculino.");
            margin_top_image_demo = "80px";
            top_imgcrp_nino = "8px";
            height_imgcrp_nino = "389px";
            nombrem_color = "#006cb5";
        }

        /*visibilidad de campos*/
        $(".form-info-libroinic .campo").show();

    }else if(libro == 2){
        console.log("Libro 2 seleccionado.");
        nombrem_color = "#4f0000";
        capucha_nino_width = "290px";
        url_capucha = '/wp-content/themes/generic/img_lib_2/595px/'+sexo+'/capuchita.png';
        margin_top_image_demo = "30px";
        top_imgcrp_nino = "18px";
        height_imgcrp_nino = "403px";
        left_imgcrp_nino = "5px";
        url_capucha_mama = "/wp-content/themes/generic/img_lib_2/595px/capuchita-mama.png";
        imgcrp_madre_top = "42px";
        imgcrp_madre_left = "-7px";
        img_madre_width = "218px";
        url_capucha_papa = "/wp-content/themes/generic/img_lib_2/595px/capuchita-papa.png";
        image_demo5_margin_top = "0";
        imgcrp_papa_top = "26px";
        imgcrp_papa_left = "5px";
        imgcrp_papa_height = "319px";
        imgcrp_papa_margin_top = "0";
        img_papa_width = "240px";
        retrato_nino_pre_width = "300px";
        retrato_nino_pre_height = "260px";

        if (sexo == "m"){
            console.log("Sexo femenino.");
            pre_dedicatoria = "Querida ";
        }else if(sexo == "h"){
            console.log("Sexo masculino.");

        }

        /*visibilidad de campos*/
        $(".form-info-libroinic .campo:not(:first-child)").hide();

    }

    /*CONFIGURAMOS LOS VALORES */
    /*NIÑO*/
    jQuery('.capucha-nino').attr("src",url_capucha).css("width", capucha_nino_width);
    jQuery('.capucha-nino').closest('.imgcrp-nino').css({
        "top":top_imgcrp_nino,
        "height":height_imgcrp_nino,
        "left":left_imgcrp_nino,
    });
    jQuery('.capucha-nino').closest('#image_demo').css("margin-top", margin_top_image_demo);
    jQuery('.libro-img-wrapp .nombrem').css("color",nombrem_color);
    /*MAMA*/
    jQuery('.imgcrp-madre img,#capucha-madre-pre').attr("src",url_capucha_mama);
    jQuery('.imgcrp-madre').css({
        "top":imgcrp_madre_top,
        "left":imgcrp_madre_left,
    });
    jQuery('.imgcrp-madre img').css("width",img_madre_width);
    /*PAPA*/
    jQuery('#image_demo5').css("margin-top",image_demo5_margin_top);
    jQuery('.imgcrp-padre img,#capucha-padre-pre').attr("src",url_capucha_papa);
    jQuery('.imgcrp-padre').css({
        "top":imgcrp_papa_top,
        "left":imgcrp_papa_left,
        "height":imgcrp_papa_height,
        "margin-top":imgcrp_papa_margin_top,
    });
    jQuery('.imgcrp-padre img').css("width",img_papa_width);
    /*RETRATO NIÑO*/
    jQuery('#retrato-nino-pre').css({
        "width":retrato_nino_pre_width,
        "height":retrato_nino_pre_height,
    });

    /*CARATULA*/
    jQuery('.btn-pasosm-wrapp .libro-img').attr("src",url_caratula);
    jQuery('.btn-pasosm-wrapp .libro-img-2').attr("src",url_caratula_2);
    /*DEDICATORIA*/
    dedicatoria = pre_dedicatoria+dedicatoria;
    jQuery("#dedicatoria-libro").val(dedicatoria);


}

function mostrarDatosLibroIngles(libro, sexo, nombre) {
    console.log("Libro: "+libro);
    console.log("Sexo: "+sexo);
    console.log("Nombre: "+nombre);
    /*CSS*/
    /*margins*/
    /**
     * Mama:
     * id="capucha-madre-pre"
     * id="image_demo4"
     * class="imgcrp-madre"
     * imagen: .imgcrp-madre img
     *
     * Papa:
     * id="capucha-padre-pre"
     * id="image_demo5"
     * class="imgcrp-padre"
     * .imgcrp-padre img
     *
     * Retrato familiar:
     * id="image_demo2"
     * id="retrato-familiar-pre"
     * class="cr-viewport cr-vp-square"
     *
     * Retrato niño:
     * id="image_demo3"
     * id="retrato-nino-pre"
     * class="cr-viewport cr-vp-square"
     *
     * */
    let margin_top_image_demo;
    let top_imgcrp_nino;
    let height_imgcrp_nino;
    let left_imgcrp_nino;
    let url_capucha;
    let url_caratula;
    let nombrem_color;
    let pre_dedicatoria = "Dear ";
    let dedicatoria = nombre+",\nThe real magic to make all your dreams come true is within you!";
    let capucha_nino_width;
    let url_capucha_mama;
    let imgcrp_madre_top;
    let imgcrp_madre_left;
    let img_madre_width;
    let url_capucha_papa;
    let image_demo5_margin_top;
    let imgcrp_papa_top;
    let imgcrp_papa_left;
    let imgcrp_papa_height;
    let imgcrp_papa_margin_top;
    let img_papa_width;
    let retrato_nino_pre_width;
    let retrato_nino_pre_height;

    url_caratula = '/wp-content/themes/generic/img_lib/595px/'+sexo+'/book-creation-banner.png';
    url_caratula_2 = '/wp-content/themes/generic/img_lib_2/595px/'+sexo+'/book-2-creation-banner.png';
    if (libro == 1){
        console.log("Libro 1 seleccionado.");
        capucha_nino_width = "260px";
        url_capucha = '/wp-content/themes/generic/img_lib/595px/'+sexo+'/velo-2.png';
        left_imgcrp_nino = "7px";
        url_capucha_mama = "/wp-content/themes/generic/img_lib/595px/veloreina-2.png";
        imgcrp_madre_top = "-27px";
        imgcrp_madre_left = "7px";
        img_madre_width = "260px";
        url_capucha_papa = "/wp-content/themes/generic/img_lib/595px/velorey-2.png";
        image_demo5_margin_top = "65px";
        imgcrp_papa_top = "8px";
        imgcrp_papa_left = "7px";
        imgcrp_papa_height = "389px";
        imgcrp_papa_margin_top = "-80px";
        img_papa_width = "260px";
        retrato_nino_pre_width = "260px";
        retrato_nino_pre_height = "300px";

        if (sexo == "m"){
            console.log("Sexo femenino.");
            margin_top_image_demo = "40px";
            top_imgcrp_nino = "11px";
            height_imgcrp_nino = "322px";
            nombrem_color = "#653f88";
        }else if(sexo == "h"){
            console.log("Sexo masculino.");
            margin_top_image_demo = "80px";
            top_imgcrp_nino = "8px";
            height_imgcrp_nino = "389px";
            nombrem_color = "#006cb5";
        }

        /*visibilidad de campos*/
        $(".form-info-libroinic .campo").show();

    }else if(libro == 2){
        console.log("Libro 2 seleccionado.");
        nombrem_color = "#4f0000";
        capucha_nino_width = "290px";
        url_capucha = '/wp-content/themes/generic/img_lib_2/595px/'+sexo+'/capuchita.png';
        margin_top_image_demo = "30px";
        top_imgcrp_nino = "18px";
        height_imgcrp_nino = "403px";
        left_imgcrp_nino = "5px";
        url_capucha_mama = "/wp-content/themes/generic/img_lib_2/595px/capuchita-mama.png";
        imgcrp_madre_top = "42px";
        imgcrp_madre_left = "-7px";
        img_madre_width = "218px";
        url_capucha_papa = "/wp-content/themes/generic/img_lib_2/595px/capuchita-papa.png";
        image_demo5_margin_top = "0";
        imgcrp_papa_top = "26px";
        imgcrp_papa_left = "5px";
        imgcrp_papa_height = "319px";
        imgcrp_papa_margin_top = "0";
        img_papa_width = "240px";
        retrato_nino_pre_width = "300px";
        retrato_nino_pre_height = "260px";

        if (sexo == "m"){
            console.log("Sexo femenino.");
        }else if(sexo == "h"){
            console.log("Sexo masculino.");

        }

        /*visibilidad de campos*/
        $(".form-info-libroinic .campo:not(:first-child)").hide();

    }

    /*CONFIGURAMOS LOS VALORES */
    /*NIÑO*/
    jQuery('.capucha-nino').attr("src",url_capucha).css("width", capucha_nino_width);
    jQuery('.capucha-nino').closest('.imgcrp-nino').css({
        "top":top_imgcrp_nino,
        "height":height_imgcrp_nino,
        "left":left_imgcrp_nino,
    });
    jQuery('.capucha-nino').closest('#image_demo').css("margin-top", margin_top_image_demo);
    jQuery('.libro-img-wrapp .nombrem, .libro-img-wrapp #p1-1-1').css("color",nombrem_color);
    /*MAMA*/
    jQuery('.imgcrp-madre img,#capucha-madre-pre').attr("src",url_capucha_mama);
    jQuery('.imgcrp-madre').css({
        "top":imgcrp_madre_top,
        "left":imgcrp_madre_left,
    });
    jQuery('.imgcrp-madre img').css("width",img_madre_width);
    /*PAPA*/
    jQuery('#image_demo5').css("margin-top",image_demo5_margin_top);
    jQuery('.imgcrp-padre img,#capucha-padre-pre').attr("src",url_capucha_papa);
    jQuery('.imgcrp-padre').css({
        "top":imgcrp_papa_top,
        "left":imgcrp_papa_left,
        "height":imgcrp_papa_height,
        "margin-top":imgcrp_papa_margin_top,
    });
    jQuery('.imgcrp-padre img').css("width",img_papa_width);
    /*RETRATO NIÑO*/
    jQuery('#retrato-nino-pre').css({
        "width":retrato_nino_pre_width,
        "height":retrato_nino_pre_height,
    });

    /*CARATULA*/
    jQuery('.btn-pasosm-wrapp .libro-img').attr("src",url_caratula);
    jQuery('.btn-pasosm-wrapp .libro-img-2').attr("src",url_caratula_2);
    /*DEDICATORIA*/
    dedicatoria = pre_dedicatoria+dedicatoria;
    jQuery("#dedicatoria-libro").val(dedicatoria);


}

function croppieImagenRetrato(libro) {
    console.log("creando croppie para retrato de niño");
    let viewport_width;
    let viewport_height;
    let boundary_width;
    let boundary_height;

    if (libro == 1){
        console.log("croppie de libro: "+1);
        viewport_width = 205;
        viewport_height = 295;
        boundary_width = 260;
        boundary_height = 330;
    }else if(libro == 2){
        console.log("croppie de libro: "+2);
        viewport_width = 295;
        viewport_height = 205;
        boundary_width = 330;
        boundary_height = 260;
    }

    $image_crop3 = $('#image_demo3').croppie({
        enableExif: true,
        viewport: {
            width:viewport_width,
            height:viewport_height,
            type:'square'
        },
        boundary:{
            width:boundary_width,
            height:boundary_height
        },
        enableOrientation: true
    });

    return $image_crop3;
}

function croppieImagenPapa(libro) {
    console.log("creando croppie para rostro de papa");
    let viewport_width;
    let viewport_height;
    let boundary_width;
    let boundary_height;

    if (libro == 1){
        console.log("croppie de papa libro: "+1);
        viewport_width = 200;
        viewport_height = 200;
        boundary_width = 300;
        boundary_height = 300;
    }else if(libro == 2){
        console.log("croppie de papa libro: "+2);
        viewport_width = 150;
        viewport_height = 200;
        boundary_width = 200;
        boundary_height = 300;
    }

    $image_crop5 = $('#image_demo5').croppie({
        enableExif: true,
        viewport: {
            width:viewport_width,
            height:viewport_height,
            type:'square'
        },
        boundary:{
            width:boundary_width,
            height:boundary_height
        },
        enableOrientation: true
    });

    return $image_crop5;
}


/**/


/**/