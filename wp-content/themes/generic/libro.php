<?php
//IMPORTANTE: id="pag-6" y id="pag-7" tienen su primera imagen con trasparencia cambiadas
if(isset($_GET["sexo"])){
    $sexo=$_GET["sexo"];
}else{
    //echo "<script> window.location.href='index.php';</script>";
}

if(isset($_GET["nombre"])){
    //$nombre=urldecode($_GET["nombre"]);
    $nombre =urldecode($_GET["nombre"]);
}else{
    //echo "<script> window.location.href='index.php';</script>";
}
if(isset($_GET["fec_nin"])){
    //$nombre=urldecode($_GET["nombre"]);
    $fecha_nino =urldecode($_GET["fec_nin"]);
    $partes_fecha = explode("-", $fecha_nino);
    $dia_nac= $partes_fecha[0];   
    $mes_nac= $partes_fecha[1];    
    $ano_nac= $partes_fecha[2];
    
    $now = time(); // or your date as well
    
    //Offset, el número de meses significa cuánto tiempo tienes de pasado el cumpleaños para regalar por el año que pasó
    $now = strtotime('-1 month', $now);
    
    $your_date = strtotime("$ano_nac-$mes_nac-$dia_nac");
    $datediff = $now - $your_date;
    $anos_nino= ceil($datediff / (60 * 60 * 24 * 365));
    
    /*
    echo $datediff."<br>";
    echo ceil($datediff / (60 * 60 * 24 * 365));
    */
    $mes_txt_nac="";
    switch ((int)$mes_nac) {
        case 1:
            $mes_txt_nac = "enero";
            break;
        case 2:
            $mes_txt_nac = "febrero";
            break;
        case 3:
            $mes_txt_nac = "marzo";
            break;
        case 4:
            $mes_txt_nac = "abril";
            break;
        case 5:
            $mes_txt_nac = "mayo";
            break;
        case 6:
            $mes_txt_nac = "junio";
            break;
        case 7:
            $mes_txt_nac = "julio";
            break;
        case 8:
            $mes_txt_nac = "agosto";
            break;
        case 9:
            $mes_txt_nac = "septiembre";
            break;
        case 10:
            $mes_txt_nac = "octubre";
            break;
        case 11:
            $mes_txt_nac = "noviembre";
            break;
        case 12:
            $mes_txt_nac = "diciembre";
            break;
    }
    
}
if(isset($_GET["nbr_pap"])){
    //$nombre_padre=urldecode($_GET["nbr_pap"]);
    $nombre_padre = urldecode($_GET["nbr_pap"]);
}
if(isset($_GET["nbr_mad"])){
    //$nombre_madre=urldecode($_GET["nbr_mad"]);
    $nombre_madre=urldecode($_GET["nbr_mad"]);
}
if(isset($_GET["nbr_gat"])){
    //$nombre_madre=urldecode($_GET["nbr_mad"]);
    $nombre_gato=urldecode($_GET["nbr_gat"]);
}
if(isset($_GET["nbr_fam"])){
    //$nombre_madre=urldecode($_GET["nbr_mad"]);
    $nombre_familiar=urldecode($_GET["nbr_fam"]);
}
if(isset($_GET["ded"])){
    //$dedicatoria=urldecode($_GET["ded"]);
    $dedicatoria=stripslashes(urldecode($_GET["ded"]));
}
if(isset($_GET["ciu_nac"])){
   $ciudad_nacimiento=stripslashes(urldecode($_GET["ciu_nac"]));

}
if(isset($_GET["can_fav"])){
   $cancion_favorita=stripslashes(urldecode($_GET["can_fav"]));

}
if(isset($_GET["com_fav"])){
   $comida_favorita=stripslashes(urldecode($_GET["com_fav"]));

}
if(isset($_GET["jug_fav"])){
   $jugete_favorito=stripslashes(urldecode($_GET["jug_fav"]));

}
if(isset($_GET["act_fav"])){
   $actividad_favorita=stripslashes(urldecode($_GET["act_fav"]));

}
if(isset($_GET["bio"])){
   $biografia=stripslashes(urldecode($_GET["bio"]));

}

//Arreglo de break line en html para hacer echo en los textarea o inputs
$breaks = array("<br />","<br>","<br/>");  




//CONVERTIR NUMERO A LETRA
//REFERENCIA: http://php.net/manual/es/function.strval.php
//OTRO EJEMPLO: https://glib.org.mx/article.php?story=20030711113025879
$nwords = array( "cero", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete",
                   "ocho", "nueve", "diez", "once", "doce", "trece",
                   "catorce", "quince", "dieciséis", "diecisiete", "dieciocho",
                   "diecinueve", "veint", 30 => "treinta", 40 => "cuarenta",
                   50 => "cincuenta", 60 => "sesenta", 70 => "setenta", 80 => "ochenta",
                   90 => "noventa" );
function int_to_words($x) {
   global $nwords;

   if(!is_numeric($x))
      $w = '#';
   else if(fmod($x, 1) != 0)
      $w = '#';
   else {
      if($x < 0) {
         $w = 'minus ';
         $x = -$x;
      } else
         $w = '';
      // ... now $x is a non-negative integer.

      if($x < 20)   // 0 to 20
         $w .= $nwords[$x];
      else if($x < 30) {   // 20 to 29
         $w .= $nwords[10 * floor($x/10)];
         $r = fmod($x, 10);
         if($r > 0){
             $w .= 'i'. $nwords[$r];
         }else{
             $w .= 'e';
         }
            
      }  else if($x < 100) {   // 21 to 99
         $w .= $nwords[10 * floor($x/10)];
         $r = fmod($x, 10);
         if($r > 0)
            $w .= ' y '. $nwords[$r];
      } else if($x < 1000) {   // 100 to 999
         $w .= $nwords[floor($x/100)] .' ciento';
         $r = fmod($x, 100);
         if($r > 0)
            $w .= ' y '. int_to_words($r);
      } else if($x < 1000000) {   // 1000 to 999999
         $w .= int_to_words(floor($x/1000)) .' mil';
         $r = fmod($x, 1000);
         if($r > 0) {
            $w .= ' ';
            if($r < 100)
               $w .= 'y ';
            $w .= int_to_words($r);
         }
      } else {    //  millions
         $w .= int_to_words(floor($x/1000000)) .' millon';
         $r = fmod($x, 1000000);
         if($r > 0) {
            $w .= ' ';
            if($r < 100)
               $word .= 'y ';
            $w .= int_to_words($r);
         }
      }
   }
   return $w;
}
//echo int_to_words($anos_nino);
//END CONVERTIR NUMERO A LETRA

//LINK PARA AÑADIR AL CARRITO EL LIBRO
$link = $_SERVER['SERVER_NAME']."/carrito/?add-to-cart=105";

/*linK=
    http://18.224.253.62/libro/?sexo=h&nombre=Jes%C3%BAs&nbr_pap=Jos%C3%A9&nbr_mad=Maria&ded=Para%20mi%20hermoso%20pr%C3%ADncipe%20Jes%C3%BAs%20en%20su%20cumplea%C3%B1os.%3Cbr%20%2F%3ETe%20amamos%20mucho%20papi%2C%20y%20mami.&bio=Naci%C3%B3%20el%2024%20de%20julio%20de%202017.%3Cbr%20%2F%3EEs%20una%20ni%C3%B1a%20vivaz%20y%20risue%C3%B1a.%3Cbr%20%2F%3ELe%20encanta%20ver%20videos%20de%20Little%20Kitten%20en%20Youtube%2C%20porque%20le%20gustan%20los%20gatos%20como%20su%20gata%20%22Lola%22.%3Cbr%20%2F%3EAprende%20todo%20de%20su%20querida%20hermanita%20mayor%20Kianna%20y%20le%20gusta%20jugar%20con%20sus%20juguetes.%3Cbr%20%2F%3EYa%20aprendi%C3%B3%20a%20caminar%20y%20se%20desplaza%20por%20toda%20la%20casa.&img_0=1540332869&img_1=1540318626&img_2=1540333162&img_3=1540333182&img_4=1540333205
*/
?>
<?php /* Template Name: Libro */ ?>
<?php get_header(); ?>

<link rel="stylesheet" href="../wp-content/themes/generic/css/bootstrap-mix.css">
<link href="../wp-content/themes/generic/css/croppie.css" rel="stylesheet">
<link rel="stylesheet" href="/wp-content/themes/generic/css/slick.css" type="text/css" media="all">
<link rel="stylesheet" href="/wp-content/themes/generic/css/slick-theme.css" type="text/css" media="all">
<link href="../wp-content/themes/generic/css/princ<?php echo ($sexo == "h" ? "2" : ""); ?>.css?ver=<?php echo INOLOOP_VERSION; ?>" rel="stylesheet">
<link href="../wp-content/themes/generic/css/bootstrap-datetimepicker.css" rel="stylesheet">
<main id="content">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="entry-content">
    <div class="vis-libro seccion marco">
        <div class="castillo-l adorno">
            <img src="/wp-content/imagenes/Castillo.png" alt="castillo">
        </div>
        <div class="titulo-pagina">
            <h1 class="tit-inicio-mobile">Mira el cuento que has creado para <span class="nombre-libro"><?php echo $nombre; ?></span></h1>

        </div>
        <div class="subir marco subir-foto-libro" style="display:none">
            <div class="imagen-ref adorno"  href="#subir-foto-mod" data-target="#subir-foto-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" >
                <div class="adorno imagen-1">

                </div>                
                <div class="adorno camara">
                <img src="/wp-content/imagenes/Camara.png" alt="camara">

                </div>
                <div class="adorno linea">
                    <img src="/wp-content/imagenes/Linea.png" alt="linea">
                </div>
                <div class="adorno descrip">
                    Subir fotos
                </div>

            </div>
            
        </div>
        <div class="libro-estado-wrapp">
            <div class="libro-estado-inn">
                <div class="libro-estado-gif">
                    <img src="../wp-content/themes/generic/img/loader.gif">
                </div>
                <div class="libro-estado-txt">
                    Tu libro se está cargando...
                </div>
            </div>
        </div>
        <div class="libro marco">
            <div class="unicornio-l adorno">
                <img src="/wp-content/imagenes/Unicornio-l.png" alt="unicornio">
            </div>
            
            <div class="regalo-l adorno">
                <img src="/wp-content/imagenes/Regalo.png" alt="regalo">
            </div>
            <div class="click-here">
                <img src="/wp-content/themes/generic/img/click-here-2.png" alt="regalo">
            </div>
            <div class="visualizador-libro banner estrecho">
                
                
                
                
                
                
                

                <div class="visualizador-perso-wrapper">
                    <div class="libro-wrapper" style="display:none">
                        <div class="libro-inner">
                            <div class="libro-pagina pagina-activa-derecha" id="pag-1" data-id="1">
                                <div class="libro-hoja hoja-adelante">
                                    <div class="libro-img-wrapp" style="overflow:hidden">
                                        <?php  if(isset($_GET['img_0'])){ ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/1.png" style="position: relative; background-color: inherit;">
                                        <img class="libro-img-ade" id="imga-0-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
                                        <?php  }else{  ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/1.jpg" style="position: relative">
                                        <?php }  ?>
                                        <div class="texto-agregar" id="p1-1"><div id="p1-1-1"><?php echo $nombre; ?></div></div>
                                    </div>
                                </div>
                                <div class="libro-hoja hoja-atras">
                                    <div class="libro-img-wrapp">
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/2.jpg">
                                    </div>
                                </div>
                            </div>
                            <div class="libro-pagina pagina-siguiente" id="pag-2" data-id="2">
                                <div class="libro-hoja hoja-adelante">
                                    <div class="libro-img-wrapp">
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/3.jpg">
                                        <?php  if(isset($_GET['img_1'])){ ?>
                                        <img class="libro-img-ade" id="imga-2-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_1'];  ?>.png">
                                        <?php  }else{  ?>
                                        <div class="texto-agregar" id="p2-0">
                                            <div id="p2-0-1">
                                                RETRATO<br>FAMILIAR
                                            </div>
                                        </div>
                                        <?php }  ?>
                                        <div class="texto-agregar" id="p2-1">
                                            <div id="p2-1-1"><?php 
                                                if(isset($_GET["ded"])){
                                                    echo $dedicatoria;
                                                }else{
                                                    echo "Querid".($sexo == "h" ? "o" : "a")." $nombre,<br> ¡La verdadera magia para hacer realidad todos tus sueños está en tu interior!";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="libro-hoja hoja-atras">
                                    <div class="libro-img-wrapp">
                                        <div class="texto-agregar" id="p2-2"><?php echo $nombre; ?></div>
                                        <div class="texto-agregar" id="p2-3">
                                            <div id="p2-3-1">
                                                Nació <?php echo (isset($_GET["fec_nin"]) ? "el $dia_nac de $mes_txt_nac del $ano_nac" : "un día muy especial"); ?> en <?php echo (isset($_GET["ciu_nac"]) ? "la hermosa ciudad de $ciudad_nacimiento" : "una hermosa ciudad"); ?>. Su canción favorita es <?php echo (isset($_GET["can_fav"]) ? $cancion_favorita : "aquella con la que baila sin parar"); ?>. Su comida favorita es <?php echo (isset($_GET["com_fav"]) ? $comida_favorita : "la preparada con amor en casa"); ?>. El juguete que más le gusta es <?php echo (isset($_GET["jug_fav"]) ? $jugete_favorito :  ($sexo == "h" ? "aquél que lo acompaña siempre para dormir" : "aquél que la acompaña siempre para dormir") ); ?>. Le encanta <?php echo (isset($_GET["act_fav"]) ? $actividad_favorita : "leer cuentos y vivir muchas aventuras con sus amigos"); ?>. Ama reír y disfrutar al máximo el tiempo con su familia. <?php echo (isset($_GET["bio"]) ? $biografia : ""); ?>
                                            </div>
                                        </div>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/4.jpg">
                                    </div>
                                </div>
                            </div>
                            <div class="libro-pagina" id="pag-3" data-id="3">
                                <div class="libro-hoja hoja-adelante">
                                    <div class="libro-img-wrapp">
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/5.jpg">
                                        <?php  if(isset($_GET['img_2'])){ ?>
                                        <img class="libro-img-ade" id="imga-3-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_2'];  ?>.png">
                                        <?php  }else{  ?>
                                        <div class="texto-agregar" id="p2-01">
                                            <div id="p2-0-1">
                                                RETRATO<br>NIÑ<?php echo ($sexo == "h" ? "O" : "A"); ?>
                                            </div>
                                        </div>
                                        <?php }  ?>
                                    </div>
                                </div>
                                <div class="libro-hoja hoja-atras">
                                    <div class="libro-img-wrapp">
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/6.jpg">
                                        <img class="libro-img-ade" id="imga-3-castillos" src="../wp-content/themes/generic/img_lib/595px/castillo.png">
                                        <div class="texto-agregar" id="p3-1">
                                            <div id="p3-1-1">
                                            <?php  
                                                if($sexo == "h"){
                                                    if(isset($_GET['fec_nin'])){
                                                ?>
                                                
                                                    <span class="first-letter"><?php echo substr($nombre, 0, 1); ?></span><?php echo  substr($nombre, 1); ?> es un dulce y valiente príncipe, amado por todos los habitantes del reino. Llegó al mundo un mágico <?php echo $dia_nac; ?> de <?php echo $mes_txt_nac; ?> de <?php echo $ano_nac; ?>. Hoy es el día más esperado por todos porque el príncipe celebra su fiesta de coronación.
                                                
                                            <?php   }else{ ?>
                                                
                                                    <span class="first-letter"><?php echo substr($nombre, 0, 1); ?></span><?php echo  substr($nombre, 1); ?> es un dulce y valiente príncipe, amado por todos los habitantes del reino. Hoy es el día más esperado por todos porque el príncipe celebra su fiesta de coronación.

                                            <?php  
                                                    }
                                                }else{  
                                                    if(isset($_GET['fec_nin'])){
                                                ?>
                                                
                                                    <span class="first-letter"><?php echo substr($nombre, 0, 1); ?></span><?php echo  substr($nombre, 1); ?> es una dulce y valiente princesa, amada por todos los habitantes del reino. Llegó al mundo un mágico <?php echo $dia_nac; ?> de <?php echo $mes_txt_nac; ?> de <?php echo $ano_nac; ?>. Hoy es el día más esperado por todos porque la princesa celebra su fiesta de coronación.
                                                
                                            <?php   }else{ ?>
                                                
                                                    <span class="first-letter"><?php echo substr($nombre, 0, 1); ?></span><?php echo  substr($nombre, 1); ?> es una dulce y valiente princesa, amada por todos los habitantes del reino. Hoy es el día más esperado por todos porque la princesa celebra su fiesta de coronación.
                                                
                                            <?php 
                                                     }
                                                }  ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="libro-pagina" id="pag-4" data-id="4">
                                <div class="libro-hoja hoja-adelante">
                                    <div class="libro-img-wrapp">
                                        <?php  if(isset($_GET['img_0'])){ ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/7-min.png">
                                        <img class="libro-img-atras" id="imga-4-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
                                        <?php  }else{  ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/7.jpg">
                                        <?php }  ?>
                                    </div>
                                </div>
                                <div class="libro-hoja hoja-atras">
                                    <div class="libro-img-wrapp">
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/8.jpg">
                                        <img class="libro-img-ade" id="imga-4-candelabro" src="../wp-content/themes/generic/img_lib/595px/candelabro.png">
                                        <div class="texto-agregar" id="p4-1">
                                            <div id="p4-1-1">
                                                <span class="first-letter">E</span>l rey <?php echo (isset($_GET["nbr_pap"]) ?  $nombre_padre: ""); ?> y la reina <?php echo (isset($_GET["nbr_mad"]) ?  $nombre_madre: ""); ?> han preparado una fiesta de ensueño. Todos en el palacio se han esmerado: deliciosos pasteles de todos los sabores perfuman la cocina y la decoración con globos que cambian de color es espectacular. Las obedientes lechuzas reales se ocuparon de enviar las invitaciones a todos los príncipes y princesas del mundo. Hasta <?php echo ( isset($_GET["sex_gat"]) ? ( $_GET["sex_gat"] == "1" ? "el querido gato" : "la querida gata") : "el querido gato"); ?> <?php echo (isset($_GET["nbr_gat"]) ? $nombre_gato : "Tommy"); ?>, que es <?php echo ( isset($_GET["sex_gat"]) ? ( $_GET["sex_gat"] == "1" ? "el mejor amigo" : "la mejor amiga") : "el mejor amigo"); ?> de <?php echo $nombre; ?>, ha preparado un espectáculo de malabarismo para entretener a los invitados.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="libro-pagina" id="pag-5" data-id="5">
                                <div class="libro-hoja hoja-adelante">
                                    <div class="libro-img-wrapp">
                                        <?php  if(isset($_GET['img_3']) && isset($_GET['img_4'])){ ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/9-min.png">
                                        <img class="libro-img-atras" id="imga-5-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_3'];  ?>.png">
                                        <img class="libro-img-atras" id="imga-5-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_4'];  ?>.png">
                                        <?php  }elseif(isset($_GET['img_3'])){  ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/9-sinrey.png">
                                        <img class="libro-img-atras" id="imga-5-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_3'];  ?>.png">
                                        <?php }elseif(isset($_GET['img_4'])){  ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/9-sinreina.png">
                                        <img class="libro-img-atras" id="imga-5-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_4'];  ?>.png">
                                        <?php }else{  ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/9.jpg">
                                        <?php }  ?>
                                    </div>
                                </div>
                                <div class="libro-hoja hoja-atras">
                                    <div class="libro-img-wrapp">
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/10.jpg">
                                        <img class="libro-img-ade" id="imga-5-regalo" src="../wp-content/themes/generic/img_lib/595px/regalos.png">
                                        <div class="texto-agregar" id="p5-1">
                                            <div id="p5-1-1">
                                                <span class="first-letter">H</span>ermosos obsequios provenientes de lejanos países inundan el salón principal. <?php echo $nombre; ?> está muy emocionad<?php echo ($sexo == "h" ? "o" : "a"); ?>, y abre el obsequio más grande y vistoso. Pero... ¡oh, sorpresa! Un feroz dragón sale del regalo, riéndose a carcajadas.
                                            </div>
                                            <div id="p5-1-2">
                                                —¡Ja, ja, ja, ja! ¡Has caído en la trampa, niñit<?php echo ($sexo == "h" ? "o" : "a"); ?>!<br/>
                                                —gruñe y, de inmediato, captura <?php echo ( isset($_GET["sex_gat"]) ? ( $_GET["sex_gat"] == "1" ? "al gato" : "a la gata") : "al gato"); ?> <?php echo (isset($_GET["nbr_gat"]) ? $nombre_gato : "Tommy"); ?> entre sus garras, para escapar volando a través de la ventana del palacio.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="libro-pagina" id="pag-6" data-id="6">
                                <div class="libro-hoja hoja-adelante">
                                    <div class="libro-img-wrapp">
                                        
                                        <?php  if(isset($_GET['img_0'])){ ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/11-min.png">
                                        <img class="libro-img-atras" id="imga-6-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
                                        <?php  }else{  ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/13.jpg">
                                        <?php }  ?>
                                    </div>
                                </div>
                                <div class="libro-hoja hoja-atras">
                                    <div class="libro-img-wrapp">
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/12.jpg">
                                        <img class="libro-img-ade" id="imga-6-unirev" src="../wp-content/themes/generic/img_lib/595px/uni-rev.png">
                                        <div class="texto-agregar" id="p6-1">
                                            <div id="p6-1-1">
                                                <span class="first-letter"><?php echo substr($nombre, 0, 1); ?></span><?php echo  substr($nombre, 1); ?> muy triste y asustad<?php echo ($sexo == "h" ? "o" : "a"); ?>, se lamenta: «¡Oh, no, <?php echo (isset($_GET["nbr_gat"]) ? $nombre_gato : "Tommy"); ?>! ¡Qué voy a hacer ahora!».
                                            </div>
                                            <div id="p6-1-2">
                                                Luego intenta calmarse y descubre, muy bien escondido, un pequeño y sencillo regalito de <?php echo (isset($_GET["nbr_fam"]) ? $nombre_familiar : "Nichola"); ?>, una 
                                            <?php if(isset($_GET["tip_fam"])){
                                                    switch ($_GET["tip_fam"]) {
                                                        case "1":
                                                            echo "tía";
                                                            break;
                                                        case "2":
                                                            echo "abuelita";
                                                            break;
                                                        case "3":
                                                            echo "prima";
                                                            break;
                                                        default:
                                                           echo "tía";
                                                    }
                                                }else{ echo "tía";} ?> muy querida que no pudo ir a su fiesta. Lo abre, y es una flauta dulce, la cual comienza a tocar. De pronto, un hermoso y colorido unicornio aparece surcando los cielos e ingresa en el salón por la ventana.
                                            </div>
                                            <div id="p6-1-3">
                                                —¡He venido a ayudarte, <?php echo ($sexo == "h" ? "príncipe" : "princesa"); ?> <?php echo $nombre; ?>! ¡Tu valentía y la magia de la flauta hicieron posible que esté aquí!<br>
                                                —dice el unicornio con voz melodiosa e invita a<?php echo ($sexo == "h" ? "l príncipe" : " la princesa"); ?> a subir sobre su lomo.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="libro-pagina" id="pag-7" data-id="7">
                                <div class="libro-hoja hoja-adelante">
                                    <div class="libro-img-wrapp">
                                        <?php  if(isset($_GET['img_0'])){ ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/13-min.png">
                                        <img class="libro-img-atras" id="imga-7-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
                                        <?php  }else{  ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/11.jpg">
                                        <?php }  ?>
                                    </div>
                                </div>
                                <div class="libro-hoja hoja-atras">
                                    <div class="libro-img-wrapp">
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/14.jpg">
                                        <img class="libro-img-ade" id="imga-7-buho" src="../wp-content/themes/generic/img_lib/595px/buho.png">
                                        <div class="texto-agregar" id="p7-1">
                                            <div id="p7-1-1">
                                                <span class="first-letter"><?php echo substr($nombre, 0, 1); ?></span><?php echo  substr($nombre, 1); ?> y el unicornio mágico, a pesar del peligro, salen a recorrer el reino entero para buscar al dragón y rescatar a <?php echo (isset($_GET["nbr_gat"]) ? $nombre_gato : "Tommy"); ?>.
                                            </div>
                                            <div id="p7-1-2">
                                                Atraviesan empinadas montañas, así como el bosque tenebroso, que es el hogar de muchos animales mágicos.
                                            </div>
                                            <div id="p7-1-3">
                                                —¡Ijujú! ¡Ya están cerca de la guarida del dragón, tengan cuidado! —exclama el sabio búho.
                                            </div>
                                            <div id="p7-1-4">
                                                A lo lejos alcanzan a ver las torres del oscuro castillo donde vive el malvado dragón.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="libro-pagina" id="pag-8" data-id="8">
                                <div class="libro-hoja hoja-adelante">
                                    <div class="libro-img-wrapp">
                                        <?php  if(isset($_GET['img_0'])){ ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/15-min.png">
                                        <img class="libro-img-atras" id="imga-8-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
                                        <?php  }else{  ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/15.jpg">
                                        <?php }  ?>
                                    </div>
                                </div>
                                <div class="libro-hoja hoja-atras">
                                    <div class="libro-img-wrapp">
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/16.jpg">
                                        <img class="libro-img-ade" id="imga-8-drag-rev" src="../wp-content/themes/generic/img_lib/595px/dragon-rev.png">
                                        <div class="texto-agregar" id="p8-1">
                                            <div id="p8-1-1">
                                                <span class="first-letter">L</span>legan hasta la torre más alta donde <?php echo ( isset($_GET["sex_gat"]) ? ( $_GET["sex_gat"] == "1" ? "el gato" : "la gata") : "el gato"); ?> <?php echo (isset($_GET["nbr_gat"]) ? $nombre_gato : "Tommy"); ?> se encuentra encerrad<?php echo ( isset($_GET["sex_gat"]) ? ( $_GET["sex_gat"] == "1" ? "o" : "a") : "o"); ?>.
                                            </div>
                                            <div id="p8-1-2">
                                                —¡Grrrooarr! ¡Nunca podrán vencerme! —truena el dragón, que sale de su escondite y vuela hacia <?php echo ($sexo == "h" ? "el príncipe" : "la princesa"); ?>.
                                            </div>
                                            <div id="p8-1-3">
                                                —¡Valiente <?php echo ($sexo == "h" ? "príncipe" : "princesa"); ?>, recuerda la magia que tienes en tus manos! —susurra el unicornio.
                                            </div>
                                            <div id="p8-1-4">
                                                <?php echo $nombre; ?> se arma de valor y toca una bella melodía con su flauta. La música adormece como un encantamiento al dragón, quien cae desde lo alto en pleno vuelo. ¡<?php echo ($sexo == "h" ? "El príncipe" : "La princesa"); ?> y el unicornio finalmente logran rescatar a <?php echo (isset($_GET["nbr_gat"]) ? $nombre_gato : "Tommy"); ?>!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="libro-pagina" id="pag-9" data-id="9">
                                <div class="libro-hoja hoja-adelante">
                                    <div class="libro-img-wrapp">
                                        <?php  if(isset($_GET['img_0'])){ ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/17-min.png">
                                        <img class="libro-img-atras" id="imga-9-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
                                        <?php  }else{  ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/17.jpg">
                                        <?php }  ?>
                                    </div>
                                </div>
                                <div class="libro-hoja hoja-atras">
                                    <div class="libro-img-wrapp">
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/18.jpg">
                                        <img class="libro-img-ade" id="imga-9-unider" src="../wp-content/themes/generic/img_lib/595px/uni-der.png">
                                        <div class="texto-agregar" id="p9-1">
                                            <div id="p9-1-1">
                                                <span class="first-letter">U</span>na vez a salvo, en el palacio todo es felicidad porque <?php echo ($sexo == "h" ? "el valiente príncipe" : "la valiente princesa"); ?>, con ayuda del unicornio mágico, consiguió derrotar al malvado dragón que aterrorizaba el reino.
                                            </div>
                                            <div id="p9-1-2">
                                                —¡Un hurra por <?php echo ($sexo == "h" ? "el príncipe" : "la princesa"); ?> <?php echo $nombre; ?> y su unicornio!<br>
                                                —proclaman los invitados que disfrutan de la fiesta y bailan al ritmo de la música.
                                            </div>
                                            <div id="p9-1-3">
                                                Agradecidos despiden al unicornio mágico que se va volando.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="libro-pagina" id="pag-10" data-id="10">
                                <div class="libro-hoja hoja-adelante">
                                    <div class="libro-img-wrapp">
                                        <?php  if(isset($_GET['img_0'])){ ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/19-min.png">
                                        <img class="libro-img-atras" id="imga-10-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
                                        <?php  }else{  ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/19.jpg">
                                        <?php }  ?>
                                    </div>
                                </div>
                                <div class="libro-hoja hoja-atras">
                                    <div class="libro-img-wrapp">
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/20.jpg">
                                        <img class="libro-img-ade" id="imga-10-mesa" src="../wp-content/themes/generic/img_lib/595px/mesa.png">
                                        <div class="texto-agregar" id="p10-1">
                                            <div id="p10-1-1">
                                                <span class="first-letter">E</span>n compañía de su familia y amigos, <?php echo $nombre; ?> sopla la vela de su pastel de coronación.
                                            </div>
                                            <div id="p10-1-2">
                                                —¡Miauu! ¡Cuéntanos cuál es tu deseo, <?php echo ($sexo == "h" ? "príncipe" : "princesa"); ?>!<br>
                                                —maúlla <?php echo ( isset($_GET["sex_gat"]) ? ( $_GET["sex_gat"] == "1" ? "el gato" : "la gata") : "el gato"); ?> <?php echo (isset($_GET["nbr_gat"]) ? $nombre_gato : "Tommy"); ?>
                                            </div>
                                            <div id="p10-1-3">
                                                <?php echo $nombre; ?> se siente agradecid<?php echo ($sexo == "h" ? "o" : "a"); ?> y emocionad<?php echo ($sexo == "h" ? "o" : "a"); ?>. Recuerda a su <?php if(isset($_GET["tip_fam"])){
                                                    switch ($_GET["tip_fam"]) {
                                                        case "1":
                                                            echo "tía";
                                                            break;
                                                        case "2":
                                                            echo "abuelita";
                                                            break;
                                                        case "3":
                                                            echo "prima";
                                                            break;
                                                        default:
                                                           echo "tía";
                                                    }
                                                }else{ echo "tía";} ?> <?php echo (isset($_GET["nbr_fam"]) ? $nombre_familiar : "Nichola"); ?> y su deseo es «nunca olvidar que las apariencias engañan y que los mejores regalos vienen del corazón».
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="libro-pagina" id="pag-11" data-id="11">
                                <div class="libro-hoja hoja-adelante">
                                    <div class="libro-img-wrapp">
                                        <?php  if(isset($_GET['img_0']) && isset($_GET['img_3']) && isset($_GET['img_4'])){ ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/21-min.png">
                                        <img class="libro-img-atras" id="imga-11-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
                                        <img class="libro-img-atras" id="imga-11-3" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_3'];  ?>.png">
                                        <img class="libro-img-atras" id="imga-11-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_4'];  ?>.png">
                                        <?php  }elseif(isset($_GET['img_0']) && isset($_GET['img_3'])){  ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/21-sinrey.png">
                                        <img class="libro-img-atras" id="imga-11-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
                                        <img class="libro-img-atras" id="imga-11-3" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_3'];  ?>.png">
                                        <?php }elseif(isset($_GET['img_0']) && isset($_GET['img_4'])){  ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/21-sinreina.png">
                                        <img class="libro-img-atras" id="imga-11-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
                                        <img class="libro-img-atras" id="imga-11-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_4'];  ?>.png">
                                        <?php }elseif(isset($_GET['img_3']) && isset($_GET['img_4'])){  ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/21-sinprinc.png">
                                        <img class="libro-img-atras" id="imga-11-3" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_3'];  ?>.png">
                                        <img class="libro-img-atras" id="imga-11-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_4'];  ?>.png">
                                        <?php }elseif(isset($_GET['img_0'])){  ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/21-sinrey-sinreina.png">
                                        <img class="libro-img-atras" id="imga-11-2" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_0'];  ?>.png">
                                        <?php }elseif(isset($_GET['img_3'])){  ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/21-sinrey-sinprinc.png">
                                        <img class="libro-img-atras" id="imga-11-3" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_3'];  ?>.png">
                                        <?php }elseif(isset($_GET['img_4'])){  ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/21-sinreina-sinprinc.png">
                                        <img class="libro-img-atras" id="imga-11-1" src="../wp-content/themes/generic/img_usr/<?php echo $_GET['img_4'];  ?>.png">
                                        <?php }else{  ?>
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/21.jpg">
                                        <?php }  ?>
                                    </div>
                                </div>
                                <div class="libro-hoja hoja-atras">
                                    <div class="libro-img-wrapp">
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/22.jpg">
                                        <div class="texto-agregar" id="p11-1">
                                            <div id="p11-1-1">
                                                ¡¡¡Mensajes especiales!!!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="libro-pagina" id="pag-12" data-id="12">
                                <div class="libro-hoja hoja-adelante">
                                    <div class="libro-img-wrapp">
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/23.jpg">
                                    </div>
                                </div>
                                <div class="libro-hoja hoja-atras">
                                    <div class="libro-img-wrapp">
                                        <img class="libro-img" src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/24.jpg">
                                        <div class="texto-agregar" id="p12-1">
                                            <div id="p12-1-1">
                                                www.soycuentaconmigo.com 
                                            </div>
                                            <div id="p12-1-2">
                                                info@soycuentaconmigo.com
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
                
                
                
                
                
                
                
            </div>
            
        </div>
        <div class="mobile-info-wrapp">
            <div class="mobile-info-inn">
                <div class="mobile-info">
                    <div class="uso-libr-info">
                        Haz click en las hojas para pasar las páginas
                    </div>
                    <div class="uso-libr-tip">
                        <small>Tip: gira el dispositivo para ver el cuento más grande</small>
                    </div>
                </div>
                <div class="botones-mobile-wrapp">
                    <div class="btn-crearm-wrapp">
                        <div class="btn-crearm-inn">
                            <a
                                onclick="jQuery( '.formato-tapa-wrapp,.botones-mobile-wrapp,.mobile-info' ).toggle()"
                                class="btn-crearm">
                                AÑADIR AL<br>CARRITO
                            </a>
                            <a
                                class="btn-outline-naranja btn-crearm-empieza"
                            >
                                EDITAR<br>CUENTO
                            </a>
                        </div>
                    </div>
                </div>
                <div class="formato-tapa-wrapp">
                    <div class="formato-tapa-inn">
                        <div class="formato-tapa-tit">
                            Elige el formato de tu cuento:
                        </div>
                        <div class="opciones-lib">
                            <a data-href="<?php echo $link."&variation_id=267&attribute_pa_tapa=pdf-digital&attribute_pa_tamano=2020&nombre_nino=$nombre&book_url=".urlencode($_SERVER['REQUEST_URI']) ?>" class="link-tipo-tapa" id="pedir-ctapa-267" onclick="setdedicatoria('267','pdf-digital','2020');">
                                <h4>PDF imprimible</h4>
                                <p>
                                    <?php
                                    $variacion=get_variation_price_by_id(105,267);
                                    $precio=$variacion->display_price;
                                    echo $precio;
                                    ?>
                                </p>
                            </a>

                            <a data-href="<?php echo $link."&variation_id=112&attribute_pa_tapa=blanda&attribute_pa_tamano=2020&nombre_nino=$nombre&book_url=".urlencode($_SERVER['REQUEST_URI']) ?>" class="link-tipo-tapa" id="pedir-ctapa-112" onclick="setdedicatoria('112','blanda','2020');">
                                <h4>20x20cm tapa blanda</h4>
                                <p>
                                    <?php
                                    $variacion=get_variation_price_by_id(105,112);
                                    $precio=$variacion->display_price;
                                    echo $precio;
                                    ?>
                                </p>
                            </a>

                            <a data-href="<?php echo $link."&variation_id=113&attribute_pa_tapa=dura&attribute_pa_tamano=2020&nombre_nino=$nombre&book_url=".urlencode($_SERVER['REQUEST_URI']) ?>" class="link-tipo-tapa" id="pedir-ctapa-113" onclick="setdedicatoria('113','dura','2020');">
                                <h4>20x20cm tapa dura</h4>
                                <p>
                                    <?php
                                    $variacion=get_variation_price_by_id(105,113);
                                    $precio=$variacion->display_price;
                                    echo $precio;
                                    ?>
                                </p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        <?php /*
        <div class="adicionales-libro-wrapp">
            <div class="adicionales">
                <h1>Cuento creado para <span class="nombre-libro"><?php echo $nombre; ?></h1>
                <p class="parrafo">Acompaña a<?php echo ($sexo == "h" ? "l príncipe " : " la princesa ").$nombre; ?> en una aventura fantástica de amistad, valentía y magia. ¿Podrá rescatar a su mejor amigo, el gato Tommy, de las garras del malvado dragón?</p>
                <form action="">
                    <div class="campo">
                        <p>Nombre de<?php echo ($sexo == "h" ? "l niño" : " la niña"); ?>:</p>
                        <input type="text" id="nombre-nino-libro" value="<?php echo $nombre; ?>">
                    </div>
                    <div class="form-group input-group date campo" id="fecha-formcont-wrapp">
                        <p>Cumpleaños de<?php echo ($sexo == "h" ? "l niño" : " la niña"); ?>:</p>
                        <input type="text" class="" id="fecha-formcont" onchange="verificarfecha('fecha-formcont', 'fecha-formcont-wrapp')"  value="<?php echo $fecha_nino; ?>">
                        <span class="input-group-addon" id="btn-dp-1" style="visibility: hidden;position: relative;left:5px;top: -78px;display: inline-block;">
                        </span>
                    </div>
                    <div class="campo">
                        <p>Nombre del padre:</p>
                        <input type="text" id="nombre-padre-libro" value="<?php echo (isset($_GET["nbr_pap"]) ? $nombre_padre : ""); ?>">
                    </div>
                    <div class="campo">
                        <p>Nombre de la madre:</p>
                        <input type="text" id="nombre-madre-libro" value="<?php echo (isset($_GET["nbr_mad"]) ? $nombre_madre : ""); ?>">
                    </div>
                    <div class="campo">
                        <p>Gato mascota:</p>
                        <select class="form-control campo-select" id="sexmasc-familiar-libro">
                            <option value="1" <?php echo ( isset($_GET["sex_gat"]) ? ( $_GET["sex_gat"] == "1" ? "selected" : "") : ""); ?>>Gatito</option>
                            <option value="2" <?php echo ( isset($_GET["sex_gat"]) ? ( $_GET["sex_gat"] == "2" ? "selected" : "") : ""); ?>>Gatita</option>
                        </select>
                        <input type="text" id="gato-madre-libro" value="<?php echo (isset($_GET["nbr_gat"]) ? $nombre_gato : "Tommy"); ?>">
                    </div>
                    <div class="campo">
                        <p>Familiar más cercana:</p>
                        <select class="form-control campo-select" id="tipo-familiar-libro">
                            <option value="1" <?php echo ( isset($_GET["tip_fam"]) ? ( $_GET["tip_fam"] == "1" ? "selected" : "") : ""); ?>>Tía</option>
                            <option value="2" <?php echo ( isset($_GET["tip_fam"]) ? ( $_GET["tip_fam"] == "2" ? "selected" : "") : ""); ?>>Abuelita</option>
                            <option value="3" <?php echo ( isset($_GET["tip_fam"]) ? ( $_GET["tip_fam"] == "3" ? "selected" : "") : ""); ?>>Prima</option>
                        </select>
                        <input type="text" id="nombre-familiar-libro" value="<?php echo (isset($_GET["nbr_fam"]) ? $nombre_familiar : "Nichola"); ?>">
                    </div>
                    <div class="campo">
                        <p>Dedicatoria:</p>
                        <textarea type="text" id="dedicatoria-libro" rows="3" class="inputs-texto" data-cuenta="cuentachar-ded" data-max="140"><?php 
                            if(isset($_GET["ded"])){
                                $dedicatoria = str_ireplace($breaks, "\r\n", $dedicatoria);  
                                echo $dedicatoria;
                            }else{
                                echo "Querid".($sexo == "h" ? "o" : "a")." $nombre,\n ¡La verdadera magia para hacer realidad todos tus sueños está en tu interior!";
                            }
                            ?></textarea>
                        <div>(<span id="cuentachar-ded">0</span>/140 caracteres)</div>
                    </div>
                    <div class="campo">
                        <p>Biografía:</p>
                        <p class="form-subcampo">Ciudad de nacimiento:</p>
                        <input type="text" id="ciudad-nacimiento-libro" value="<?php echo (isset($_GET["ciu_nac"]) ? $_GET["ciu_nac"] : ""); ?>">
                        <p class="form-subcampo">Canción favorita:</p>
                        <input style="text-transform:lowercase" type="text" id="cancion-favorita-libro" value="<?php echo (isset($_GET["can_fav"]) ? $_GET["can_fav"] : ""); ?>">
                        <p class="form-subcampo">Comida favorita:</p>
                        <input style="text-transform:lowercase" type="text" id="comida-favorita-libro" value="<?php echo (isset($_GET["com_fav"]) ? $_GET["com_fav"] : ""); ?>">
                        <p class="form-subcampo">Juguete favorito:</p>
                        <input style="text-transform:lowercase" type="text" id="juguete-favorito-libro" value="<?php echo (isset($_GET["jug_fav"]) ? $_GET["jug_fav"] : ""); ?>">
                        <p class="form-subcampo">Actividad favorita:</p>
                        <input style="text-transform:lowercase" type="text" id="actividad-favorita-libro" value="<?php echo (isset($_GET["act_fav"]) ? $_GET["act_fav"] : ""); ?>">
                        <p class="form-subcampo">Adicional a biografía:</p>
                        <textarea type="text" id="biografia-libro" rows="2" class="inputs-texto" data-cuenta="cuentachar-bio" data-max="30"><?php 
                            if(isset($_GET["bio"])){
                                $biografia = str_ireplace($breaks, "\r\n", $biografia); 
                                echo $biografia;
                            } ?></textarea>
                        <div>(<span id="cuentachar-bio">0</span>/30 caracteres)</div>
                    </div>
                    <div class="btn-form-mod">
                        <div id="modificar">
                            Modificar
                        </div>
                    </div>
                    <div class="btn-form-mod">
                        <a id="ordenar" data-target="#elegir-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin"> Ordenar

                        </a>
                    </div>
                </form>
            </div>
            </div>
            
        */ ?>
            
        <div class="nube-inter adorno">
            <img src="../wp-content/imagenes/Nubes1.png" alt="nubes">

        </div>  
    </div>
    
    
</div>
</article>
<?php endwhile; endif; ?>
</main>


<!-- edit book -->

    

<div class="btn-pasosm-wrapp">
    <div class="btn-pasosm-inn">

        <div class="btnclosem-wrapp">
            <div class="btnclosem-inn">
                <div class="btnclosem">X</div>
            </div>
        </div>

        <div class="slidem-wrapp">

            <div class="slidem-inn" id="slidem-inn-1">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="titm-wrapp">
                    <div class="titm-inn">
                        ¿Para quién es el cuento?
                    </div>
                </div>

                <div class="inpm-wrapp">
                    <div class="inpm-inn">
                        <input type="text" class="inpm" placeholder="Nombre" style="margin: 0 ">
                    </div>
                </div>

                <div class="titm-wrapp">
                    <div class="titm-inn">
                        Selecciona si es
                    </div>
                </div>

                <div class="btnsexom-wrapp">
                    <div class="btnsexom-inn">
                        <div class="btnsexom btnsexom-h sexo-el" data-sexo="h">
                            <img src="/wp-content/imagenes/nino-icon.png" class="sexoicon-mbh"
                                alt="logo de Cuenta conmigo" data-imgsexo="h">
                            <div class="sexodscr-mbh">Niño</div>
                        </div>
                        <div class="btnsexom btnsexom-m sexo-el" data-sexo="m">
                            <img src="/wp-content/imagenes/nina-icon.png" class="sexoicon-mbh"
                                alt="logo de Cuenta conmigo" data-imgsexo="m">
                            <div class="sexodscr-mbh">Niña</div>
                        </div>
                    </div>
                </div>



            </div>
            <div class="slidem-inn" id="slidem-inn-2" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="titm-wrapp">
                    <div class="titm-inn">
                        Ahora, elige el cuento de <span class="nombrem"></span>
                    </div>
                </div>

                <div class="banner-ini-wrapp biw-m">
                    <div class="banner-modal estrecho" style="height: inherit; max-width: 100%">
                        <div class="banner-ini-inner">
                            <div class="single-item2">
                                <div class="carousel-inicio-item">
                                    <div class="ciimimg-wrapp">
                                        <div class="">
                                            <div class="libro-img-wrapp" style="overflow:hidden">
                                                <img class="libro-img"
                                                    src="../wp-content/themes/generic/img_lib/595px/h/1.jpg"
                                                    style="position: relative">
                                                <div class="texto-agregar p1-cor" id="p1-1" style="top: 19%">
                                                    <div id="p1-1-1"><span
                                                            class="nombrem nombrem-coronacion"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ciim-wrapp">
                                        <div class="ciim-tit">La coronación</div>
                                        <div class="ciim-dscr">Una fantástica aventura de amistad,
                                            valentía y
                                            magia. En el día de tu coronación un feroz
                                            dragón ha raptado a tu mejor amigo: el gato
                                            Tommy. ¿Podrás rescatarlo? </div>
                                    </div>
                                </div>
                                <div class="carousel-inicio-item">
                                    <div class="ciimimg-wrapp-2">
                                        <div class="">
                                            <div class="libro-img-wrapp" style="overflow:hidden">
                                                <img class="libro-img-2"
                                                    src="../wp-content/themes/generic/img_lib_2/595px/h/libro-2-banner-creacion.png"
                                                    style="position: relative">
                                                <div class="texto-agregar" id="p1-1-adv" style="top: 21%;">
                                                    <div id="p1-1-1-adv">
                                                        de <span class="nombrem nombrem-browm"
                                                            style="rgb(79,0,0) !important"></span>
                                                        en<br>
                                                        Machu Picchu
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ciim-wrapp">
                                        <div class="ciim-tit">Aventura en Machu Picchu</div>
                                        <div class="ciim-dscr">
                                            Viaja en el tiempo a una emocionante aventura en Machu Picchu. Un terremoto se aproxima. Para salvar la ciudad, debes encontrar los animales sagrados de los incas.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="btn-crearm-wrapp">
                            <div class="btn-crearm-inn">
                                <label class="btn-crearm btn-libro-elegido"
                                    onclick="jQuery( '.btnsgtem' ).click();">
                                    ELEGIR CUENTO
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="btn-car-in prev" aria-label="Previous"><i class="arrow left"></i></div>
                    <div class="btn-car-in next" aria-label="Next"><i class="arrow right"></i></div>
                </div>
            </div>

            <div class="slidem-inn" id="slidem-inn-3" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">
                        <img class="img-pers-in capucha-nino"
                            src="../wp-content/themes/generic/img_lib/595px/h/velo-2.png"
                            style="width: 200px;">
                    </div>
                </div>
                <div class="titm-wrapp">
                    <div class="titm-inn">
                        ¡Aquí subiremos las fotos!<br>Sube una foto de <span class="nombrem"></span>
                    </div>
                </div>
                <details class="detalis-ver-consejos">
                    <summary>Ver consejos</summary>
                    <p style="text-align:center">
                        Usa una foto en alta resolución (mínimo<br>
                        300KB y máximo 2 MB). El rostro del<br>
                        protagonista debe estar totalmente de frente.<br>
                        Asegúrate que la foto tenga buena<br>
                        iluminación y no esté borrosa.
                        <img class="consejo-img-nino"
                            src="../wp-content/themes/generic/img/tips-image.png">
                    </p>
                </details>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <label class="btn-crearm" for="upload_image">
                            SUBIR FOTO
                        </label>
                        <input style="display: none" type="file" name="upload_image"
                            id="upload_image" />
                    </div>
                </div>

            </div>

            <div class="slidem-inn" id="slidem-inn-4" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">

                        <div class="row">
                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                <div class="descr-ajustar-img dscr-r-red">
                                    Rotar izq./der.
                                </div>
                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo">-90°</div>
                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo">+90°</div>
                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo"
                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                    aria-valuenow="50">
                            </div>
                            <div class="col-xs-12 text-center">
                                <div class="descr-ajustar-img dscr-a-red">
                                    Reducir/ Ampliar
                                </div>
                                <div id="image_demo" style="width:100%; margin-top:80px">
                                    <div class="img-cropp-imgdel imgcrp-nino"><img class="capucha-nino"
                                            src="../wp-content/themes/generic/img_lib/595px/h/velo-2.png">
                                    </div>

                                </div>
                            </div>
                            <div class="titm-inn" style="font-weight:bold;">
                                Encaja la foto de <span class="nombrem"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <a href="#" class="btn-crearm crop_image_inicio" data-id="0" for="upload_image">
                            SIGUIENTE FOTO
                        </a>
                        <a class="btn-outline-naranja" href="#"
                            onclick="jQuery( '.btnantem' ).click();">
                            CAMBIAR FOTO
                        </a>
                    </div>
                </div>

            </div>

            <div class="slidem-inn" id="slidem-inn-5" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">
                        <img class="img-pers-in"
                            src="../wp-content/themes/generic/img_lib/595px/veloreina-2.png"
                            id="capucha-madre-pre">
                    </div>
                </div>
                <div class="titm-wrapp">
                    <div class="titm-inn">
                        Sube una foto de mamá (opcional)
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <label class="btn-crearm" for="upload_image4">
                            SUBIR FOTO
                        </label>
                        <input style="display: none" type="file" name="upload_image4"
                            id="upload_image4" />
                        <a class="btn-outline-naranja" href="#"
                            onclick="jQuery( '.btnsgtem' ).click();jQuery( '.btnsgtem' ).click();">
                            SALTAR PASO
                        </a>
                    </div>
                </div>

            </div>

            <div class="slidem-inn" id="slidem-inn-6" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">

                        <div class="row">
                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                <div class="descr-ajustar-img dscr-r-red">
                                    Rotar izq./der.
                                </div>
                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo4">-90°</div>
                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo4">+90°</div>
                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo4"
                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                    aria-valuenow="50">
                            </div>
                            <div class="col-xs-12 text-center">
                                <div class="descr-ajustar-img dscr-a-red">
                                    Reducir/ Ampliar
                                </div>
                                <div id="image_demo4"
                                    style="width:100%; margin-top:20px;position: relative">
                                    <div class="img-cropp-imgdel imgcrp-madre"><img
                                            src="../wp-content/themes/generic/img_lib/595px/veloreina-2.png">
                                    </div>

                                </div>
                            </div>
                            <div class="titm-inn" style="font-weight:bold;">
                                Encaja la foto de mamá
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <a href="#" class="btn-crearm crop_image_inicio" data-id="3">
                            SIGUIENTE FOTO
                        </a>
                        <a class="btn-outline-naranja" href="#"
                            onclick="jQuery( '.btnantem' ).click();">
                            CAMBIAR FOTO
                        </a>
                    </div>
                </div>

            </div>

            <div class="slidem-inn" id="slidem-inn-7" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">
                        <img class="img-pers-in"
                            src="../wp-content/themes/generic/img_lib/595px/velorey-2.png"
                            id="capucha-padre-pre">
                    </div>
                </div>
                <div class="titm-wrapp">
                    <div class="titm-inn">
                        Sube una foto de papá (opcional)
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <label class="btn-crearm" for="upload_image5">
                            SUBIR FOTO
                        </label>
                        <input style="display: none" type="file" name="upload_image5"
                            id="upload_image5" />
                        <a class="btn-outline-naranja" href="#"
                            onclick="jQuery( '.btnsgtem' ).click();jQuery( '.btnsgtem' ).click();">
                            SALTAR PASO
                        </a>
                    </div>
                </div>

            </div>

            <div class="slidem-inn" id="slidem-inn-8" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">

                        <div class="row">
                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                <div class="descr-ajustar-img dscr-r-red">
                                    Rotar izq./der.
                                </div>
                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo5">-90°</div>
                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo5">+90°</div>
                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo5"
                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                    aria-valuenow="50">
                            </div>
                            <div class="col-xs-12 text-center">
                                <div class="descr-ajustar-img dscr-a-red">
                                    Reducir/ Ampliar
                                </div>
                                <div id="image_demo5" style="width:100%; position: relative">
                                    <div class="img-cropp-imgdel imgcrp-padre"><img
                                            src="../wp-content/themes/generic/img_lib/595px/velorey-2.png">
                                    </div>

                                </div>
                            </div>
                            <div class="titm-inn" style="font-weight:bold;">
                                Encaja la foto de papá
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <a href="#" class="btn-crearm crop_image_inicio" data-id="4">
                            SIGUIENTE FOTO
                        </a>
                        <a class="btn-outline-naranja" href="#"
                            onclick="jQuery( '.btnantem' ).click();">
                            CAMBIAR FOTO
                        </a>
                    </div>
                </div>
            </div>

            <div class="slidem-inn" id="slidem-inn-9" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">
                        <div class="cuadro-imgsubir" id="retrato-familiar-pre"></div>
                    </div>
                </div>
                <div class="titm-wrapp">
                    <div class="titm-inn">
                        Sube una foto familiar (opcional)
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <label class="btn-crearm" for="upload_image2">
                            SUBIR FOTO
                        </label>
                        <input style="display: none" type="file" name="upload_image2"
                            id="upload_image2" />
                        <a class="btn-outline-naranja" href="#"
                            onclick="jQuery( '.btnsgtem' ).click();jQuery( '.btnsgtem' ).click();">
                            SALTAR PASO
                        </a>
                    </div>
                </div>

            </div>

            <div class="slidem-inn" id="slidem-inn-10" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">

                        <div class="row">
                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                <div class="descr-ajustar-img dscr-r-red">
                                    Rotar izq./der.
                                </div>
                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo2">-90°</div>
                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo2">+90°</div>
                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo2"
                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                    aria-valuenow="50">
                            </div>
                            <div class="col-xs-12 text-center cropp-wrapp-rosa">
                                <div class="descr-ajustar-img dscr-a-red">
                                    Reducir/ Ampliar
                                </div>
                                <div id="image_demo2"
                                    style="width:100%; margin-top:20px;position: relative">

                                </div>
                            </div>
                            <div class="titm-inn" style="font-weight:bold;">
                                Encaja la foto familiar
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <a href="#" class="btn-crearm crop_image_inicio" data-id="1">
                            SIGUIENTE FOTO
                        </a>
                        <a class="btn-outline-naranja" href="#"
                            onclick="jQuery( '.btnantem' ).click();">
                            CAMBIAR FOTO
                        </a>
                    </div>
                </div>

            </div>

            <div class="slidem-inn" id="slidem-inn-11" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">
                        <div class="cuadro-imgsubir" id="retrato-nino-pre"></div>
                    </div>
                </div>
                <div class="titm-wrapp">
                    <div class="titm-inn">
                        Sube un retrato de <span class="nombrem"></span> (opcional)
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <label class="btn-crearm" for="upload_image3">
                            SUBIR FOTO
                        </label>
                        <input style="display: none" type="file" name="upload_image3"
                            id="upload_image3" />
                        <a class="btn-outline-naranja" href="#"
                            onclick="jQuery( '.btnsgtem' ).click();jQuery( '.btnsgtem' ).click();">
                            SALTAR PASO
                        </a>
                    </div>
                </div>

            </div>

            <div class="slidem-inn" id="slidem-inn-12" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">

                        <div class="row">
                            <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                                <div class="descr-ajustar-img dscr-r-red">
                                    Rotar izq./der.
                                </div>
                                <div class="rotate-90 neg-90" data-rotation="-90" data-imgcrop="image_demo3">-90°</div>
                                <div class="rotate-90 pos-90" data-rotation="90" data-imgcrop="image_demo3">+90°</div>
                                <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo3"
                                    type="range" step="10" aria-label="zoom" min="0" max="100"
                                    aria-valuenow="50">
                            </div>
                            <div class="col-xs-12 text-center cropp-wrapp-rosa">
                                <div class="descr-ajustar-img dscr-a-red">
                                    Reducir/ Ampliar
                                </div>
                                <div id="image_demo3"
                                    style="width:100%; margin-top:20px;position: relative">

                                </div>
                            </div>
                            <div class="titm-inn" style="font-weight:bold;">
                                Encaja el retrato de <span class="nombrem"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <a href="#" class="btn-crearm crop_image_inicio" data-id="2">
                            SIGUIENTE PASO
                        </a>
                        <a class="btn-outline-naranja" href="#"
                            onclick="jQuery( '.btnantem' ).click();">
                            CAMBIAR FOTO
                        </a>
                    </div>
                </div>

            </div>

            <div class="slidem-inn" id="slidem-inn-13" style="display: none">
                <div class="estrellasm-wrapp">
                    <div class="estrellasm-inn">
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">1</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">2</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">3</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">4</div>
                        </div>
                        <div class="estrellasm">
                            <div class="star-five star-five-active"></div>
                            <div class="star-number">5</div>
                        </div>
                    </div>
                </div>
                <div class="titm-wrapp">
                    <div class="titm-inn">
                        Finalmente, personaliza la dedicatoria y otros detalles (opcional)
                    </div>
                </div>

                <div class="img-pers-wrapp">
                    <div class="img-pers-inn">

                        <div class="row">
                            <form action="" class="form-info-libroinic">
                                <div class="campo">
                                    <p>Dedicatoria:</p>
                                    <textarea type="text" id="dedicatoria-libro" rows="3"
                                        class="inputs-texto" data-cuenta="cuentachar-ded"
                                        data-max="140">Querido @niño,
¡La verdadera magia para hacer realidad todos tus sueños está en tu interior!</textarea>
                                    <div>(<span id="cuentachar-ded">0</span>/140 caracteres)</div>
                                </div>


                                <details class="detalis-ver-consejos campo">
                                    <summary>Detalles de la historia</summary>
                                    <div>





                                <div class="form-group input-group date campo"
                                    id="fecha-formcont-wrapp">
                                    <p>Cumpleaños del niño:</p>
                                    <input type="text" class="" id="fecha-formcont"
                                        onchange="verificarfecha('fecha-formcont', 'fecha-formcont-wrapp')"
                                        value="">
                                    <span class="input-group-addon" id="btn-dp-1"
                                        style="visibility: hidden;position: relative;left:5px;top: -78px;display: inline-block;">
                                    </span>
                                </div>
                                <div class="campo">
                                    <p>Nombre del padre:</p>
                                    <input type="text" id="nombre-padre-libro" value="">
                                </div>
                                <div class="campo">
                                    <p>Nombre de la madre:</p>
                                    <input type="text" id="nombre-madre-libro" value="">
                                </div>
                                <div class="campo">
                                    <p>Gato mascota:</p>
                                    <select class="form-control campo-select"
                                        id="sexmasc-familiar-libro">
                                        <option value="1">Gatito</option>
                                        <option value="2">Gatita</option>
                                    </select>
                                    <input type="text" id="gato-madre-libro" value="">
                                </div>
                                <div class="campo">
                                    <p>Familiar más cercana:</p>
                                    <select class="form-control campo-select" id="tipo-familiar-libro">
                                        <option value="1">Tía</option>
                                        <option value="2">Abuelita</option>
                                        <option value="3">Prima</option>
                                    </select>
                                    <input type="text" id="nombre-familiar-libro" value="Nichola">
                                </div>
                                <div class="campo">
                                    <p>Biografía:</p>
                                    <p class="form-subcampo">Ciudad de nacimiento:</p>
                                    <input type="text" id="ciudad-nacimiento-libro" value="">
                                    <p class="form-subcampo">Canción favorita:</p>
                                    <input style="text-transform:lowercase" type="text"
                                        id="cancion-favorita-libro" value="">
                                    <p class="form-subcampo">Comida favorita:</p>
                                    <input style="text-transform:lowercase" type="text"
                                        id="comida-favorita-libro" value="">
                                    <p class="form-subcampo">Juguete favorito:</p>
                                    <input style="text-transform:lowercase" type="text"
                                        id="juguete-favorito-libro" value="">
                                    <p class="form-subcampo">Actividad favorita:</p>
                                    <input style="text-transform:lowercase" type="text"
                                        id="actividad-favorita-libro" value="">
                                    <p class="form-subcampo">Adicional a biografía:</p>
                                    <textarea type="text" id="biografia-libro" rows="2"
                                        class="inputs-texto" data-cuenta="cuentachar-bio"
                                        data-max="30"></textarea>
                                    <div>(<span id="cuentachar-bio">0</span>/30 caracteres)</div>
                                </div>



                                    </div>
                                </details>
                            </form>


                        </div>
                    </div>
                </div>
                <div class="btn-crearm-wrapp">
                    <div class="btn-crearm-inn">
                        <a href="#" class="btn-crearm" id="ver-cuento">
                            VER CUENTO
                        </a>
                    </div>
                </div>

            </div>

        </div>
        <div class="btnsgtem-wrapp">
            <div class="btnsgtem-inn" data-current="1">
                <div class="btnmovem btnsgtem" data-current="1" data-next="2">Siguiente &#10230;</div>
                <div class="btnmovem btnantem" data-current="2" data-next="1">&#10229; Volver al paso
                    anterior</div>
            </div>
        </div>



    </div>
</div>

<!-- edit book -->


<?php get_footer(); ?>

<div id="subir-foto-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title">Subir foto</h4>
            </div>
            <div class="modal-body">
                <div class="subir-foto-wrapp">
                    <div class="btn-group btn-group-vertical btn-group-subfoto">
                        <div class="btn-group-subfotoinn">
                            <div class="btn-group-subfotoimg">
                                <img src="../wp-content/themes/generic/img/rostro-nino.png">
                            </div>
                            <div class="btn-group-subfotodscr">
                                <label class="btn btn-sub-fotomd" for="upload_image">Rostro de niñ<?php echo ($sexo == "h" ? "o" : "a"); ?></label>
                            </div>

                        </div>
                        <div class="btn-group-subfotoinn">
                            <div class="btn-group-subfotoimg">
                                <img src="../wp-content/themes/generic/img/retrato-familia.jpg">
                            </div>
                            <div class="btn-group-subfotodscr">
                                <label class="btn btn-sub-fotomd" for="upload_image2">Retrato familiar</label>
                            </div>

                        </div>
                        <div class="btn-group-subfotoinn">
                            <div class="btn-group-subfotoimg">
                                <img src="../wp-content/themes/generic/img/retrato-nino.png">
                            </div>
                            <div class="btn-group-subfotodscr">
                                <label class="btn btn-sub-fotomd" for="upload_image3">Retrato de niñ<?php echo ($sexo == "h" ? "o" : "a"); ?></label>
                            </div>

                        </div>
                        <div class="btn-group-subfotoinn">
                            <div class="btn-group-subfotoimg">
                                <img src="../wp-content/themes/generic/img/rostro-mama.png">
                            </div>
                            <div class="btn-group-subfotodscr">
                                <label class="btn btn-sub-fotomd" for="upload_image4">Rostro de mamá</label>
                            </div>

                        </div>
                        <div class="btn-group-subfotoinn">
                            <div class="btn-group-subfotoimg">
                                <img src="../wp-content/themes/generic/img/rostro-papa.png">
                            </div>
                            <div class="btn-group-subfotodscr">
                                <label class="btn btn-sub-fotomd" for="upload_image5">Rostro de papá</label>
                            </div>

                        </div>
                    </div>
                    <div style="display:none" data-dscr="Grupo de botones para subir las fotos">
                        <input type="file" name="upload_image" id="upload_image" />
                        <input type="file" name="upload_image2" id="upload_image2" />
                        <input type="file" name="upload_image3" id="upload_image3" />
                        <input type="file" name="upload_image4" id="upload_image4" />
                        <input type="file" name="upload_image5" id="upload_image5" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<style>
    #subir-foto-mod .modal-content{
        border-radius: 25px;
        border: 4px solid rgba(68, 216, 190, 1);
    }
    #subir-foto-mod .modal-header .close-modal{
        height: 30px;
        width: 30px;
        padding: 0 0 7px 0;
        background-color: rgb(68, 216, 190);
        border-radius: 100%;
        color: #fff;
        font-size: 35px;
        opacity: 1;
        line-height: 21px;
    }
    .btn-group-subfoto{
        display: inline-flex;
        width: 100%;
    }
    .btn-group-subfotoinn{
        width: calc(100% / 5);
        padding: 15px;
    }
    .btn-group-subfotoimg{
        height: 85px;
    }
    .btn-group-subfotodscr{
        
    }
    .btn-group-subfotodscr label{
        white-space: unset;
        padding: 6px 5px;
    }
    .btn-group-subfotoinn:nth-child(1) label{
        background-color: rgb(170,15,177);
        border: 1px solid rgb(170,15,177)!important;
    }
    .btn-group-subfotoinn:nth-child(2) label{
        background-color: rgb(68,216,190);
        border: 1px solid rgb(68,216,190)!important;
    }
    .btn-group-subfotoinn:nth-child(3) label{
        background-color: rgb(255,151,2);
        border: 1px solid rgb(255,151,2)!important;
    }
    .btn-group-subfotoinn:nth-child(4) label{
        background-color: rgb(255,100,0);
        border: 1px solid rgb(255,100,0)!important;
    }
    .btn-group-subfotoinn:nth-child(5) label{
        background-color: rgb(253,206,14);
        border: 1px solid rgb(253,206,14)!important;
    }
    .link-tipo-tapa{
        cursor: pointer;
    }
</style>
<div id="uploadimageModal" class="modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">ROSTRO DE NIÑ<?php echo ($sexo == "h" ? "O" : "A"); ?></h4>
      		</div>
      		<div class="modal-body">
        		<div class="row">
                    <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo" type="range" step="10" aria-label="zoom" min="0" max="100" aria-valuenow="50">
                    </div>
  					<div class="col-xs-12 text-center">
                        <div id="image_demo" style="width:100%; margin-top:80px">
                            <div class="img-cropp-imgdel imgcrp-nin<?php echo ($sexo == "h" ? "o" : "a"); ?>"><img src="../wp-content/themes/generic/img_lib/595px/<?php echo $sexo; ?>/velo-2.png" ></div>
                        
                        </div>
  					</div>
  					<div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <button class="btn crop_image" data-id="0">Subir imagen</button>
					</div>
				</div>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
    </div>
</div>

<div id="uploadimageModal2" class="modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">RETRATO FAMILIAR</h4>
      		</div>
      		<div class="modal-body">
        		<div class="row">
                    <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo2" type="range" step="10" aria-label="zoom" min="0" max="100" aria-valuenow="50">
                    </div>
  					<div class="col-xs-12 text-center cropp-wrapp-rosa">
                        <div id="image_demo2" style="width:100%; margin-top:30px">                        
                        
                        </div>
  					</div>
  					<div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <button class="btn crop_image" data-id="1">Subir imagen</button>
					</div>
				</div>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
    </div>
</div>

<div id="uploadimageModal3" class="modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">RETRATO DE NIÑ<?php echo ($sexo == "h" ? "O" : "A"); ?></h4>
      		</div>
      		<div class="modal-body">
        		<div class="row">
                    <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo3" type="range" step="10" aria-label="zoom" min="0" max="100" aria-valuenow="50">
                    </div>
  					<div class="col-xs-12 text-center cropp-wrapp-rosa">
                        <div id="image_demo3" style="width:100%; margin-top:30px">                        
                        
                        </div>
  					</div>
  					<div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <button class="btn crop_image" data-id="2">Subir imagen</button>
					</div>
				</div>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
    </div>
</div>
<div id="uploadimageModal4" class="modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">ROSTRO DE MADRE</h4>
      		</div>
      		<div class="modal-body">
        		<div class="row">
                    <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo4" type="range" step="10" aria-label="zoom" min="0" max="100" aria-valuenow="50">
                    </div>
  					<div class="col-xs-12 text-center">
                        <div id="image_demo4" style="width:100%; margin-top:20px;position:relative;">
                            <div class="img-cropp-imgdel imgcrp-madre"><img src="../wp-content/themes/generic/img_lib/595px/veloreina-2.png" ></div>
                        
                        </div>
  					</div>
  					<div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <button class="btn crop_image" data-id="3">Subir imagen</button>
					</div>
				</div>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
    </div>
</div>

<div id="uploadimageModal5" class="modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">ROSTRO DE PADRE</h4>
      		</div>
      		<div class="modal-body">
        		<div class="row">
                    <div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <input class="range-rotate" data-rot-ant="0" data-imgcrop="image_demo5" type="range" step="10" aria-label="zoom" min="0" max="100" aria-valuenow="50">
                    </div>
  					<div class="col-xs-12 text-center">
                        <div id="image_demo5" style="width:100%; margin-top:80px">
                            <div class="img-cropp-imgdel imgcrp-padre"><img src="../wp-content/themes/generic/img_lib/595px/velorey-2.png" ></div>
                        
                        </div>
  					</div>
  					<div class="col-md-12 wrapp-btn-mod" style="padding-top:30px;">
                        <button class="btn crop_image" data-id="4">Subir imagen</button>
					</div>
				</div>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
    </div>
</div>


<script src="../wp-content/themes/generic/js/croppie.js"></script>
<script src="../wp-content/themes/generic/js/princ.js"></script>
<script src="../wp-content/themes/generic/js/moment-with-locales.js"></script>
<script src="../wp-content/themes/generic/js/bootstrap-datetimepicker.js"></script>
<script>
var sexo_libro = "<?php echo $_GET['sexo'];  ?>";
var libro_sel = 1;
var nombre_nino = "<?php echo $_GET['nombre'];  ?>";

    
    
var estado_carga = 0;
var num_msg_carga = 0;
var pagina_actual=1;    
var paginatotal = $( ".libro-pagina" ).length;
    
var mensaje_subir_error="";
    
console.log(paginatotal);
console.log(location.search);
var img_1= <?php echo (isset($_GET['img_0'])? $_GET['img_0'] : 0);  ?>;
console.log(img_1);
var img_2= <?php echo (isset($_GET['img_1'])? $_GET['img_1'] : 0);  ?>;
console.log(img_2);
var img_3= <?php echo (isset($_GET['img_2'])? $_GET['img_2'] : 0);  ?>;
console.log(img_3);
var img_4= <?php echo (isset($_GET['img_3'])? $_GET['img_3'] : 0);  ?>;
console.log(img_4);
var img_5= <?php echo (isset($_GET['img_4'])? $_GET['img_4'] : 0);  ?>;
console.log(img_5);
var imgs_arr = [img_1, img_2, img_3, img_4, img_5];
console.log(imgs_arr);




    
    
/*HOME PAGE EDIT FUNCTIONS*/
jQuery(".btn-crearm-empieza").on("click", function() {
    jQuery(".btn-pasosm-wrapp").css("display", "flex");
    jQuery("body").css("overflow", "hidden");
});
jQuery(".btnclosem").on("click", function() {
    jQuery(".btn-pasosm-wrapp").css("display", "none");
    jQuery("body").css("overflow", "inherit");
});
/*Función que se ejecuta cuando se presion en siguiente,
  en los pasos para crear un nuevo cuento
*/
jQuery(".btnsgtem").on("click", function() {
    console.log("Presionó");

    let current_slide = parseInt(jQuery(".btnsgtem-inn").attr('data-current'));
    let next_slide = current_slide + 1;
    console.log(next_slide);

    if (current_slide == 8 && libro_sel == 2) {
        jQuery("#slidem-inn-8").hide();
        current_slide = 10;
        next_slide = 11;
    }

    if (current_slide == 1) {
        let nombre = jQuery(".inpm").val();
        if (!(sexo_libro && nombre)) {
            return alert("Por favor ingrese un nombre y seleccione un sexo");
        }

        nombre_nino = nombre;
        jQuery(".nombrem").html(nombre);
    }

    console.log("mostrar: " + next_slide);
    console.log("esconder: " + current_slide);

    jQuery("#slidem-inn-" + current_slide).hide();
    jQuery("#slidem-inn-" + next_slide).show();

    if (current_slide == 1) {
        console.log("slider");

        if (!jQuery(".single-item2").hasClass("slick-initialized")) {
            jQuery('.single-item2').slick({
                infinite: false,
                autoplay: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                prevArrow: jQuery('.slidem-wrapp .prev'),
                nextArrow: jQuery('.slidem-wrapp .next'),
            });

            jQuery('.single-item2').on('afterChange', function() {
                var dataId = jQuery('.single-item2 .slick-current').attr("data-slick-index");

                libro_sel = ++dataId;
            });
            mostrarDatosLibro(libro_sel, sexo_libro, nombre_nino);
        }

        /*
        $image_crop3 = croppieImagenRetrato(libro_sel);
        $image_crop5 = croppieImagenPapa(libro_sel);

        mostrarDatosLibro(libro_sel, sexo_libro, nombre_nino);
        */
        console.log(libro_sel);
    }


    jQuery(".btnantem").show();

    jQuery(".btnsgtem-inn").attr('data-current', next_slide);



});

    
/*Función que se ejecuta cuando se presiona en anterior,
  en los pasos para crear un nuevo cuento
*/
jQuery(".btn-libro-elegido").on("click", function() {

    $image_crop3 = croppieImagenRetrato(libro_sel);
    $image_crop5 = croppieImagenPapa(libro_sel);

    mostrarDatosLibro(libro_sel, sexo_libro, nombre_nino);
});
jQuery(".btnantem").on("click", function() {
    console.log("Presionó");

    let current_slide = parseInt(jQuery(".btnsgtem-inn").attr('data-current'));
    let next_slide = current_slide - 1;

    if (next_slide == 1) {
        jQuery(".btnantem").hide();
    } else {
        jQuery(".btnantem").show();
    }

    console.log("mostrar: " + next_slide);
    console.log("esconder: " + current_slide);

    jQuery("#slidem-inn-" + current_slide).hide();
    jQuery("#slidem-inn-" + next_slide).show();

    jQuery(".btnsgtem-inn").attr('data-current', next_slide);
});
jQuery(".sexo-el").on("click", function() {
    jQuery(".sexo-el").css("color", "#fff");
    jQuery('.sexoicon-mbh').css("filter", "inherit");
    var sexo = jQuery(this).attr("data-sexo");
    //variable global
    sexo_libro = sexo;

    jQuery(".lnk-libro").attr("data-sexo", sexo);
    jQuery('[data-sexo="' + sexo + '"]').css("color", "rgb(0, 0, 0)");
    jQuery('[data-imgsexo="' + sexo + '"]').css("filter", "invert(1)");

    let src_capucha = '/wp-content/themes/generic/img_lib/595px/' + sexo + '/velo-2.png';
    let url_caratula = '/wp-content/themes/generic/img_lib/595px/'+sexo+'/1.jpg';
    let url_caratula_2 = '/wp-content/themes/generic/img_lib_2/595px/'+sexo+'/libro-2-banner-creacion.png';

    let nombre = jQuery(".inpm").val();

    jQuery('.capucha-nino').attr("src", src_capucha);
    jQuery('.btn-pasosm-wrapp .libro-img').attr("src",url_caratula);
    jQuery('.btn-pasosm-wrapp .libro-img-2').attr("src",url_caratula_2);

    if (sexo == 'm') {
        jQuery('.capucha-nino').closest('.imgcrp-nino').css({
            "top": "-30px",
            "height": "330px",
        });
        jQuery('.capucha-nino').closest('#image_demo').css("margin-top", "25px");
        jQuery('.libro-img-wrapp .nombrem-coronacion').css("color", "#653f88");
    }

});
jQuery(".lnk-libro").on("click", function() {

    var sexo = jQuery(this).attr("data-sexo");
    var nombre = jQuery("#input-crea-libro").val();
    var len_nombre = nombre.length;
    var regex = new RegExp(/^[a-zA-ZñÑ\s]+$/);
    var libro = parseInt(jQuery(".slick-active .carousel-inicio-item").attr("data-libro"));
    if(libro == 1){
       libro = "book";
    console.log("Libro seleccionado: "+libro);
    }else if(libro == 2){
       libro = "book-2";
    }
    

    if (len_nombre == 0) {
        alert("Debe ingresar un nombre");
    } else if (!regex.test(nombre)) {
        alert("Debe ingresar sólo letras.");
    } else if (len_nombre < 3) {
        alert(
            "El nombre es muy corto. Por favor contáctate con nosotros para poder brindarte una opción especial."
        );
    } else if (len_nombre > 10) {
        alert(
            "El nombre es muy largo. Por favor contáctate con nosotros para poder brindarte una opción especial."
        );
    } else if (!sexo) {
        alert("Debe seleccionar un sexo");
    } else {
        var link_href = "/"+libro+"/?nombre=" + nombre + "&sexo=" + sexo + "&tkn=0";
        window.location.href = link_href;
    }
});

    


/*SUBIR IMAGENES MODALES*/

$image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 200,
        type: 'square' //circle
    },
    boundary: {
        width: 300,
        height: 300
    },
    enableOrientation: true
});
$image_crop2 = $('#image_demo2').croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 270,
        type: 'square'
    },
    boundary: {
        width: 260,
        height: 330
    },
    enableOrientation: true
});
/*
DEPENDE DEL LIBRO, SE SELECCIONA EN croppieImagenRetrato(libro)
$image_crop3 = $('#image_demo3').croppie({
    enableExif: true,
    viewport: {
        width:205,
        height:295,
        type:'square'
    },
    boundary:{
        width:260,
        height:330
    },
    enableOrientation: true
});
*/
$image_crop4 = $('#image_demo4').croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 200,
        type: 'square'
    },
    boundary: {
        width: 300,
        height: 300
    },
    enableOrientation: true
});
/*
DEPENDE DEL LIBRO, SE SELECCIONA EN croppieImagenPapa(libro)
$image_crop5 = $('#image_demo5').croppie({
    enableExif: true,
    viewport: {
        width:200,
        height:200,
        type:'square'
    },
    boundary:{
        width:300,
        height:300
    },
    enableOrientation: true
});
*/
$('#upload_image').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
$('#upload_image2').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop2.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
$('#upload_image3').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop3.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
$('#upload_image4').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop4.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
$('#upload_image5').on('change', function() {
    var reader = new FileReader();
    reader.onload = function(event) {
        $image_crop5.croppie('bind', {
            url: event.target.result,
            orientation: 1
        }).then(function() {
            console.log('jQuery bind complete');
        });
    }
    reader.readAsDataURL(this.files[0]);
    jQuery(".btnsgtem").click();
    $(this).val('');
});
    

$('.crop_image_inicio').click(function(event) {
    $(this).css({
        "opacity": "0.5",
        "pointer-events": "none"
    });
    var num_img = parseInt($(this).attr("data-id"));
    var width_sz;
    var height_sz;
    var image_crop;
    switch (num_img) {
        case 0:
            image_crop = $image_crop;
            width_sz = 200;
            height_sz = 200;
            break;
        case 1:
            image_crop = $image_crop2;
            width_sz = 400;
            height_sz = 540;
            break;
        case 2:
            image_crop = $image_crop3;
            if (libro_sel == 1) {
                width_sz = 410;
                height_sz = 590;
            } else if (libro_sel == 2) {
                width_sz = 590;
                height_sz = 410;
            }
            break;
        case 3:
            image_crop = $image_crop4;
            width_sz = 200;
            height_sz = 200;
            break;
        case 4:
            image_crop = $image_crop5;
            if (libro_sel == 1) {
                width_sz = 200;
                height_sz = 200;
            } else if (libro_sel == 2) {
                width_sz = 150;
                height_sz = 200;
            }
            break;
        default:
            image_crop = $image_crop;
            break;
    }
    image_crop.croppie('result', {
        type: 'canvas',
        size: {
            width: width_sz,
            height: height_sz
        }
    }).then(function(response) {
        console.log("response: " + response);
        $.ajax({
            url: "/wp-content/themes/generic/upload.php",
            type: "POST",
            data: {
                "image": response
            },
            success: function(data) {
                $(".crop_image_inicio").css({
                    "opacity": "1",
                    "pointer-events": "inherit"
                });

                imgs_arr[num_img] = data;
                jQuery(".btnsgtem").click();

                console.log(imgs_arr);
            }
        });
    })
});


$('#ver-cuento').click(function(event) {
    var link_red = "";


    let nombre_nino = jQuery(".inpm").val();
    let sexo = sexo_libro;


    var fecha_nino = $("#fecha-formcont").val();
    var nombre_padre = $("#nombre-padre-libro").val();
    var nombre_madre = $("#nombre-madre-libro").val();
    var sexo_mascota = $("#sexmasc-familiar-libro").val();
    var nombre_mascota = $("#gato-madre-libro").val();
    var tipo_familiar = $("#tipo-familiar-libro").val();
    var nombre_familiar = $("#nombre-familiar-libro").val();

    var dedicatoria = $("#dedicatoria-libro").val();
    dedicatoria = dedicatoria.replace(/\r?\n/g, '<br />');

    //BIOGRAFIA();
    var ciudad_nacimiento = $("#ciudad-nacimiento-libro").val();
    var cancion_favorita = $("#cancion-favorita-libro").val();
    var comida_favorita = $("#comida-favorita-libro").val();
    var juguete_favorito = $("#juguete-favorito-libro").val();
    var actividad_favorita = $("#actividad-favorita-libro").val();
    var biografia = $("#biografia-libro").val();
    biografia = biografia.replace(/\r?\n/g, '<br />');

    if (verificarnombre(nombre_nino, "nombre del niño") && verificarnombre(nombre_padre, "nombre del padre") &&
        verificarnombre(nombre_madre, "nombre de la madre") && verificarnombre(nombre_mascota,
            "nombre de la mascota") && verificarnombre(nombre_familiar, "nombre de la familiar") &&
        verificardedicatoria(dedicatoria, "dedicatoria") && verificarbiografia(ciudad_nacimiento,
            "ciudad de nacimiento") && verificarbiografia(cancion_favorita, "canción favorita") &&
        verificarbiografia(comida_favorita, "comida favorita") && verificarbiografia(juguete_favorito,
            "juguete favorito") && verificarbiografia(actividad_favorita, "actividad favorita") &&
        verificarbiografia(biografia, "biografía")) {

        link_red += ("&nombre=" + encodeURIComponent(nombre_nino));
        link_red += ("&sexo=" + sexo);

        if (fecha_nino) {
            link_red += ("&fec_nin=" + encodeURIComponent(fecha_nino));
        }
        if (nombre_padre) {
            link_red += ("&nbr_pap=" + encodeURIComponent(nombre_padre));
        }
        if (nombre_madre) {
            link_red += ("&nbr_mad=" + encodeURIComponent(nombre_madre));
        }
        if (sexo_mascota) {
            link_red += ("&sex_gat=" + encodeURIComponent(sexo_mascota));
        }
        if (nombre_mascota) {
            link_red += ("&nbr_gat=" + encodeURIComponent(nombre_mascota));
        }
        if (tipo_familiar) {
            link_red += ("&tip_fam=" + encodeURIComponent(tipo_familiar));
        }
        if (nombre_familiar) {
            link_red += ("&nbr_fam=" + encodeURIComponent(nombre_familiar));
        }
        if (dedicatoria) {
            link_red += ("&ded=" + encodeURIComponent(dedicatoria));
        }
        if (ciudad_nacimiento) {
            link_red += ("&ciu_nac=" + encodeURIComponent(ciudad_nacimiento));
        }
        if (cancion_favorita) {
            link_red += ("&can_fav=" + encodeURIComponent(cancion_favorita));
        }
        if (comida_favorita) {
            link_red += ("&com_fav=" + encodeURIComponent(comida_favorita));
        }
        if (juguete_favorito) {
            link_red += ("&jug_fav=" + encodeURIComponent(juguete_favorito));
        }
        if (actividad_favorita) {
            link_red += ("&act_fav=" + encodeURIComponent(actividad_favorita));
        }
        if (biografia) {
            link_red += ("&bio=" + encodeURIComponent(biografia));
        }

        for (var i = 0; i < imgs_arr.length; i++) {
            if (imgs_arr[i] > 0) {
                link_red += ("&img_" + i + "=" + encodeURIComponent(imgs_arr[i]));
            }
        }

        let libro_nombre = "libro";
        if (libro_sel == 2) {
            libro_nombre = "libro-2"
        }

        window.location.href = "/" + libro_nombre + "/index.php?" + link_red;

    } else {
        alert(mensaje_subir_error);
    }



});

    
/*HOME PAGE EDIT FUNCTIONS*/
    
    
    
    
function ingresarMensaje () {


    if(estado_carga == 1){ 
        
    } else {
        
        var msg_carga = "";
        switch (num_msg_carga) {
            case 0:
                msg_carga = "El libro está cargando";
                break;
            case 1:
                msg_carga = "Crear algo bonito toma tiempo :)";
                break;
            case 2:
                msg_carga = "Parece que la conexión anda algo lenta.";
                break;
            case 3:
                msg_carga = "Por cierto, ¡tu libro está quedando genial!";
                break;
            case 4:
                msg_carga = "Ya casi está listo.";
                break;
            default:
                msg_carga = "El libro está cargando";
                break;
        }
        $(".libro-estado-txt").html(msg_carga);
        console.log(estado_carga);
        if(num_msg_carga < 4){
            num_msg_carga++;
        }else{
           num_msg_carga =0;
        }
        setTimeout(ingresarMensaje, 8000); //wait 50 ms, then try again
    }
}
    
function setdedicatoria(variation, tapa, tamano){
    
    var nombre_nino = "<?php echo $nombre; ?>";
    var sexo = "<?php echo $sexo; ?>";
    var fecha_nino = $("#fecha-formcont").val();
    var nombre_padre = $("#nombre-padre-libro").val();
    var nombre_madre = $("#nombre-madre-libro").val();
    var sexo_mascota = $("#sexmasc-familiar-libro").val();
    var nombre_mascota = $("#gato-madre-libro").val();
    var tipo_familiar = $("#tipo-familiar-libro").val();
    var nombre_familiar = $("#nombre-familiar-libro").val();
      
    var dedicatoria = $("#dedicatoria-libro").val();
    dedicatoria = dedicatoria.replace(/\r?\n/g, '<br />');

    //BIOGRAFIA();
    var ciudad_nacimiento = $("#ciudad-nacimiento-libro").val();
    var cancion_favorita = $("#cancion-favorita-libro").val();
    var comida_favorita = $("#comida-favorita-libro").val();
    var juguete_favorito = $("#juguete-favorito-libro").val();
    var actividad_favorita = $("#actividad-favorita-libro").val();
    var biografia = $("#biografia-libro").val();
    biografia = biografia.replace(/\r?\n/g, '<br />');

    if(verificarnombre(nombre_nino, "nombre del niño") && verificarnombre(nombre_padre, "nombre del padre") && verificarnombre(nombre_madre, "nombre de la madre") && verificarnombre(nombre_mascota, "nombre de la mascota") && verificarnombre(nombre_familiar, "nombre de la familiar") && verificardedicatoria(dedicatoria, "dedicatoria") && verificarbiografia(ciudad_nacimiento, "ciudad de nacimiento") && verificarbiografia(cancion_favorita, "canción favorita") && verificarbiografia(comida_favorita, "comida favorita") && verificarbiografia(juguete_favorito, "juguete favorito")&& verificarbiografia(actividad_favorita, "actividad favorita") && verificarbiografia(biografia, "biografía")){
        

        if(nombre_nino){
           nombre_nino=encodeURIComponent(nombre_nino);
        }else{
           nombre_nino=encodeURIComponent("<?php echo $nombre; ?>");
        }
       console.log(nombre_nino);


        if(fecha_nino){
           fecha_nino=encodeURIComponent(fecha_nino);
        }
       console.log(fecha_nino);

        if(nombre_padre){
           nombre_padre=encodeURIComponent(nombre_padre);
        }
       console.log(nombre_padre);

        if(nombre_madre){
           nombre_madre=encodeURIComponent(nombre_madre);
        }
       console.log(nombre_madre);

        if(sexo_mascota){
           sexo_mascota=encodeURIComponent(sexo_mascota);
        }
       console.log(sexo_mascota);

        if(nombre_mascota){
           nombre_mascota=encodeURIComponent(nombre_mascota);
        }
       console.log(nombre_mascota);

        if(tipo_familiar){
           tipo_familiar=encodeURIComponent(tipo_familiar);
        }
       console.log(tipo_familiar);

        if(nombre_familiar){
           nombre_familiar=encodeURIComponent(nombre_familiar);
        }
       console.log(nombre_familiar);
        
        if(dedicatoria){
           dedicatoria=encodeURIComponent(dedicatoria);
        }
        
        if(ciudad_nacimiento){
           ciudad_nacimiento=encodeURIComponent(ciudad_nacimiento);
        }
       console.log(ciudad_nacimiento);

        if(cancion_favorita){
           cancion_favorita=encodeURIComponent(cancion_favorita);
        }
       console.log(cancion_favorita);

        if(comida_favorita){
           comida_favorita=encodeURIComponent(comida_favorita);
        }
       console.log(comida_favorita);

        if(juguete_favorito){
           juguete_favorito=encodeURIComponent(juguete_favorito);
        }
       console.log(juguete_favorito);

        if(actividad_favorita){
           actividad_favorita=encodeURIComponent(actividad_favorita);
        }
       console.log(actividad_favorita);

        if(biografia){
           biografia=encodeURIComponent(biografia);
        }
       console.log(biografia);

        jQuery.ajax({
            type:"POST",
            url: "../wp-content/themes/generic/setvariables.php",
            data: { 
                sexo: sexo,
                nombre: nombre_nino,
                fec_nin: fecha_nino,
                nbr_pap: nombre_padre,
                nbr_mad: nombre_madre,
                sex_gat: sexo_mascota,
                nbr_gat: nombre_mascota,
                tip_fam: tipo_familiar,
                nbr_fam: nombre_familiar,
                ded: dedicatoria,
                ciu_nac: ciudad_nacimiento,
                can_fav: cancion_favorita,
                com_fav: comida_favorita,
                jug_fav: juguete_favorito,
                act_fav: actividad_favorita,
                bio: biografia,
                img_0: img_1,
                img_1: img_2,
                img_2: img_3,
                img_3: img_4,
                img_4: img_5,
                variation: variation, 
                tapa: tapa, 
                tamano: tamano
            },
            cache: false,
            success: function(result){
                console.log("Resultado: "+result);
                var link_href = jQuery("#pedir-ctapa-"+variation).attr("data-href");
                console.log("Link: "+link_href);
                window.location.href= link_href;
            },
            error: function(error){
                console.log(JSON.stringify(error));
            }


        });

        
        
        
    }else{
        alert(mensaje_subir_error);
    }
    

}
    
//FUNCION PARA ROTAR
function drawRotated(id_cr_container, degrees){
    var canvas=$("#"+id_cr_container+" canvas")[0];
    var ctx=canvas.getContext("2d");

    // Create a temp canvas to store our data (because we need to clear the other box after rotation.
    var tempCanvas = document.createElement("canvas"),
    tempCtx = tempCanvas.getContext("2d");

    tempCanvas.width = canvas.width;
    tempCanvas.height = canvas.height;
    // put our data onto the temp canvas
    tempCtx.drawImage(canvas,0,0,canvas.width,canvas.height);

    // Append for debugging purposes, just to show what the canvas did look like before the transforms.
    document.body.appendChild(tempCanvas);

    ctx.clearRect(0,0,canvas.width,canvas.height);
    ctx.save();
    
    //Trasladamos el punto de rotacion
    ctx.translate((canvas.width)/2,(canvas.height)/2);
    //Rotamos
    ctx.rotate(degrees*Math.PI/180);
    //Regresamos el punto de rotación
    ctx.translate(-(canvas.width)/2,-(canvas.height)/2);
    //Dibujamos el temporal
    ctx.drawImage(tempCanvas,0,0,canvas.width,canvas.height);
    ctx.restore();
    // Append for debugging purposes, just to show what the canvas did look like before the transforms.
    document.body.removeChild(tempCanvas);
}
//END FUNCION PARA ROTAR
$(document).ready(function(){
    ingresarMensaje();
    
    $( ".inputs-texto" ).each(function() {
        var texto = $(this).val();
        var leng_texto = texto.length;
        var elem_muestr = $(this).attr("data-cuenta");  
        var max_char = $(this).attr("data-max");  
        
        if(leng_texto < max_char){
            $("#"+elem_muestr).html(leng_texto);
        }else{
            $("#"+elem_muestr).html("<span style='color:red;font-weight:bold'>"+leng_texto+"</span>");
        }
    });
 
    
      $('.btn-sub-fotomd').on('click', function(){
        $('#subir-foto-mod').modal('toggle');
      });
    
    
    
    //AJAX

  $('.crop_image').click(function(event){
        $( this ).css({
          "opacity": "0.5",
          "pointer-events": "none"
        });
      var num_img= parseInt($(this).attr("data-id"));
      var width_sz;
      var height_sz;
      var image_crop;
      switch (num_img) {
            case 0:
                image_crop= $image_crop;
                width_sz = 200;
                height_sz = 200;
                break;
            case 1:
                image_crop= $image_crop2;
                width_sz = 400;
                height_sz = 540;
                break;
            case 2:
                image_crop= $image_crop3;
                width_sz = 410;
                height_sz = 590;
                break;
            case 3:
                image_crop= $image_crop4;
                width_sz = 200;
                height_sz = 200;
                break;
            case 4:
                image_crop= $image_crop5;
                width_sz = 200;
                height_sz = 200;
                break;
            default:
                image_crop= $image_crop;
                break;
        }
    image_crop.croppie('result', {
      type: 'canvas',
      size: { width: width_sz, height: height_sz }
    }).then(function(response){
        console.log("response: "+response);
      $.ajax({
        url:"/wp-content/themes/generic/upload.php",
        type: "POST",
        data:{"image": response},
        success:function(data)
        {
            $('#uploadimageModal').modal('hide');
            
            //Acá se tiene que poner la nueva acción
            //window.location = "/libro.php";
            //$('#uploaded_image').html(data);
            var link_red = "";
            var num_links = 0;
            
            var nombre_nino = $("#nombre-nino-libro").val();
            var fecha_nino = $("#fecha-formcont").val();
            var nombre_padre = $("#nombre-padre-libro").val();
            var nombre_madre = $("#nombre-madre-libro").val();
            var sexo_mascota = $("#sexmasc-familiar-libro").val();
            var nombre_mascota = $("#gato-madre-libro").val();
            var tipo_familiar = $("#tipo-familiar-libro").val();
            var nombre_familiar = $("#nombre-familiar-libro").val();
      
            var dedicatoria = $("#dedicatoria-libro").val();
            dedicatoria = dedicatoria.replace(/\r?\n/g, '<br />');
      
            //BIOGRAFIA();
            var ciudad_nacimiento = $("#ciudad-nacimiento-libro").val();
            var cancion_favorita = $("#cancion-favorita-libro").val();
            var comida_favorita = $("#comida-favorita-libro").val();
            var juguete_favorito = $("#juguete-favorito-libro").val();
            var actividad_favorita = $("#actividad-favorita-libro").val();
            var biografia = $("#biografia-libro").val();
            biografia = biografia.replace(/\r?\n/g, '<br />');

            if(nombre_nino){
               link_red+=("&nombre="+encodeURIComponent(nombre_nino));
            }else{
               link_red+=("&nombre="+encodeURIComponent("<?php echo $nombre; ?>"));
            }
            if(fecha_nino){
               link_red+=("&fec_nin="+encodeURIComponent(fecha_nino));
            }
            if(nombre_padre){
               link_red+=("&nbr_pap="+encodeURIComponent(nombre_padre));
            }
            if(nombre_madre){
               link_red+=("&nbr_mad="+encodeURIComponent(nombre_madre));
            }
            if(sexo_mascota){
               link_red+=("&sex_gat="+encodeURIComponent(sexo_mascota));
            }
            if(nombre_mascota){
               link_red+=("&nbr_gat="+encodeURIComponent(nombre_mascota));
            }
            if(tipo_familiar){
               link_red+=("&tip_fam="+encodeURIComponent(tipo_familiar));
            }
            if(nombre_familiar){
               link_red+=("&nbr_fam="+encodeURIComponent(nombre_familiar));
            }
            if(dedicatoria){
               link_red+=("&ded="+encodeURIComponent(dedicatoria));
            }
            if(ciudad_nacimiento){
               link_red+=("&ciu_nac="+encodeURIComponent(ciudad_nacimiento));
            }
            if(cancion_favorita){
               link_red+=("&can_fav="+encodeURIComponent(cancion_favorita));
            }
            if(comida_favorita){
               link_red+=("&com_fav="+encodeURIComponent(comida_favorita));
            }
            if(juguete_favorito){
               link_red+=("&jug_fav="+encodeURIComponent(juguete_favorito));
            }
            if(actividad_favorita){
               link_red+=("&act_fav="+encodeURIComponent(actividad_favorita));
            }
            if(biografia){
               link_red+=("&bio="+encodeURIComponent(biografia));
            }
            
            
            imgs_arr[num_img]= data;
            
            for (var i = 0; i < imgs_arr.length; i++) { 
                
                if(imgs_arr[i] > 0 ){
                   link_red+=("&img_"+i+"="+imgs_arr[i]);
                   num_links++;
                }
            }
            //$("body").append("index.php?"+link_red);
            window.location.href = "index.php?sexo=<?php echo $sexo; ?>"+link_red;
            //console.log("mensaje:" +data);
        }
      });
    })
  });


  $('#modificar').click(function(event){
        var link_red = "";
      
        
        var nombre_nino = $("#nombre-nino-libro").val();
        var fecha_nino = $("#fecha-formcont").val();
        var nombre_padre = $("#nombre-padre-libro").val();
        var nombre_madre = $("#nombre-madre-libro").val();
        var sexo_mascota = $("#sexmasc-familiar-libro").val();
        var nombre_mascota = $("#gato-madre-libro").val();
        var tipo_familiar = $("#tipo-familiar-libro").val();
        var nombre_familiar = $("#nombre-familiar-libro").val();
      
        var dedicatoria = $("#dedicatoria-libro").val();
        dedicatoria = dedicatoria.replace(/\r?\n/g, '<br />');
      
        //BIOGRAFIA();
        var ciudad_nacimiento = $("#ciudad-nacimiento-libro").val();
        var cancion_favorita = $("#cancion-favorita-libro").val();
        var comida_favorita = $("#comida-favorita-libro").val();
        var juguete_favorito = $("#juguete-favorito-libro").val();
        var actividad_favorita = $("#actividad-favorita-libro").val();
        var biografia = $("#biografia-libro").val();
        biografia = biografia.replace(/\r?\n/g, '<br />');
      
        if(verificarnombre(nombre_nino, "nombre del niño") && verificarnombre(nombre_padre, "nombre del padre") && verificarnombre(nombre_madre, "nombre de la madre") && verificarnombre(nombre_mascota, "nombre de la mascota") && verificarnombre(nombre_familiar, "nombre de la familiar") && verificardedicatoria(dedicatoria, "dedicatoria") && verificarbiografia(ciudad_nacimiento, "ciudad de nacimiento") && verificarbiografia(cancion_favorita, "canción favorita") && verificarbiografia(comida_favorita, "comida favorita") && verificarbiografia(juguete_favorito, "juguete favorito")&& verificarbiografia(actividad_favorita, "actividad favorita") && verificarbiografia(biografia, "biografía")){
           
            if(nombre_nino){
               link_red+=("&nombre="+encodeURIComponent(nombre_nino));
            }else{
               link_red+=("&nombre="+encodeURIComponent("<?php echo $nombre; ?>"));
            }
            if(fecha_nino){
               link_red+=("&fec_nin="+encodeURIComponent(fecha_nino));
            }
            if(nombre_padre){
               link_red+=("&nbr_pap="+encodeURIComponent(nombre_padre));
            }
            if(nombre_madre){
               link_red+=("&nbr_mad="+encodeURIComponent(nombre_madre));
            }
            if(sexo_mascota){
               link_red+=("&sex_gat="+encodeURIComponent(sexo_mascota));
            }
            if(nombre_mascota){
               link_red+=("&nbr_gat="+encodeURIComponent(nombre_mascota));
            }
            if(tipo_familiar){
               link_red+=("&tip_fam="+encodeURIComponent(tipo_familiar));
            }
            if(nombre_familiar){
               link_red+=("&nbr_fam="+encodeURIComponent(nombre_familiar));
            }
            if(dedicatoria){
               link_red+=("&ded="+encodeURIComponent(dedicatoria));
            }
            if(ciudad_nacimiento){
               link_red+=("&ciu_nac="+encodeURIComponent(ciudad_nacimiento));
            }
            if(cancion_favorita){
               link_red+=("&can_fav="+encodeURIComponent(cancion_favorita));
            }
            if(comida_favorita){
               link_red+=("&com_fav="+encodeURIComponent(comida_favorita));
            }
            if(juguete_favorito){
               link_red+=("&jug_fav="+encodeURIComponent(juguete_favorito));
            }
            if(actividad_favorita){
               link_red+=("&act_fav="+encodeURIComponent(actividad_favorita));
            }
            if(biografia){
               link_red+=("&bio="+encodeURIComponent(biografia));
            }

            for (var i = 0; i < imgs_arr.length; i++) { 
                if(imgs_arr[i] > 0 ){
                   link_red+=("&img_"+i+"="+encodeURIComponent(imgs_arr[i]));
                }
            }

            window.location.href = "index.php?sexo=<?php echo $sexo; ?>"+link_red;
           
        }else{
            alert(mensaje_subir_error);
        }
      

      
  });

    console.log("Cargo todo1");

});
    
window.onload = function() {
    $(".libro-estado-wrapp").hide();
    $(".libro-wrapper").show();
    estado_carga = 1;
    console.log("Cargo todo2");
};
</script>