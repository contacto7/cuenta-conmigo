
<?php get_header(); ?>
<div class="header">
        <h1 class="entry-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
    </div>
<main id="content">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    
    <?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
    <?php the_content(); ?>
    <div class="entry-links"><?php wp_link_pages(); ?></div>
    </article>
    <?php if ( ! post_password_required() ) { comments_template( '', true ); } ?>
    <?php endwhile; endif; ?>
    
</main>
<?php if (is_product()||is_shop()){?>
<aside class="woocommerce-sidebar" id="sidebar">
    <?php if ( is_active_sidebar( 'woocommerce-side-bar' ) ) : ?>
        <?php dynamic_sidebar( 'woocommerce-side-bar' ); ?>
    <?php endif; ?> 
</aside>
<?php } ?>
<?php get_footer(); ?>