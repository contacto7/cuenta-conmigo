var TO_RADIANS = Math.PI/180;

function mostrarcert(e){
    $('body').css({"min-width":"980px"});
    $("#canvas-"+e).css({
        "position": "relative",
        "top": "0",
        "left":"0"
    });
} 

//AÑADIR IMAGEN
function drawRotatedImage(image, canvas, context, x, y, angle) { 
 
	// save the current co-ordinate system 
	// before we screw with it
	context.save(); 
 
	// move to the middle of where we want to draw our image
	context.translate(x, y);
 
	// rotate around that point, converting our 
	// angle from degrees to radians 
	context.rotate(angle * TO_RADIANS);
 
	// draw it up and to the left by half the width
	// and height of the image 
	context.drawImage(image, 0, 0, image.width,    image.height,     // source rectangle
                                   0, 0, canvas.width, canvas.height);
 
	// and restore the co-ords to how they were when we began
	context.restore(); 
}


function cropCara(image, canvas, context, x, y, angle) { 
    
    context.save();
    context.scale(1,2);
    context.beginPath();
    context.arc(60, 0, 60, 0, Math.PI * 2, true);
    context.closePath();
    context.clip();
    context.scale(1,0.5);
    context.drawImage(image, 0, 0, image.width,    image.height,     // source rectangle
                                   0, 0, canvas.width, canvas.height);
    context.restore(); 
    
    
}


function cropRotateCara(image, canvas, context, x, y, angle) { 
    
    //todos los tamaños dependen del tamaño del canvas
    var tamano_imagen = parseInt(canvas.width);
    
    var pos_x_ovalo_rostro = tamano_imagen/2;
    var rad_ovalo_rostro = tamano_imagen/2;
    
    var pos_x_ovalo_cabello = tamano_imagen/2;
    var pos_y_ovalo_cabello = tamano_imagen;
    var rad_ovalo_cabello = tamano_imagen;
    
    //ROTACION
	// save the current co-ordinate system 
	// before we screw with it
	context.save(); 
 
	// move to the middle of where we want to draw our image
	context.translate(x, y);
 
	// rotate around that point, converting our 
	// angle from degrees to radians 
	context.rotate(angle * TO_RADIANS);
 
	// draw it up and to the left by half the width
	// and height of the image 
    //END ROTACION
    
    //PRIMER CROP OVALADO FORMA DE CARA
    context.save();
    context.scale(1,2);
    context.beginPath();
    context.arc(pos_x_ovalo_rostro, 0, rad_ovalo_rostro, 0, Math.PI * 2, true);
    context.closePath();
    context.clip();
    context.scale(1,0.5);
    //END PRIMER CROP OVALADO FORMA DE CARA
    
    //PRIMER CROP OVALADO FORMA DE CABELLO
    context.save();
    context.beginPath();
    context.arc(pos_x_ovalo_cabello, pos_y_ovalo_cabello,rad_ovalo_cabello, 0, Math.PI * 2, true);
    context.closePath();
    context.clip();
    //END PRIMER CROP OVALADO FORMA DE CABELLO
    
    context.drawImage(image, 0, 0, image.width,    image.height,     // source rectangle
                                   0, 0, canvas.width, canvas.height);
    context.restore(); 
}
//END AÑADIR IMAGEN

//AÑADIR FOTO//


//END AÑADIR FOTO//