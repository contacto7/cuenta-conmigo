<?php

session_start();

unset($_SESSION['sexo']);
unset($_SESSION['nombre']);
unset($_SESSION['fec_nin']);
unset($_SESSION['nbr_pap']);
unset($_SESSION['nbr_mad']);
unset($_SESSION['sex_gat']);
unset($_SESSION['nbr_gat']);
unset($_SESSION['tip_fam']);
unset($_SESSION['nbr_fam']);
unset($_SESSION['ded']);
unset($_SESSION['ciu_nac']);
unset($_SESSION['can_fav']);
unset($_SESSION['com_fav']);
unset($_SESSION['jug_fav']);
unset($_SESSION['act_fav']);
unset($_SESSION['bio']);
unset($_SESSION['img_0']);
unset($_SESSION['img_1']);
unset($_SESSION['img_2']);
unset($_SESSION['img_3']);
unset($_SESSION['img_4']);
unset($_SESSION['variation']);
unset($_SESSION['tapa']);
unset($_SESSION['tamano']);

//INFORMACIÓN NECESARIA
$sexo = $_POST["sexo"];
$nombre = $_POST["nombre"];
$_SESSION['sexo']=$sexo;
$_SESSION['nombre']=$nombre;

//INFO ADICIONAL
if($_POST["fec_nin"] != ""){
    $fec_nacimiento_nino = $_POST["fec_nin"];
    $_SESSION['fec_nin']=$fec_nacimiento_nino;
}

if($_POST["nbr_pap"] != ""){
    $nombre_padre = $_POST["nbr_pap"];
    $_SESSION['nbr_pap']=$nombre_padre;
}

if($_POST["nbr_mad"] != ""){
    $nombre_madre = $_POST["nbr_mad"];
    $_SESSION['nbr_mad']=$nombre_madre;
}

if($_POST["sex_gat"] != ""){
    $sexo_gato = $_POST["sex_gat"];
    $_SESSION['sex_gat']=$sexo_gato;
}

if($_POST["nbr_gat"] != ""){
    $nombre_gato = $_POST["nbr_gat"];
    $_SESSION['nbr_gat']=$nombre_gato;
}

if($_POST["tip_fam"] != ""){
    $tipo_familiar = $_POST["tip_fam"];
    $_SESSION['tip_fam']=$tipo_familiar;
}

if($_POST["nbr_fam"] != ""){
    $nombre_familiar = $_POST["nbr_fam"];
    $_SESSION['nbr_fam']=$nombre_familiar;
}

if($_POST["ded"] != ""){
    $dedicatoria = $_POST["ded"];
    $_SESSION['ded']=$dedicatoria;
}

if($_POST["ciu_nac"] != ""){
    $ciudad_nacimiento = $_POST["ciu_nac"];
    $_SESSION['ciu_nac']=$ciudad_nacimiento;
}

if($_POST["can_fav"] != ""){
    $cancion_favorita = $_POST["can_fav"];
    $_SESSION['can_fav']=$cancion_favorita;
}

if($_POST["com_fav"] != ""){
    $comida_favorita = $_POST["com_fav"];
    $_SESSION['com_fav']=$comida_favorita;
}

if($_POST["jug_fav"] != ""){
    $juguete_favorito = $_POST["jug_fav"];
    $_SESSION['jug_fav']=$juguete_favorito;
}

if($_POST["act_fav"] != ""){
    $actividad_favorita = $_POST["act_fav"];
    $_SESSION['act_fav']=$actividad_favorita;
}

if($_POST["bio"] != ""){
    $biografia = $_POST["bio"];
    $_SESSION['bio']=$biografia;
}

//IMAGENES
if($_POST["img_0"] > 0){
    $rostro_nino = $_POST["img_0"];
    $_SESSION['img_0']=$rostro_nino;
}

if($_POST["img_1"] > 0){
    $retrato_familiar = $_POST["img_1"];
    $_SESSION['img_1']=$retrato_familiar;
}

if($_POST["img_2"] > 0){
    $retrato_nino = $_POST["img_2"];
    $_SESSION['img_2']=$retrato_nino;
}

if($_POST["img_3"] > 0){
    $rostro_madre = $_POST["img_3"];
    $_SESSION['img_3']=$rostro_madre;
}

if($_POST["img_4"] > 0){
    $rostro_padre = $_POST["img_4"];
    $_SESSION['img_4']=$rostro_padre;
}

if($_POST["variation"] != ""){
    $variation = $_POST["variation"];
    $_SESSION['variation']=$variation;
}

if($_POST["tapa"] != ""){
    $tapa = $_POST["tapa"];
    $_SESSION['tapa']=$tapa;
}

if($_POST["tamano"] != ""){
    $tamano = $_POST["tamano"];
    $_SESSION['tamano']=$tamano;
}

?>