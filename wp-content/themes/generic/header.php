<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width" />
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div id="wrapper" class="hfeed">
        <?php
        $subdomain = array_shift((explode('.', $_SERVER['HTTP_HOST'])));
        $cart_link = "/carrito/";
        $subdomain_class = "www";
        
        if($subdomain == "en"){
            $subdomain_class = "en";
            $cart_link = "/cart/";
        }
        ?>
        <header id="header" class="header-<?php echo $subdomain_class; ?>">

            <nav id="menu">

                <label class="toggle" for="toggle">
                    <span class="menu-icon">&#9776;</span>
                    <span class="menu-title"><?php esc_html_e( 'Menu', 'generic' ); ?></span>
                </label>
                <div class="mobile-header logo-mbh-wrapp">
                    <a class="logo-mbh-inn" href="/">
                        <img src="/wp-content/imagenes/Logo.png" class="logo-mbh" alt="logo de Cuenta conmigo">
                    </a>
                </div>
                <div class="mobile-header carrito-mbh-wrapp">
                    <a class="logo-mbh-inn" href="<?php echo $cart_link; ?>">
                        <img src="/wp-content/imagenes/cart-1.png" class="logo-mbh" alt="logo de Cuenta conmigo">
                    </a>
                </div>
                <input id="toggle" class="toggle" type="checkbox" />

                <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
                <div class="idiomas">
                    <?php wp_nav_menu( array( 'theme_location' => 'language-menu' ) ); ?>
                </div>
            </nav>
        </header>
        <div id="container">