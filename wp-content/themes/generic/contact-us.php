<?php /* Template Name: Contact-Us */ ?>
<?php get_header(); ?>
<div class="page-template-contacto" style="padding-top:100px">
    <div class="titulo-pagina">
        <h1 style="color: #ff6400;
        font-size: 28px;">Contact Us</h1>    
    </div>
    <main id="content">
        <div class="parrafo estrecho">
        <?php
        echo do_shortcode('[contact-form-7 id="107" title="Contact Form"]')
        ?>
        </div>

    </main>
</div>
<?php get_footer(); ?>