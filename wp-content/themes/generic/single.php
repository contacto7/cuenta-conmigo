<?php get_header(); ?>

<main id="content">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php get_template_part( 'entry' ); ?>
<?php if ( ! post_password_required() && ! is_product()) { comments_template( '', true ); } ?>
<?php endwhile; endif; ?>

</main>

<?php if(is_product()){ ?>
    <aside class="woocommerce-sidebar" id="sidebar">
    <?php if ( is_active_sidebar( 'woocommerce-side-bar' ) ) : ?>
        <?php dynamic_sidebar( 'woocommerce-side-bar' ); ?>
    <?php endif; ?> 
    </aside>

<?php } ?>

<?php get_footer(); ?>