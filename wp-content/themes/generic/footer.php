</div>
<?php
$subdomain = array_shift((explode('.', $_SERVER['HTTP_HOST'])));
//var_dump($subdomain);
if($subdomain=="en"){
?> 
    
<footer id="footer" class="marco">
    <div class="nube-footer adorno">
        <img src="/wp-content/imagenes/Nubes5.png" alt="nubes">

    </div>
    <div class="principe">
        <img src="/wp-content/imagenes/Principe volando.png" alt="principe">

    </div>
    <div class="texto-contacto">
        <h2 class="titulo">
            Contact Us
        </h2>
        <p class="contenido">
            Need personalised attention? <br>
            Send us a message to <a class="dfi-ln" href="mailto:info@soycuentaconmigo.com"
                target="_blank">info@soycuentaconmigo.com</a>
        </p>

    </div>
    <div class="redes">
        <div class="red fb">
            <a class="cfi-img" href="https://www.facebook.com/cconmigocuentos/" target="_blank">
                <img src="/wp-content/imagenes/Facebook.png" alt="facebook">
            </a>
        </div>
        <div class="red wpp">
            <a class="cfi-img" href="https://www.instagram.com/cuentaconmigocuentos/"
                target="_blank">
                <img src="/wp-content/imagenes/ig-5252.png" alt="instagram">
            </a>
        </div>
        <div class="red tw">
            <a class="cfi-img" href="https://twitter.com/CuentaConmigoUk"
                target="_blank">
                <img src="/wp-content/imagenes/Twitter.png" alt="twitter">
            </a>
        </div>
    </div>
    <div class="logo">
        <img src="/wp-content/imagenes/Logo.png" alt="logo">

    </div>
</footer>


<?php
    }else{
?> 
<footer id="footer" class="marco">
    <div class="nube-footer adorno">
        <img src="/wp-content/imagenes/Nubes5.png" alt="nubes">

    </div>
    <div class="principe">
        <img src="/wp-content/imagenes/Principe volando.png" alt="principe">

    </div>
    <div class="texto-contacto">
        <h2 class="titulo">
            Contacto
        </h2>
        <p class="contenido">
            ¿Necesitas atención personalizada? <br>
            Escríbenos a <a class="dfi-ln" href="mailto:info@soycuentaconmigo.com"
                target="_blank">info@soycuentaconmigo.com</a>
        </p>
        <p class="lugar">
            Lima-Perú
        </p>
    </div>
    <div class="redes">
        <div class="red fb">
            <a class="cfi-img" href="https://www.facebook.com/cconmigocuentos/" target="_blank">
                <img src="/wp-content/imagenes/Facebook.png" alt="facebook">
            </a>
        </div>
        <div class="red wpp">
            <a class="cfi-img" href="https://www.instagram.com/cuentaconmigocuentos/"
                target="_blank">
                <img src="/wp-content/imagenes/ig-5252.png" alt="instagram">
            </a>
        </div>
        <div class="red tw">
            <a class="cfi-img" href="https://twitter.com/CuentaConmigoUk"
                target="_blank">
                <img src="/wp-content/imagenes/Twitter.png" alt="twitter">
            </a>
        </div>
    </div>
    <div class="logo">
        <img src="/wp-content/imagenes/Logo.png" alt="logo">

    </div>
</footer>

<?php
         }
?>

</div>
<?php wp_footer(); ?>
</body>

</html>